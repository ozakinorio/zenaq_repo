jQuery.fn.extend({
    paralmax: function () {
        var a;
        return $(window).width(), a = $(window).height(), $(this).each(function () {
            function h() {
                f() ? $(c).data("speed") || 0 === $(c).data("speed") ? j.speed = $(c).data("speed") : j.speed = 0.15 : j.speed = 0
            }

            function b() {
                var m, d, i = c;
                !0 === j.resizable && ($(window).width() > j.breakpoint ? (m = parseInt($(i).parent().outerHeight()), d = parseInt($(i).parent().offset().top) > parseInt(a) ? parseInt(a * j.speed) : parseInt($(i).parent().offset().top) * j.speed, $(i).height(m + d)) : $(i).height(m))
            }

            function g() {
                var i = c,
                    o = $("html").scrollTop(),
                    d = $("body").scrollTop(),
                    m = (o > d ? o : d) - j.start,
                    p = parseInt(m * j.speed) + j.offset;
                $(i).css({
                    webkitTransform: "translate3d(0, " + p + "px, 0)",
                    MozTransform: "translate3d(0, " + p + "px, 0)",
                    msTransform: "translateY(" + p + "px)",
                    OTransform: "translate3d(0, " + p + "px, 0)",
                    transform: "translate3d(0, " + p + "px, 0)"
                })
            }

            function l() {
                if (!f()) {
                    var d = c;
                    $(d).css({
                        webkitTransform: "translate3d(0, 0, 0)",
                        MozTransform: "translate3d(0, 0, 0)",
                        msTransform: "translateY(0)",
                        OTransform: "translate3d(0, 0, 0)",
                        transform: "translate3d(0, 0, 0)"
                    })
                }
            }

            function f() {
                return $(window).width() > j.breakpoint
            }
            var k, c = this,
                j = {
                    start: (k = $(c).offset()).top,
                    stop: k.top + $(c).outerHeight(),
                    speed: 0,
                    resizable: !!$(c).data("resize") && $(c).data("resize"),
                    breakpoint: $(c).data("breakpoint") ? $(c).data("breakpoint") : 0,
                    offset: $(c).data("offset") ? $(c).data("offset") * $(c).outerHeight() : 0
                };
            h(), b(), l(), f() && g(), $(window).bind("scroll", function () {
                f() && g()
            }), $(window).bind("resize", function () {
                h(), b(), l()
            })
        })
    }
});
