
//slider_product
    jQuery(function($){
        $('.slider').slick({
            arrows:false,
            asNavFor:'.thumb',
        });
        $('.thumb').slick({
            asNavFor:'.slider',
            focusOnSelect: true,
            slidesToShow:5,
            slidesToScroll:1
        });
    });


//product_detail_slick_gallery
jQuery(function($){
    $('.slick_gallery').slick({ //{}を入れる
        autoplay: true, //「オプション名: 値」の形式で書く
        arrows: true,
        slidesToShow: 3,
        centerMode: true,
        centerPadding: '5%',
        adaptiveHeight: true,
        prevArrow: '<p class="slick-prev"><img class="slide-arrow" src="img/common/arrow_left.svg"></p>',
        nextArrow: '<p class="slick-next"><img class="slide-arrow" src="img/common/arrow.svg"></p>',
        responsive: [{
          breakpoint: 480,
          settings: {
            slidesToShow: 2, // 表示スライド数 2つ
            slidesToScroll: 1,
          }
        }]
      });
});

//product_detail_slick_gallery
jQuery(function($){
  $('.slick_gallery02').slick({ //{}を入れる
      autoplay: true, //「オプション名: 値」の形式で書く
      arrows: true,
      slidesToShow: 3,
      centerMode: true,
      centerPadding: '5%',
      adaptiveHeight: true,
      prevArrow: '<p class="slick-prev02"><img class="slide-arrow" src="img/common/arrow_left.svg"></p>',
      nextArrow: '<p class="slick-next02"><img class="slide-arrow" src="img/common/arrow.svg"></p>',
      responsive: [{
        breakpoint: 480,
        settings: {
          slidesToShow: 2, // 表示スライド数 2つ
          slidesToScroll: 1,
        }
      }]
    });
});


//タブ
//任意のタブにURLからリンクするための設定
function GethashID (hashIDName){
    if(hashIDName){
      //タブ設定
      $('.tab li').find('a').each(function() { //タブ内のaタグ全てを取得
        var idName = $(this).attr('href'); //タブ内のaタグのリンク名（例）#lunchの値を取得
        if(idName == hashIDName){ //リンク元の指定されたURLのハッシュタグ（例）http://example.com/#lunch←この#の値とタブ内のリンク名（例）#lunchが同じかをチェック
          var parentElm = $(this).parent(); //タブ内のaタグの親要素（li）を取得
          $('.tab li').removeClass("active"); //タブ内のliについているactiveクラスを取り除き
          $(parentElm).addClass("active"); //リンク元の指定されたURLのハッシュタグとタブ内のリンク名が同じであれば、liにactiveクラスを追加
          //表示させるエリア設定
          $(".area").removeClass("is-active"); //もともとついているis-activeクラスを取り除き
          $(hashIDName).addClass("is-active"); //表示させたいエリアのタブリンク名をクリックしたら、表示エリアにis-activeクラスを追加
        }
      });
    }
  }
  //タブをクリックしたら
  $('.tab a').on('click', function() {
    var idName = $(this).attr('href'); //タブ内のリンク名を取得
    GethashID (idName);//設定したタブの読み込みと
    return false;//aタグを無効にする
  });

  // 上記の動きをページが読み込まれたらすぐに動かす
  $(window).on('load', function () {
      $('.tab li:first-of-type').addClass("active"); //最初のliにactiveクラスを追加
      $('.area:first-of-type').addClass("is-active"); //最初の.areaにis-activeクラスを追加
    var hashName = location.hash; //リンク元の指定されたURLのハッシュタグを取得
    GethashID (hashName);//設定したタブの読み込み
  });


//拡大ルーペアニメーション
$(function(){
  var imageWidth = 1000; //画像サイズ
  var magnification = 3; //拡大率

  $('[id^="image"]').mousemove(function(e){
      var offset = $(this).offset();
      var left = (e.pageX - offset.left);
      var top = (e.pageY - offset.top);
      var width = $(this).width();
      var height = $(this).height();
      var loupe = $(this).children('.loupe');

      //拡大鏡起動反転
      if(loupe.is(':not(:animated):hidden')){
          $(this).trigger('mouseenter');
      }

      //範囲外判定
      if(e.pageX < offset.left || e.pageY < offset.top || e.pageX > (offset.left + width) || e.pageY > (offset.top + height))
      {
          if(!loupe.is(':animated')){
              $(this).trigger('mouseleave');
          }
          return false;
      }

      //拡大鏡表示位置
      loupe.css('left', e.pageX - loupe.width() / 2);
      loupe.css('top', e.pageY - loupe.height() / 2);

      //画像表示位置
      var x = (magnification * left * -1) + (loupe.width() / 2);
      var y = (magnification * top * -1) + (loupe.height() / 2);
      loupe.css('background-position', x + 'px ' + y + 'px');
  }).mouseleave(function(){
      $(this).children('.loupe').stop(true, true).fadeOut('fast');
  }).mouseenter(function(){
      $(this).children('.loupe').css('background-size', (imageWidth * magnification) + 'px auto');
      $(this).children('.loupe').stop(true, true).fadeIn('fast');
  });
});

$(function () {
  $('.popup').magnificPopup({
    type: 'image',
  });
});

$(function () {
  $('.popup-youtube').magnificPopup({
    type: 'iframe',
    removalDelay: 200,
  });
});

//SVGアニメーションの描画
var stroke;
stroke = new Vivus('mask', {//アニメーションをするIDの指定
    start:'manual',//自動再生をせずスタートをマニュアルに
    type: 'scenario-sync',// アニメーションのタイプを設定
    duration: 70,//アニメーションの時間設定。数字が小さくなるほど速い
    forceRender: false,//パスが更新された場合に再レンダリングさせない
    animTimingFunction:Vivus.EASE,//動きの加速減速設定
},
function(){
       $("#mask").attr("class", "done");//描画が終わったらdoneというクラスを追加
}
);


$(window).on('load',function(){
  $("#splash").delay(3000).fadeOut('slow');//ローディング画面を3秒（3000ms）待機してからフェイドアウト
	$("#splash_logo").delay(3000).fadeOut('slow');//ロゴを3秒（3000ms）待機してからフェイドアウト
        stroke.play();//SVGアニメーションの実行
});


//window scroll
var beforePos = 0;//スクロールの値の比較用の設定

//スクロール途中でヘッダーが消え、上にスクロールすると復活する設定を関数にまとめる
function ScrollAnime() {
    var elemTop = $('#area').offset().top;//#areaの位置まできたら
	var scroll = $(window).scrollTop();
    //ヘッダーの出し入れをする
    if(scroll == beforePos) {
		//IE11対策で処理を入れない
    }else if(elemTop > scroll || 0 > scroll - beforePos){
		//ヘッダーが上から出現する
		$('#header').removeClass('UpMove');	//#headerにUpMoveというクラス名を除き
		$('#header').addClass('DownMove');//#headerにDownMoveのクラス名を追加
    }else {
		//ヘッダーが上に消える
        $('#header').removeClass('DownMove');//#headerにDownMoveというクラス名を除き
		$('#header').addClass('UpMove');//#headerにUpMoveのクラス名を追加
    }
    beforePos = scroll;//現在のスクロール値を比較用のbeforePosに格納
}


// 画面をスクロールをしたら動かしたい場合の記述
$(window).scroll(function () {
	ScrollAnime();//スクロール途中でヘッダーが消え、上にスクロールすると復活する関数を呼ぶ
});


//トップアバウトホバーアニメーション
//リスト要素を取得する
const list = document.querySelector(".list");
//リスト要素の子要素（li）をすべて取得する
const listChildren = list.children;

//listChildrenの数だけ繰り返し処理を実行する
for (let item of listChildren) {
    item.addEventListener(
        "mouseover",
        function () {
            //リストアイテムのdata-targetを取得
            let target = item.dataset.target;
            //id="app"の要素を取得
            let el = document.getElementById("app");
            //id="app"のclassをクリア
            el.classList = "";
            //id="app"に上で取得したtargetをclass属性で追加
            el.classList.add(target);
        },
        false
    );
}

//ヘッダーアニメーション
$(window).scroll(function(){
	if ($(window).scrollTop() > 20) {
		$('.header_col').addClass('scroll');
	} else {
		$('.header_col').removeClass('scroll');
	}
});

