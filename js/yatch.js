$(document).ready(function(){
  
	var checkExistNewsletter = setInterval(function() {
	  var $item = $('#footer-newsletter');
	  // console.log($item);
	  if ($item.length) {
		  // console.log("Exists 2!");
		  clearInterval(checkExistNewsletter);
		  $('#footer-newsletter').attr('action', '/contact');
	  }
	}, 100); // check every 100ms
  
	if (navigator.userAgent.indexOf('Mac OS X') != -1) {
	  $("body").addClass("u-mac");
	}
  
	$(".Slideshow__Introduce").click((function(e) {
	  e.preventDefault(),
	  $("html,body").animate({
		  scrollTop: ($(window).height() - 81)
	  }, 1000)
	}));
  
	if($('.BlogInformation').length){
	  $('body').addClass('template-blog-information');
	}
  
	if($('#shopify-section-custom-page-recruitment').length){
	  $('body').addClass('template-recruitment');
	}
  
	var $slider = $('.c-featured-collections__slide');
	var $progressBar1 = $('.c-featured-collections__progress');
	var $progressBarLabel1 = $( '.slider__label' );
	var $itemToShow = 2;
	
	$slider.on('init', function(event, slick, currentSlide, nextSlide) {   
	  
	  var calc = ( (1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
  
	  $progressBar1
		.css('background-size', calc + '% 100%')
		.attr('aria-valuenow', calc );
	  
	  $progressBarLabel1.text( calc + '% completed' );
	});
	
	$slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
	  // console.log(slick.slideCount, slick.options.slidesToShow);
	  var calc = ( (nextSlide + 1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
  
	  $progressBar1
		.css('background-size', calc + '% 100%')
		.attr('aria-valuenow', calc );
	  
	  $progressBarLabel1.text( calc + '% completed' );
	});
	
  
	$slider.slick({
	  slidesToShow: 2,
	  slidesToScroll: 1,
	  infinite: false,
	  arrows: true,
	  dots: false,
	  draggable: true,
	  variableWidth: false,
	  appendArrows: '.c-featured-collections__slide-arrow',
	  waitForAnimate: false,
	  responsive: [
		{
		  breakpoint: 768,
		  settings: {
			arrows: true,
			variableWidth: true,
			slidesToShow: 2
		  }
		}
	  ]
	});
	
   
  
	//special slideshow
	var $slider2 = $('.c-slide-special__slideshow');
	var $sliderRelatedProduct = $('.c-slide-special__slideshow1');
	var $progressBar2 = $('.c-slide-special__progress');
	var $progressBarLabel2 = $( '.slider__label_2' );
	var $itemToShow = 3;
  
	$slider2.on('init', function(event, slick, currentSlide, nextSlide) {   
	  
	  var calc = ( (1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
  
	  $progressBar2
		.css('background-size', calc + '% 100%')
		.attr('aria-valuenow', calc );
	  
	  $progressBarLabel2.text( calc + '% completed' );
	});
  
	$slider2.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
	  // console.log(slick);
	  var calc = ( (nextSlide + 1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
	  
	  $progressBar2
		.css('background-size', calc + '% 100%')
		.attr('aria-valuenow', calc );
	  
	  $progressBarLabel2.text( calc + '% completed' );
	});
  
	$sliderRelatedProduct.on('init', function(event, slick, currentSlide, nextSlide) {   
	  // console.log(slick);
	  var calc = ( (1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
	  
	  $progressBar2
		.css('background-size', calc + '% 100%')
		.attr('aria-valuenow', calc );
	  
	  $progressBarLabel2.text( calc + '% completed' );
	});
  
	$sliderRelatedProduct.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
	  // console.log(slick);
	  var calc = ( (nextSlide + 1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
	  
	  $progressBar2
		.css('background-size', calc + '% 100%')
		.attr('aria-valuenow', calc );
	  
	  $progressBarLabel2.text( calc + '% completed' );
	});
  
	$slider2.slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  infinite: false,
	  arrows: true,
	  dots: false,
	  draggable: true,
	  appendArrows: '.c-slide-special__slide-arrow',
	  waitForAnimate: false,
	  responsive: [
		{
		  breakpoint: 768,
		  settings: {
			arrows: true,
			variableWidth: true,
			slidesToShow: 1
		  }
		}
	  ]
	});
	
	$sliderRelatedProduct.slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  infinite: false,
	  arrows: true,
	  dots: false,
	  draggable: true,
	  appendArrows: '.c-slide-special__slide-arrow',
	  responsive: [
		{
		  breakpoint: 768,
		  settings: {
			arrows: true,
  //           variableWidth: true,
			slidesToShow: 2
		  }
		}
	  ]
	});
  
	//End special slideshow
  
	//Featured Collections 3
	function productSlider(){
	  $('.c-featured-collections-3__slide').each(function(){
		var $slider3 = $(this);
		var $progressBar = $(this).parents('.c-featured-collections-3__main').find('.c-featured-collections-3__progress');
		var $progressBarLabel = $(this).parents('.c-featured-collections-3__main').find( '.slider__label_3' );
		var $itemToShow = 4;
  
		$slider3.on('init', function(event, slick, currentSlide, nextSlide) {   
		  // console.log(slick);
		  var calc = ( (1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
		  
		  $progressBar
			.css('background-size', calc + '% 100%')
			.attr('aria-valuenow', calc );
		  
		  $progressBarLabel.text( calc + '% completed' );
		});
		
		$slider3.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
		  // console.log(slick);
		  var calc = ( (nextSlide + 1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
		  
		  $progressBar
			.css('background-size', calc + '% 100%')
			.attr('aria-valuenow', calc );
		  
		  $progressBarLabel.text( calc + '% completed' );
		});
	  
		$slider3.slick({
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  infinite: false,
		  arrows: true,
		  dots: false,
		  draggable: true,
		  appendArrows: $(this).parents('.c-featured-collections-3__main').find('.c-featured-collections-3__slide-arrow'),
		  waitForAnimate: false,
		  responsive: [
			{
			  breakpoint: 768,
			  settings: {
				arrows: true,
				// variableWidth: true,
				slidesToShow: 2
			  }
			}
		  ]
		});
	  });
	}
	productSlider();
	//End Featured Collections 3
  
  
	// Brands list
  
	var $slider4 = $('.c-brands-list__slide');
	var $progressBar4 = $('.c-brands-list__progress');
	var $progressBarLabel4 = $( '.slider__label_4' );
	// var $itemToShow = 6;
  
	$slider4.on('init', function(event, slick, currentSlide, nextSlide) {   
	  // console.log(slick);
	  var calc = ( (1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
	  
	  $progressBar4
		.css('background-size', calc + '% 100%')
		.attr('aria-valuenow', calc );
	  
	  $progressBarLabel4.text( calc + '% completed' );
	});
	
	$slider4.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
	  // console.log(slick);
	  var calc = ( (nextSlide + 1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
	  
	  $progressBar4
		.css('background-size', calc + '% 100%')
		.attr('aria-valuenow', calc );
	  
	  $progressBarLabel4.text( calc + '% completed' );
	});
  
	$slider4.slick({
	  rows: 2,
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  infinite: false,
	  arrows: true,
	  dots: false,
	  draggable: true,
	  variableWidth: true,
	  appendArrows: '.c-brands-list__slide-arrow',
	  waitForAnimate: false,
	  responsive: [
		{
		  breakpoint: 768,
		  settings: {
			arrows: true,
			slidesToShow: 2
		  }
		}
	  ]
	});
  
	// End Brands list
  
	//magazine slideshow
	var $sliderMagazine = $('.c-blog-post-magazine__slideshow');
	var $progressBar5 = $('.c-blog-post-magazine__progress');
	var $progressBarLabel5 = $( '.slider__label_magazine' );
	var $itemToShow = 3;
  
	$sliderMagazine.on('init', function(event, slick, currentSlide, nextSlide) {   
	  // console.log(slick);
	  var calc = ( (1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
	  
	  $progressBar5
		.css('background-size', calc + '% 100%')
		.attr('aria-valuenow', calc );
	  
	  $progressBarLabel5.text( calc + '% completed' );
	});
	
	$sliderMagazine.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
	  // console.log(slick);
	  var calc = ( (nextSlide + 1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
	  
	  $progressBar5
		.css('background-size', calc + '% 100%')
		.attr('aria-valuenow', calc );
	  
	  $progressBarLabel5.text( calc + '% completed' );
	});
  
	$sliderMagazine.slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  infinite: false,
	  arrows: true,
	  dots: false,
	  draggable: true,
	  appendArrows: '.c-blog-post-magazine__slide-arrow',
	  waitForAnimate: false,
	  responsive: [
		{
		  breakpoint: 768,
		  settings: {
			arrows: true,
			variableWidth: true,
			slidesToShow: 1
		  }
		}
	  ]
	});
  
	//End magazine slideshow
  
	// //Color Variations slideshow
  
	var $sliderColorVariant = $('.Product__ColorVariations-box');
	var $progressBar = $('.Product__ColorVariations-progress');
	var $progressBarLabel = $( '.Product__ColorVariations-slider-label' );
	var $itemToShow = 3;
  
	$sliderColorVariant.on('init', function(event, slick, currentSlide, nextSlide) {   
	  // console.log(slick);
	  var calc = ( (1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
	  
	  $progressBar
		.css('background-size', calc + '% 100%')
		.attr('aria-valuenow', calc );
	  
	  $progressBarLabel.text( calc + '% completed' );
	});
	
	$sliderColorVariant.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
	  // console.log(slick);
	  var calc = ( (nextSlide + 1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
	  
	  $progressBar
		.css('background-size', calc + '% 100%')
		.attr('aria-valuenow', calc );
	  
	  $progressBarLabel.text( calc + '% completed' );
	});
  
	$sliderColorVariant.slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  infinite: false,
	  arrows: true,
	  dots: false,
	  draggable: true,
	  appendArrows: '.Product__ColorVariations-slide-arrow',
	  waitForAnimate: false,
	  responsive: [
		{
		  breakpoint: 768,
		  settings: {
			arrows: true,
			slidesToShow: 2
		  }
		}
	  ]
	});
  
	// //End Color Variations slideshow
  
  
  
  
  
  
	// Recommened products
  
	var checkRecommendedProducts = setInterval(function() {
	  var $item = $('.ProductList--carousel1 .Carousel__Cell');
	  if ($item.length) {
		  clearInterval(checkRecommendedProducts);
  
		  var $sliderRecommendedProducts = $('#shopify-section-product-recommendations .ProductList--carousel1');
			var $progressBar6 = $('.ProductList__progress');
			var $progressBarLabel6 = $( '.ProductList-slider__label' );
  
			$sliderRecommendedProducts.on('init', function(event, slick, currentSlide, nextSlide) {   
			  // console.log(slick);
			  var calc = ( (1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
			  
			  $progressBar6
				.css('background-size', calc + '% 100%')
				.attr('aria-valuenow', calc );
			  
			  $progressBarLabel6.text( calc + '% completed' );
			});
			
			$sliderRecommendedProducts.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
			  // console.log(slick);
			  var calc = ( (nextSlide + 1) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
			  
			  $progressBar6
				.css('background-size', calc + '% 100%')
				.attr('aria-valuenow', calc );
			  
			  $progressBarLabel6.text( calc + '% completed' );
			});
  
			$sliderRecommendedProducts.slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  infinite: false,
			  arrows: true,
			  dots: false,
			  draggable: true,
			  appendArrows: '.ProductList__slide-arrow',
			  waitForAnimate: false,
			  responsive: [
				{
				  breakpoint: 768,
				  settings: {
					arrows: true,
					variableWidth: false,
					slidesToShow: 2
				  }
				}
			  ]
			});
  
	  }
	}, 100); // check every 100ms
  
	
  
	// End Recommended products
  
  
  
	$('.Product__DescriptionHeading').addClass("c-show");
	$('.Product__DescriptionHeading').next().slideDown("slow");
	$('.Product__DescriptionHeading').on('click',function(){
	  $(this).next().slideToggle("slow");
	  $(this).toggleClass("c-show");
	});
	$('.Cart__DescriptionHeading').on('click',function(){
	  $(this).next().slideToggle("slow");
	  $(this).toggleClass("c-show");
	});
  
	$('label[for="checkBoxGiftWrapping"]').on('click',function(){
	  $('.Cart__BoxShowHide').slideToggle("slow");
  
	  // if($('#checkBoxGiftWrapping').prop('checked')){
	  //   console.log('checked');
	  //   $('#rteWrappingMessage').val('');
	  //   $('#rteOptionMessage').val('');
	  //   $('#rteSenderMessage').val('');
	  //   $('#GiftCard').prop('checked', false);
	  //   $('#rteCardMessage').val('');
	  // }else{
	  //   console.log('unchecked');
	  //   $('#rteWrappingMessage').val('縺ｪ縺�');
	  //   $('#rteOptionMessage').val('蟇ｾ雎｡蝠�刀蜷�');
	  //   $('#rteSenderMessage').val('繝�せ繝� 繝�せ繝�');
	  //   $('#GiftCard').prop('checked', true);
	  //   $('#rteCardMessage').val('Happy Birthday');
	  // }
  //     if($("#GiftBow").prop("checked")){
  //       $("#GiftBow").prop("checked", false);
  //     }else{
  //       $("#GiftBow").prop("checked", true);
  //     }
  
  //     if($("#GiftCard").prop("checked")){
  //       $("#GiftCard").prop("checked", false);
  //     }else{
  //       $("#GiftCard").prop("checked", true);
  //     }
  //     $(".Cart__GiftOption").slideToggle("slow");
	});
	
	if($(".template-cart").length){
	  $('form[action="/cart"]').submit(function(event){
		if ($("#GiftBow").prop("checked") && ($("#optionbow").val() == "No")) {
		  var span = document.createElement("span");
		  span.className = "error_gift";
		  span.innerText = "驕ｸ謚槭＠縺ｦ縺上□縺輔＞";
		  span.style.color = "red";
		  $(".error_gift").remove();
		  $("#optionbow").parent().append(span);
		  $([document.documentElement, document.body]).animate({
			scrollTop: $("#optionbow").parents(".Cart__GiftOption").offset().top
		  }, 2000);
		  event.preventDefault();
		}
		return;
	  });
	}
  
	$('label[for="GiftBow"]').on('click',function(){
	  $(this).next().slideToggle("slow");
	});
  
	$('label[for="GiftCard"]').on('click',function(){
	  $(this).next().slideToggle("slow");
	});
  
  
  
	$('.Product__DescriptionClose-1').click(function(){
	  $('.Product__DescriptionHeading').trigger('click');
	})
	
  //   $('.stamped-header-title').on('click',function(){
  // 	console.log(55);
  //     $('.stamped-content').slideToggle("slow");
  //     $(this).next().toggleClass("c-show");
  //   });
  
	var checkReviewBox = setInterval(function() {
	  var $item = $('.stamped-reviews');
	  if ($item.length) {
		  // console.log("checkReviewBox!");
		  clearInterval(checkReviewBox);
		  $(".stamped-content").hide();
		  $('.stamped-header-title').addClass("c-show");
		  $('.stamped-content').slideDown("slow");
			 $('.stamped-header-title').on('click',function(){
			$('.stamped-content').slideToggle("slow");
			$(this).next().toggleClass("c-show");
		  });
		  $item.append('<div class="Product__DescriptionCloseBox"><button class="Product__DescriptionClose Product__DescriptionClose-2">髢峨§繧�</button></div>');
		  $('.Product__DescriptionClose-2').click(function(){
			$('.stamped-header-title').trigger('click');
		  })
	  }
	}, 100); // check every 100ms
  
	var checkReviewsPage = setInterval(function() {
	  var $item = $('.p-page-reviews');
	  if ($item.length) {
		  clearInterval(checkReviewsPage);
		  $('header.PageHeader').addClass('c-headerReviewsPage');
	  }
	}, 100); // check every 100ms
	if($(".template-product").length){
	  var checkBundles = setInterval(function() {
		var $item = $('.Product__Bundle .th_pb_add_to_cart.btn.button');
		if ($item.length) {
			clearInterval(checkBundles);
			$('.th_pb_add_to_cart.btn.button .button_text').html('繧ｻ繝�ヨ縺ｧ雉ｼ蜈･縺吶ｋ');
			var $temp = $('.Product__Bundle span.th_pb_total_sale_percentage').text().trim();
			var percent = $temp.slice(0, $temp.length - 1);
	
		}
	  }, 100); // check every 100ms
  
	  var checkBundles1 = setInterval(function() {
		var $item1 = $('.th_pb_specific_bundles .th_pb_total_sale_percentage');
		var $item2 = $('.Product__Bundle .th_pb_specific_bundles .th_pb_old_price .money.th_pb_line_through');
		if ($item1.length && $item2.length) {
			clearInterval(checkBundles1);
			var text = $item1.text();
			var text2 = $item2.text();
			var percent = text.slice(0,text.length - 1);
			var index = text2.lastIndexOf("ﾂ･");
			var oldprice = text2.slice(index + 1,text2.length).replace(',', '');
			$item1.text("ﾂ･" + addThousandsSeparators(parseInt(parseFloat(oldprice) * percent / 100)));
		}
	  }, 100); // check every 100ms
	
	  var checkBundles2 = setInterval(function() {
		var $item = $('.th_pb_add_to_cart.btn.button');
		if ($item.length) {
			clearInterval(checkBundles2);
			$('.th_pb_add_to_cart.btn.button .button_text').html('繧ｻ繝�ヨ縺ｧ雉ｼ蜈･縺吶ｋ');
		}
	  }, 100); // check every 100ms
	  
	  var checkSubmitButton = setInterval(function() {
		var $item = $('#stamped-button-submit');
		if ($item.length) {
			clearInterval(checkSubmitButton);
			$item.prop("value", "繝ｬ繝薙Η繝ｼ繧帝∽ｿ｡縺吶ｋ");
		}
	  }, 100); // check every 100ms
	}
	
	var checkWidgetBold = setInterval(function() {
	  var $item = $('.bsub-widget__wrapper');
	  if ($item.length) {
		  clearInterval(checkWidgetBold);
		  
		  $('span[data-bsub-delivery-frequency]').each(function(){
			$(this).html('驟埼＃');
		  })
  
		  $('.bsub-widget__text').each(function(){
			var text = $(this).text().trim();
			if(text == 'One-time Purchase'){
			  $(this).html('1蠎ｦ髯舌ｊ縺ｮ雉ｼ蜈･');
			}
			
			var flag = false;
			var percent;
			$(this).find('span').each(function(){
			  var textSpan = $(this).text();
			  if(textSpan == 'Subscribe and save'){
  //               console.log(textSpan);
				$(this).text('螳壽悄萓ｿ');
				flag = true;
			  }else if(textSpan.indexOf('%')){
				var index = textSpan.indexOf(' ');
				percent = textSpan.slice(index, textSpan.length - 1).trim();
				$(this).text((percent != "" ? ' ('+percent+')': ""));
			  }
			  if(textSpan.indexOf('ﾂ･') && $(this).hasClass("bsub-widget__group-discount-summary")){
				var index = textSpan.indexOf(' ');
  //               console.log(index);
				percent = textSpan.slice(index, textSpan.length - 1).trim();
				$(this).text((percent != "" ? ' ('+percent+' OFF)': ""));
			  }
			})
  
		  });
	  }
	}, 100); // check every 100ms
  
	// Hide c-recently-viewed-products when don't have item.
	if($(".shopify-section--c-recently-viewed-products").length){
  //     console.log("n");
	  var relatedProductInterval = setInterval(function() {
		var length = $(".shopify-section--c-recently-viewed-products .c-recently-viewed-products #recently-viewed-products .c-recently-viewed-products__item").length;
  //       console.log(length);
		if(length > 0){
		  clearInterval(relatedProductInterval);
		  $(".shopify-section--c-recently-viewed-products").removeClass("shopify-section--c-recently-viewed-products--hide");
		}
	  }, 200);
	}
	// if($(".template-collection,.template-search").length){
	//   // Close Accordion
	//   var closeAccordionInterval = setInterval(function() {
	//     if($(".boost-pfs-filter-option-column-1").length){
	//       clearInterval(closeAccordionInterval);
	//       $(".boost-pfs-filter-option-column-1 .boost-pfs-filter-option-title-heading").trigger("click");
	//     }
	//   }, 200);
	// }
   
	
	//popup recruiment
	
   $(window).on("load resize",function(){
	  if($(".section-popup-recruitment").length){
		$(".section-popup-recruitment").addClass("is-open");
		$(".section-popup-recruitment .popup-recruitment").css({
		  "height": $(window).innerHeight(),
		});
	
		// $(".popup-recruitment__close").on("click",function(){
		//   $(this).parents(".section-popup-recruitment").removeClass("is-open");
		// });
	  }
	});
	
  //   Css Vimeo
  // var iframevimeo = setInterval(function() {
  //   if($(".shogun-video-embed").length){
  //     clearInterval(iframevimeo);
  //   	$(".shogun-video-embed").attr("id","iframe");
  //       var frame = document.getElementById("iframe");
  //       frame.addEventListener("load", ev => {
  //         const new_style_element = document.createElement("style");
  //         new_style_element.textContent = ".vp-controls-wrapper { display: none; }";
  //         console.log(new_style_element);
  //         ev.target.contentWindow.head.appendChild(new_style_element);
  //       });
  //   }
  //   }, 200);
	
	// Background color
   if($(".template-index .ProductItem__ImageWrapper").length){
	  var colors = ["#D7E37A","#E2907E","#A7BBE0"];
	  var count = 0;
	  $(".template-index .ProductItem__ImageWrapper").addClass("unview");
	  $(".template-index .ProductItem__ImageWrapper").each(function(index,val){
		if(count >= 3){
		  count = 0;
		}
		var html = document.createElement("div");
		html.className= "tricolore_cover";
		html.style.backgroundColor = colors[count];
		count ++;
		$(val).append(html);
	  });
	}
  
	if($(".c-slide-special__link").length){
	  var colors = ["#D7E37A","#E2907E","#A7BBE0"];
	  var count = 0;
	  $(".c-slide-special__link").addClass("unview");
	  $(".c-slide-special__link").each(function(index,val){
		if(count >= 3){
		  count = 0;
		}
		var html = document.createElement("div");
		html.className= "tricolore_cover";
		html.style.backgroundColor = colors[count];
		count ++;
		$(val).append(html);
	  });
	}
  
	if($(".c-blog-post-magazine__img").length){
	  var colors = ["#D7E37A","#E2907E","#A7BBE0"];
	  var count = 0;
	  $(".c-blog-post-magazine__img").addClass("unview");
	  $(".c-blog-post-magazine__img").each(function(index,val){
		if(count >= 3){
		  count = 0;
		}
		var html = document.createElement("div");
		html.className= "tricolore_cover";
		html.style.backgroundColor = colors[count];
		count ++;
		$(val).append(html);
	  });
	}
  
  // =======================================================================================================================
  // Set default please choose
  // End
  // =======================================================================================================================
  
  // =======================================================================================================================
  // Set hide progress bar when not enough product
  setTimeout(function() {
	if($("div[role='progressbar']").length){
	  $("div[role='progressbar']").each(function(index,val){
	   if(!$(val).parents(".c-recently-viewed-products__bottom-box").length){
		  var progress = $(val).attr("aria-valuenow");
		  if(progress == undefined || progress < 0 || progress == 100){
			$(val).hide();
		  }
		}
	  });
	}
  }, 0);
  // End
  // =======================================================================================================================
  
  
  // =======================================================================================================================
  // Set dropdown
	if($(".single-option-selector").length){
	  $(".single-option-selector").on("change",function(){
		$(".Popover__Value[data-value='"+ $(this).val() +"']").trigger("click");
	  });
	}
  // End
  // =======================================================================================================================
  // =======================================================================================================================
  // Button wishlisthero
  if($(".add_wishlist").length){
	$(".add_wishlist").on("click",function(){
	  $(".wishlisthero-product-page-button-container .Button").trigger("click");
	  var checkAddWishlist = setInterval(() => {
		if($(".MuiSnackbar-root").length){
		  clearInterval(checkAddWishlist);
		  if($("#message-id").length){
			$(this).text("縺頑ｰ励↓蜈･繧翫↓霑ｽ蜉�");
		  }else{
			$(this).text("縺頑ｰ励↓蜈･繧翫↓霑ｽ蜉�縺励∪縺励◆");
		  }
		}
	  }, 200);
	});
  }
  // End
  // =======================================================================================================================
	$(window).on("load",function(){
	 
  
	  // if($(".c-slide-special__link").length){
	  //   $(".c-slide-special__link").removeClass("unview");
	  // }
  
	  // if($(".c-blog-post-magazine__img").length){
	  //   $(".c-blog-post-magazine__img").removeClass("unview");
	  // }
  
	  if($(".Product__ColorVariations-box .tricolore_cover").length){
		$(".Product__ColorVariations-box .tricolore_cover").addClass("view");
	  }
	});
	if($(".c-featured-collections").length){
	  var sc1Position = $(".c-featured-collections").offset().top;
	  // $(".c-featured-collections .slick-track").addClass("hidden-slide");
	  $(".c-featured-collections").each(function(index,val){
		var count = 330;
		$(val).find(".c-featured-collections__item").each(function(key,v){
		  $(v).css({
			"transform": "translate3d("+(count)+"px, 0px, 0px)",
			"opacity" : 0,
			"transition": "1.2s cubic-bezier(0.215, 0.61, 0.355, 1)"
		  });
		  count += 50;
		});
	  });
	}
	if($(".c-slide-special").length){
	  var sc2Position = $(".c-slide-special").offset().top;
	  // $(".c-slide-special .slick-track").addClass("hidden-slide");
	  $(".c-slide-special").each(function(index,val){
		var count = 330;
		$(val).find(".c-slide-special__item").each(function(key,v){
		  $(v).css({
			"transform": "translate3d("+(count)+"px, 0px, 0px)",
			"opacity" : 0,
			"transition": "1.2s cubic-bezier(0.215, 0.61, 0.355, 1)"
		  });
		  count += 50;
		});
	  });
	}
	if($(".c-featured-collections-3").length){
	  var sc3Position = $(".c-featured-collections-3").offset().top;
	  // $(".c-featured-collections-3 .slick-track").addClass("hidden-slide");
	 
	  $(".c-featured-collections-3").each(function(index,val){
	  var count = 330;
		$(val).find(".c-featured-collections-3__item").each(function(key,v){
		  $(v).css({
			"transform": "translate3d("+(count)+"px, 0px, 0px)",
			"opacity" : 0,
		  });
		  count += 50;
		});
	  });
	}
	if($(".c-blog-post-magazine").length){
	  var sc4Position = $(".c-blog-post-magazine").offset().top;
	  // $(".c-blog-post-magazine .slick-track").addClass("hidden-slide");
	  $(".c-blog-post-magazine").each(function(index,val){
		var count = 330;
		$(val).find(".c-blog-post-magazine__item").each(function(key,v){
		  $(v).css({
			"transform": "translate3d("+(count)+"px, 0px, 0px)",
			"opacity" : 0,
			"transition": "1.2s cubic-bezier(0.215, 0.61, 0.355, 1)"
		  });
		  count += 50;
		});
	  });
	}
  
   
  
  
	 var Addoffset = 0;
	$(window).on("load scroll",function(){
	  var wd = $(window).height() + $(window).scrollTop();
	  // sc1Position
	  if(wd > sc1Position + Addoffset){
		setTimeout(function(){
		  if($(".c-featured-collections").length){
			// $(".c-featured-collections .slick-track").removeClass("hidden-slide");
			$(".c-featured-collections").each(function(index,val){
			  $(val).find(".c-featured-collections__item").each(function(key,v){
				$(v).css({
				  "transform": "none",
				  "opacity" : 1
				});
			  });
			});
			$(".c-featured-collections .ProductItem__ImageWrapper").removeClass("unview");
		  }
		}, 300);
		 setTimeout(function(){
		  $(".c-featured-collections .tricolore_cover").addClass("view");
		}, 800)
	  }
	  // sc2Position
	  if(wd > sc2Position + Addoffset){
		setTimeout(function(){
		  if($(".c-slide-special").length){
			// $(".c-slide-special .slick-track").removeClass("hidden-slide");
			$(".c-slide-special").each(function(index,val){
			  $(val).find(".c-slide-special__item").each(function(key,v){
				$(v).css({
				  "transform": "none",
				  "opacity" : 1,
				});
			  });
			});
			$(".c-slide-special .c-slide-special__link").removeClass("unview");
		  }
		}, 300);
		 setTimeout(function(){
		  $(".c-slide-special .tricolore_cover").addClass("view");
		}, 800)
	  }
	  // sc3Position
	  $(".c-featured-collections-3").each(function(index,val){
		if(wd > $(val).offset().top + Addoffset){
		  setTimeout(function(){
			if($(val).length){
			  $(val).find(".c-featured-collections-3__item").each(function(key,v){
				$(v).css({
				  "transform": "none",
				  "opacity" : 1,
				});
			  });
			  // $(val).find(".slick-track").removeClass("hidden-slide");
			  $(val).find(".ProductItem__ImageWrapper").removeClass("unview");
			}
		  }, 300);
		   setTimeout(function(){
			$(val).find(".tricolore_cover").addClass("view");
		  }, 800)
		}
	  });
	  // sc4Position
	  if(wd > sc4Position + Addoffset){
		setTimeout(function(){
		  if($(".c-blog-post-magazine").length){
			// $(".c-blog-post-magazine .slick-track").removeClass("hidden-slide");
			$(".c-blog-post-magazine").each(function(index,val){
			  $(val).find(".c-blog-post-magazine__item").each(function(key,v){
				$(v).css({
				  "transform": "none",
				  "opacity" : 1,
				});
			  });
			});
			$(".c-blog-post-magazine .c-blog-post-magazine__img").removeClass("unview");
		  }
		}, 300);
		 setTimeout(function(){
		  $(".c-blog-post-magazine .tricolore_cover").addClass("view");
		}, 800)
	  }
	  // sc5Position
	  // custom-recently-viewed-products.liquid
  
	  // Show ScrollTop button
	  if(wd > ($("body").height() / 2)){
		$(".scroll_top").addClass("is_active");
	  }else{
		$(".scroll_top").removeClass("is_active");
	  }
	});
  
  // menu header
	if($(".HorizontalList").length){
	  $(".HorizontalList__Item").on("mouseleave",function(){
		var dropmenu = $(this).find(".DropdownMenu");
		if(dropmenu.length){
		  $(this).removeClass("is-expanded");
		  dropmenu.attr("aria-hidden","true");
		}
	  })
	}
  //   Add action hover product
	if($(".ProductItem__ImageWrapper").length){
		$(".ProductItem__ImageWrapper").addClass("product-hover");
	}
	
	if($(".brand-thumbnail").length){
		$(".brand-thumbnail").addClass("product-hover");
	}
	 if($(".BlogMagazine__Thumbnail").length){
		$(".BlogMagazine__Thumbnail").addClass("product-hover");
	}
  //   End
	
  // Hide Star Reviews if don't have review
  $(window).on("load",function(){
	setTimeout(function(){
	  if($(".stamped-badge-caption").length){
	   
		$(".stamped-badge-caption").each(function(index,val){
		  var data_reviews = $(val).data("reviews");
		  if(data_reviews <= 0){
			$(val).parents(".stamped-main-badge").hide();
		  }
		});
		
	  }
	}, 1000);
  });
  // End
  
  // Scroll top
  if($(".scroll_top").length){
	$(".scroll_top").on("click",function(){
	  $('html, body').animate({scrollTop:0}, '300');
	});
  }
  // End
  // ===================================================================================================
  // Loading
  if($(".loading").length && !sessionStorage.getItem("onpage")){
	$(".PageContainer").css("opacity",0);
	$(".loading").addClass("is_active");
	if($("#main").length){
	  $("#main").css("opacity",0);
	}
  }
  
  if($(".loading").length && sessionStorage.getItem("onpage")){
	$(".loading").removeClass("is_active");
  }
  
  // End
  // ===================================================================================================
  // Main visual parallax
  // if($("#section-slideshow").length){
  //   $(window).on("scroll",function(){
  //     console.log($(this).scrollTop() / 2);
  //     $(".Slideshow__Slide").css('transform','translateY('+ $(this).scrollTop() / 2 +'px)');
  //   });
  // }
  // ===================================================================================================
  // ===================================================================================================
  // Wish list style
  var wishlistStyle = setInterval(function() {
	if($(".MuiCardHeader-title").length){
	  clearInterval(wishlistStyle);
	  $(".MuiCardHeader-title a").css({
		"font-family": "a-otf-midashi-go-mb31-pr6n,sans-serif !important",
		"font-weight": "600",
		"font-style": "normal"
	  });
	}
  }, 200);
  // ===================================================================================================
  // ===================================================================================================
  // Position error Form
  $(window).on("load resize",function(){
	var form_erros_position = setInterval(function(){
	  if($(".globo-formbuilder").length){
		clearInterval(form_erros_position);
		$(".globo-formbuilder .classic-input").each(function(index,val){
		  var element =  $(val).position();
		  $(val).next().css({
			"left": element.left,
		  });
		});
	  }
	}, 200);
  });
  // End
  // ===================================================================================================
  // ===================================================================================================
  // Check Bundle Products
  if($(".template-product").length){
	if($(".th_pb_list_ul").length == 0){
	  $(".Product__Bundle").hide();
	}
  }
  // End
  // ===================================================================================================
  // ===================================================================================================
  // Check login form reviews
  if($(".template-product").length){
	var loginformreview = setInterval(function(){
	  if($(".new-review-form").length){
		clearInterval(loginformreview);
		if(login == false){
		  var Div = document.createElement("div");
		  var TagA = document.createElement("a");
		  var TagAText = document.createTextNode("繝ｭ繧ｰ繧､繝ｳ");
		  Div.className = "mark_login_review_form";
		  TagA.className = "button_login_review_form";
		  TagA.href = routerLogin;
		  TagA.appendChild(TagAText);
		  Div.appendChild(TagA);
		  $(".new-review-form").append(Div);
		}else{
		  $(".new-review-form").find("input[name='author']").val(customerName != "" ? customerName : "Empty");
		  $(".new-review-form").find("input[name='email']").val(customerEmail != "" ? customerEmail : "Empty");
		}
	  }
	}, 200);
  }
  
  // End
  // ===================================================================================================
  // Destroy slide related Product
  if($(".template-article").length && $('#related_products_slider').length){
	var destroySlide = setInterval(function() {
		try {
		  $('#related_products_slider').data('owlCarousel').destroy();
		  $('#related_products_slider').removeClass("owl-carousel");
		  StampedFn.loadBadges();
		  clearInterval(destroySlide);
	  } catch (error) {
		console.log(error.message);
	  }
	}, 200);  
  }
  // End
  // ===================================================================================================
  // Remove prefix tags
  if($(".template-collection,.template-search").length){
	var removePrefixTags = setInterval(function (){
	  if($(".boost-pfs-filter-option-value").length){
		$(".boost-pfs-filter-option-value").each(function(index,val){
		  var text = $(val).text().toLowerCase().trim();
		  if(text.indexOf("magazineitem__") > -1){
			$(val).parents(".boost-pfs-filter-option-item").remove();
		  }
		  else if(text.indexOf("magazine__") > -1){
			$(val).parents(".boost-pfs-filter-option-item").remove();
		  }
		  else if(text.indexOf("color__") > -1){
			$(val).parents(".boost-pfs-filter-option-item").remove();
		  }
		  else if(text.indexOf("c__sale") > -1){
			$(val).parents(".boost-pfs-filter-option-item").remove();
		  }
		  else if(text.indexOf("__") > -1){
			var text = text.split("__")[1];
			$(val).text(text)
		  }
		});
	  }
	  if($(".refine-by-value").length){
		$(".refine-by-value").each(function(index,val){
		  var text = $(val).text();
		  if(text.indexOf("__") > 0){
			var text = text.split("__")[1];
			$(val).text(text)
		  }
		});
	  }
	}, 200);
  }
  if($(".breadcrumb").length){
	$(".breadcrumb span").each(function(index,val){
	  var text = $(val).text();
	  if(text.indexOf("__") > 0){
		var text = text.split("__")[1];
		$(val).text(text);
	  }
	});     
  }
  // End
  // ===================================================================================================
  if($(".referral_program_details").length){
	$(".referral_program_details").css("color","#c9c6c6");
	var checkSmileCustomer = setInterval(function() {
	  try {
		Smile.customer.referral_url;
		// Smile.fetchCustomer({ include: 'vip_tier' }).then((customer) => {
		//   if($(".miles_rank").length){
		//     $(".miles_rank").text(customer.vip_tier.name);
		//   }
		// });
		clearInterval(checkSmileCustomer);
		if(Smile.customer.points_balance < 10){
		  $(".miles_rank").text("White");
		}
		else if(Smile.customer.points_balance < 50){
		  $(".miles_rank").text("Bronze");
		}
		else if(Smile.customer.points_balance < 100){
		  $(".miles_rank").text("Silver");
		}
		else{
		  $(".miles_rank").text("Gold");
		}
		clearInterval(checkSmileCustomer);
		$(".referral_program_details").css("color","#000");
		$(".referral_program_details").on("click",function(){
			copyToClipboard(Smile.customer.referral_url);
			alert("邏ｹ莉九Μ繝ｳ繧ｯ繧偵さ繝斐�縺励∪縺励◆: " + Smile.customer.referral_url);
		});
	  } catch (error) {
		console.log(error.message);
	  }
	}, 200);
  }
  function copyToClipboard(text) {
	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val(text).select();
	document.execCommand("copy");
	$temp.remove();
  }
  // ===================================================================================================
  // Menu background random color
  if($(".Header__Icon.Icon-Wrapper--clickable").length){
	$(".Header__Icon.Icon-Wrapper--clickable").on("click",function(){
	  var colors = ["#D7E37A","#E2907E","#A7BBE0"];
	  $("#shopify-section-sidebar-menu .Drawer").css("background-color",colors[Math.floor(Math.random() * 3)]);
	
	  if($(".SidebarMenu").attr("aria-hidden") == "true"){
		$(".Collapsible__Button").removeClass("is_active");
		$(".Collapsible__Button").next().slideUp();
	  }
	});
  }
  // End
  // ===================================================================================================
  // Search blogs
  if($('.template-search').length){
	setTimeout(function() {
	  var activeArticle = setInterval(function() {
		var url_string = window.location.href; //window.location.href
		var url = new URL(url_string);
		var type = url.searchParams.get("type");
		if(type == "article" && $(".boost-pfs-search-result-panel-controls").length){
		  clearInterval(activeArticle);
		  $(".boost-pfs-search-result-panel-controls").find(".boost-pfs-search-result-panel-item:last-child").trigger("click");
		}
	  }, 200);
	}, 0);
  }
  // ===================================================================================================
  // Guide Scroll
  if($("#shopify-section-page-guide-template").length){
	try{
	  var url = window.location.href;
	  var target ="";
	  url = url.split("#").pop();
	  target = $("#"+url).data("id");
	  if(target != undefined){
		$([document.documentElement, document.body]).animate({
		  scrollTop: $("#"+ target).offset().top
		}, 2000);
		$("#"+url).parents(".Faq__Item").attr("aria-expanded","true");
		$("#"+url).attr("aria-hidden","false");
		$("#"+url).css("height", $("#"+url).find(".Faq__Answer").innerHeight());
		$(".FaqSummary__Item").removeClass("is-active");
		$("a[href='#"+target+"']").parent().addClass("is-active");
	  }else{
		$([document.documentElement, document.body]).animate({
		  scrollTop: $("#"+ url).offset().top
		}, 2000);
		$(".FaqSummary__Item").removeClass("is-active");
		$("a[href='#"+url+"']").parent().addClass("is-active");
	  }
	}
	catch(err){
		console.log(err.message);
	}
  }
  // End
  // ===================================================================================================
  // ===================================================================================================
  // Magazine Hide tags number
  if($(".Article__Keywords").length){
	$(".Article__tags a").each(function(){
	  var text = $(this).text();
	  var validate = isNumeric(text);
	  if(validate){
		$(this).remove();
	  }
	});
	function isNumeric(num){
	  return !isNaN(num)
	}
  }
  // ===================================================================================================
  // ===================================================================================================
  // Change options color collections
  // var optionsColor = setInterval(function() {
  //   if($(".boost-pfs-filter-option-swatch-image").length){
  //     var colors = {
  //       "Blue":"#69ABD6",
  //       "Green":"#B1D568",
  //       "Yellow":"#F2E63E",
  //       "Red":"#E15D5D",
  //       "Pink":"#D96095",
  //       "Purple":"#AD7EC8",
  //       "Black":"#121212",
  //       "Gray":"#CBCBCB",
  //       "White":"#FFFFFF",
  //     };
  //     $(".boost-pfs-filter-option-swatch-image").each(function(index,val){
  //       var color = $(val).attr("title");
  //       $(val).css("background-color",colors[color]);
  //     });
  //   }
  // }, 200);
  
  // End
  // ===================================================================================================
  // ===================================================================================================
  // Search Bar SP
	if($("#SearchMenu").length){
	  var searchElement = document.getElementById('SearchMenu');
	  var searchResultsElement = searchElement.querySelector('.Search__Results');
	  var queryMap = {};
	  $("#SearchMenu .Search__Input").on("keyup",function(event){
		var _this24 = this;
  
		  if (event.keyCode === 13) {
			return;
		  }
  
		  // Unfortunately, fetch does not support as of today cancelling a request. As a consequence what we do is that we manually
		  // keep track of sent requests, and only use the results of the last one
		  var lastInputValue = event.target.value;
  
		  if (lastInputValue === '') {
			_resetSearch();
			return;
		  }
  
		  var queryOptions = { method: 'GET', credentials: 'same-origin' };
  
		  var queries = [fetch(window.routes.searchUrl + '?view=ajax&q=' + encodeURIComponent(lastInputValue) + '*&type=product', queryOptions)];
  
		  if (window.theme.searchMode !== 'product') {
			queries.push(fetch(window.routes.searchUrl + '?view=ajax&q=' + encodeURIComponent(lastInputValue) + '*&type=' + window.theme.searchMode.replace('product,', ''), queryOptions));
		  }
  
		  queryMap[lastInputValue] = true;
  
		  document.dispatchEvent(new CustomEvent('theme:loading:start'));
  
		  Promise.all(queries).then(function (responses) {
			// If we receive the result for a query that is not the last one, we simply do not process the result
			if (lastInputValue !== event.target.value) {
			  return;
			}
  
			delete queryMap[event.target.value];
  
			Promise.all(responses.map(function (response) {
			  return response.text();
			})).then(function (contents) {
			  // If we have only one content then we only have product, otherwise we have products and articles
			  if (window.theme.searchMode === 'product') {
				searchResultsElement.innerHTML = contents[0];
			  } else {
				searchResultsElement.innerHTML = '<div class="PageLayout PageLayout--breakLap">\n              <div class="PageLayout__Section">' + contents[0] + '</div>\n              <div class="PageLayout__Section PageLayout__Section--secondary">' + contents[1] + '</div>\n            </div>';
			  }
  
			  searchResultsElement.setAttribute('aria-hidden', 'false');
			});
  
			document.dispatchEvent(new CustomEvent('theme:loading:end'));
		  });
	  })
  
	  function _resetSearch() {
		if (window.theme.searchMode === 'product') {
		  searchResultsElement.innerHTML = '';
		} else {
		  searchResultsElement.innerHTML = '<div class="PageLayout PageLayout--breakLap">\n              <div class="PageLayout__Section"></div>\n              <div class="PageLayout__Section PageLayout__Section--secondary"></div>\n            </div>';
		}
  
		searchResultsElement.setAttribute('aria-hidden', 'true');
  
		document.dispatchEvent(new CustomEvent('theme:loading:end')); // Just in case
	  }
	}
  // End
  // ===================================================================================================
  
  
  var timeoutSlideShow = 5500;
  if($(window).width() <= 768){
	var timeoutSlideShow = 6500;
  }
  if(!sessionStorage.getItem("onpage")){
	setTimeout(function() {
	  $(".Slideshow__Carousel").on("init reInit afterChange beforeChange", function (
		event,
		slick,
		currentSlide,
		nextSlide
	  ) {
		//currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
		var index = currentSlide ? currentSlide : 0;
	
		var autoplaySpeed = slick.slickGetOption("autoplaySpeed");
		var speed = slick.slickGetOption("speed");
		var controller = $(".slider-dots-box");
		var slider = $(".Slideshow__Slide");
		var r = 20;
		var stroke = 1;
		var width = r * 2 + stroke;
		var anchor = width / 2;
		var rlen = 2 * Math.PI * r;
	
		if (event.type == "init") {
		  var current_html = "";
		  for (let i = 1; i <= slick.slideCount; i++) {
			current_html += "<button>";
			current_html +=
			  '<svg class="progress" width="' +
			  width +
			  '" height="' +
			  width +
			  '"><circle r="' +
			  r +
			  '" cx="' +
			  anchor +
			  '" cy="' +
			  anchor +
			  '" style="stroke-dasharray: ' +
			  rlen +
			  "px; stroke-dashoffset: " +
			  rlen +
			  "px; stroke-width: " +
			  stroke +
			  'px"/></svg>';
			current_html += "<span>" + i + "</span>";
			current_html += "</button>";
		  }
		  controller.html(current_html);
		  controller.children().eq(index).find("circle").stop().animate(
			{
			  "stroke-dashoffset": "0px"
			},
			autoplaySpeed
		  );
	
		  controller.find("button").each(function (item, index) {
			$(this).click(function () {
			  slick.slickGoTo(item);
			});
		  });
		   var image = slider.eq(index).find(".Slideshow__Image");
		  image.addClass("animate_scale");
		}
		if (event.type == "beforeChange") {
		  var target = controller.children().eq(index).find("circle");
		   slider.eq(nextSlide).find(".Slideshow__Image").addClass("animate_scale");
		  target.css({
			"stroke-dashoffset": "0px"
		  });
		  target.stop().animate(
			{
			  "stroke-dashoffset": -rlen + "px"
			},
			speed
		  );
		}
		if (event.type == "afterChange") {
		  var target = controller.children().eq(index).find("circle");
		  var prevIndex = index - 1 < 0 ? slick.slideCount - 1 : index - 1;
		  slider.eq(prevIndex).find(".Slideshow__Image").removeClass("animate_scale");
		  target.css({
			"stroke-dashoffset": rlen + "px"
		  });
		  target.stop().animate(
			{
			  "stroke-dashoffset": "0px"
			},
			autoplaySpeed
		  );
		}
	  });
	
	
	  $('.Slideshow__Carousel').slick({
		  arrows: false,
		  infinite: true,
		  dots: false,
		  draggable: true,
		  focusOnSelect: true,
		  autoplay: true,
		  autoplaySpeed:5000,
		  pauseOnHover: false,
		  pauseOnFocus: false,
		  fade:true
	  });
	}, timeoutSlideShow);
  }else{
	$(".Slideshow__Carousel").on("init reInit afterChange beforeChange", function (
	  event,
	  slick,
	  currentSlide,
	  nextSlide
	) {
	  //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
	  var index = currentSlide ? currentSlide : 0;
  
	  var autoplaySpeed = slick.slickGetOption("autoplaySpeed");
	  var speed = slick.slickGetOption("speed");
	  var controller = $(".slider-dots-box");
	  var slider = $(".Slideshow__Slide");
	  var r = 20;
	  var stroke = 1;
	  var width = r * 2 + stroke;
	  var anchor = width / 2;
	  var rlen = 2 * Math.PI * r;
  
	  if (event.type == "init") {
		var current_html = "";
		for (let i = 1; i <= slick.slideCount; i++) {
		  current_html += "<button>";
		  current_html +=
			'<svg class="progress" width="' +
			width +
			'" height="' +
			width +
			'"><circle r="' +
			r +
			'" cx="' +
			anchor +
			'" cy="' +
			anchor +
			'" style="stroke-dasharray: ' +
			rlen +
			"px; stroke-dashoffset: " +
			rlen +
			"px; stroke-width: " +
			stroke +
			'px"/></svg>';
		  current_html += "<span>" + i + "</span>";
		  current_html += "</button>";
		}
		controller.html(current_html);
		controller.children().eq(index).find("circle").stop().animate(
		  {
			"stroke-dashoffset": "0px"
		  },
		  autoplaySpeed
		);
  
		controller.find("button").each(function (item, index) {
		  $(this).click(function () {
			slick.slickGoTo(item);
		  });
		});
		  var image = slider.eq(index).find(".Slideshow__Image");
		image.addClass("animate_scale");
	  }
	  if (event.type == "beforeChange") {
		var target = controller.children().eq(index).find("circle");
		  slider.eq(nextSlide).find(".Slideshow__Image").addClass("animate_scale");
		target.css({
		  "stroke-dashoffset": "0px"
		});
		target.stop().animate(
		  {
			"stroke-dashoffset": -rlen + "px"
		  },
		  speed
		);
	  }
	  if (event.type == "afterChange") {
		var target = controller.children().eq(index).find("circle");
		var prevIndex = index - 1 < 0 ? slick.slideCount - 1 : index - 1;
		slider.eq(prevIndex).find(".Slideshow__Image").removeClass("animate_scale");
		target.css({
		  "stroke-dashoffset": rlen + "px"
		});
		target.stop().animate(
		  {
			"stroke-dashoffset": "0px"
		  },
		  autoplaySpeed
		);
	  }
	});
  
  
	$('.Slideshow__Carousel').slick({
		arrows: false,
		infinite: true,
		dots: false,
		draggable: true,
		focusOnSelect: true,
		autoplay: true,
		autoplaySpeed:7000,
		pauseOnHover: false,
		pauseOnFocus: false,
		fade:true
	});
  }
	
  
	// Custom Navigation 
  
	$('.Drawer__Main .Collapsible').each(function(){
  //     var content = $(this).find('.Collapsible__Button').text();
  //     $(this).find('.Collapsible__Button').html(content);
	   var content = $(this).find('a.Collapsible__Button.Heading.u-h6').text();
	  $(this).find('a.Collapsible__Button.Heading.u-h6').html(content);
	  var content = $(this).find('button.Collapsible__Button.Heading.u-h6 a').text();
	  $(this).find('button.Collapsible__Button.Heading.u-h6 a').html(content);
	 $(this).find('button.Collapsible__Button.Heading.u-h6 .Collapsible__Plus').remove();
	  
	 var arrow = document.createElement("i");
	 arrow.className = "arrow-menu";
	  arrow.style.width = "calc(100% - "+ $(this).find('button.Collapsible__Button.Heading a').width() +"px)";
	 $(this).find('button.Collapsible__Button.Heading').append(arrow);
	})
	
   $('.Drawer__Main .Collapsible__child-1').each(function(){
	  if( $(this).find(".arrow-menu")){
		$(this).find(".arrow-menu").remove();
	  }
	  var arrow = document.createElement("i");
	  arrow.className = "arrow-menu";
		arrow.style.width = "calc(100% - "+ $(this).find('button.Collapsible__Button.Heading a').width() +"px)";
	  $(this).find('button.Collapsible__Button.Heading').append(arrow);
	});
	
  //   if($(".Collapsible").length && $(window).width() > 1024){
  //     $(".Collapsible").on("mouseenter",function(){
  //       $(this).find(".Collapsible__Inner").css({
  //         "height": $(this).find(".Collapsible__Inner .Collapsible__Content").innerHeight(),
  //         "visibility": "visible"
  //       });
  //       $(this).find(".Collapsible__Button").addClass("is_active");
  //     });
  //     $(".Collapsible").on("mouseleave",function(){
  //       $(this).find(".Collapsible__Inner").css({
  //         "height":0,
  //         "visibility": "hidden"
  //       });
  //       $(this).find(".Collapsible__Button").removeClass("is_active");
  //     });
  //   }
	
	 if($(".Collapsible .Collapsible__Button").length){
	  $(".Collapsible").children(':first-child').find(".arrow-menu").on("click",function(){
		if($(this).parent().hasClass("is_active")){
		  $(this).parent().next().slideUp({
			complete:function(){
			  $(this).css("height","auto");
			}
		  });
		  $(this).parent().attr("aria-expanded","true");
		  $(this).parent().removeClass("is_active");
		  
		}else{
		  $(this).parent().attr("aria-expanded","false");
		  $(this).parent().next().slideDown({
			complete:function(){
			  $(this).parent().css("height","auto");
			}
		  });
		  $(this).parent().addClass("is_active");
		  
		  $('.Drawer__Main .Collapsible__child-1').each(function(){
			$(this).find('button.Collapsible__Button.Heading .arrow-menu').css("width","calc(100% - "+ $(this).find('button.Collapsible__Button.Heading a').width() +"px)");
		  });
		}
	  });
	}
  
	if($(".Collapsible__child-1").length && $(window).width()){
	  $(".Collapsible__child-1 .Collapsible__Button .arrow-menu").on("click",function(){
		if($(this).parent().hasClass("is_active")){
		  $(this).parent().next().slideUp({
			duration:600,
			complete:function(){
			  $(this).parent().css("height","auto");
			}
		  });
		  $(this).parent().removeClass("is_active");
		}else{
		  $(this).parent().next().slideDown({
			duration:600,
			complete:function(){
			  $(this).parent().css("height","auto");
			}
		  });
		  $(this).parent().addClass("is_active");
		}
	  });
	}
	
	if($(".Collapsible__child-1").length && $(window).width() <= 768){
	  $(".Collapsible__child-1").find(".Collapsible__Button").attr("aria-expanded","true");
	  $(".Collapsible__child-1").find(".Collapsible__Button").addClass("is_active");
	  $(".Collapsible__child-1").each(function(index,val){
		$(val).find(".Collapsible__Inner").slideDown();
	  });
	}
  
	// End custom navigation 
  
	// Slick for product details 
	if($('.Product__Slideshow').length){
	  var flkty = Flickity.data('.Product__Slideshow');
  
	  var NavScroller = $('.Product__SlideshowNavScroller').children();
	  var $progressBarSlideshow = $('.Product__Slideshow-progress');
	  NavScroller.each(function(index) {
		$(this).click(function() {
		  flkty.selectCell(index);
		  NavScroller.removeClass('is-selected');
		  $(this).addClass('is-selected');
		})
	  })
	  // flkty.on( 'scroll', function( progress ) {
	  //   console.log(progress);
	  //   progress = Math.max( 0, Math.min( 1, progress ) );
	  //   $progressBarSlideshow
	  //           .css('background-size', progress * 100 + '% 100%')
	  //           .attr('aria-valuenow', progress * 100);
	  // });
	  var calc = ( 1 / (flkty.slides.length) ) * 100;
		
	  $progressBarSlideshow
		  .css('background-size', calc + '% 100%')
		  .attr('aria-valuenow', calc );
	  flkty.on( 'scroll', function(index) {
		var position = $(".Product__SlideItem.is-selected").data("media-position");
		var calc = ( position / (flkty.slides.length) ) * 100;
		  
		  $progressBarSlideshow
			.css('background-size', calc + '% 100%')
			.attr('aria-valuenow', calc );
	  });
	
	  $('.Product__SlideshowNavigation .slick-prev').on( 'click', function() {
		flkty.previous();
	  });
	  $('.Product__SlideshowNavigation .slick-next').on( 'click', function() {
		flkty.next();
	  });
	}
	
  //   var $progressBarSlideshow = $('.Product__Slideshow-progress');
  //   flkty.on( 'settle', function(index) {
  //     console.log(index);
  //     // var calc = ( index / (flkty.slides.length - 1) ) * 100;
		
  //     // console.log(index, $progressBarSlideshow, calc)
  //     //   $progressBarSlideshow
  //     //     .css('background-size', calc + '% 100%')
  //     //     .attr('aria-valuenow', calc );
  //   });
  //   console.log(flkty);
	// $('.Product__SlideshowNavScroller').slick({
	  // 	// autoplay: true,
	  // 	vertical: true,
	  // 	infinite: false,
	  // 	verticalSwiping: true,
	  // 	slidesToShow: 5,
	  // 	focusOnSelect: true,
	//   arrows: false,
	//   adaptiveHeight: true,
	  // 	responsive: [
	  // 		{
	  // 			breakpoint: 767,
	  // 			settings: {
	  // 				vertical: false,
	  // 			}
	  // 		},
	  // 		{
	  // 			breakpoint: 479,
	  // 			settings: {
	  // 				vertical: false,
	  // 				slidesPerRow: 3,
	  // 				slidesToShow: 3,
	  // 			}
	  // 		},
	  // 	]
	  // });
  
	// var $sliderProduct = $('.Product__Slideshow');
	// var $progressBarSlideshow = $('.Product__Slideshow-progress');
	// var $progressBarLabelSlideshow = $( '.Product__Slideshow-slider-label' );
	// var $itemToShow = 3;
	
	// $sliderProduct.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
	//   console.log(slick);
	//   var calc = ( (nextSlide) / ((slick.slideCount + 1) - (slick.options.slidesToShow)) ) * 100;
	  
	//   $progressBarSlideshow
	//     .css('background-size', calc + '% 100%')
	//     .attr('aria-valuenow', calc );
	  
	//   $progressBarLabelSlideshow.text( calc + '% completed' );
	// });
  
	  // $sliderProduct.slick({
	//   slidesToShow: 1,
	//   slidesToScroll: 1,
	//   arrows: false,
	//   fade: true,
	//   asNavFor: '.Product__SlideshowNavScroller',
	  // 	responsive: [
	  // 		{
	  // 			breakpoint: 768,
	  // 			settings: {
	//         arrows: true,
	//         appendArrows: '.Product__Slideshow-slide-arrow',
	  // 				vertical: false,
	  // 				fade: false,
	  // 			}
	  // 		},
	  // 	]
	  // });
  
	// End slick for product details 
	
	var x, i, j, l, ll, selElmnt, a, b, c;
  /* Look for any elements with the class "custom-select": */
  x = document.getElementsByClassName("BlogMagazine__SelectBox");
  l = x.length;
  for (i = 0; i < l; i++) {
	selElmnt = x[i].getElementsByTagName("select")[0];
	ll = selElmnt.length;
	/* For each element, create a new DIV that will act as the selected item: */
	a = document.createElement("DIV");
	a.setAttribute("class", "select-selected");
	a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
	x[i].appendChild(a);
	/* For each element, create a new DIV that will contain the option list: */
	b = document.createElement("DIV");
	b.setAttribute("class", "select-items select-hide");
	for (j = 0; j < ll; j++) {
	  /* For each option in the original select element,
	  create a new DIV that will act as an option item: */
	  console.log(selElmnt.options[j].value);
	  c = document.createElement("a");
	  c.href = selElmnt.options[j].value;
	  c.innerHTML = selElmnt.options[j].innerHTML;
	  c.addEventListener("click", function(e) {
		  /* When an item is clicked, update the original select box,
		  and the selected item: */
		  var y, i, k, s, h, sl, yl;
		  s = this.parentNode.parentNode.getElementsByTagName("select")[0];
		  sl = s.length;
		  h = this.parentNode.previousSibling;
		  for (i = 0; i < sl; i++) {
			if (s.options[i].innerHTML == this.innerHTML) {
			  s.selectedIndex = i;
			  h.innerHTML = this.innerHTML;
			  y = this.parentNode.getElementsByClassName("same-as-selected");
			  yl = y.length;
			  for (k = 0; k < yl; k++) {
				y[k].removeAttribute("class");
			  }
			  this.setAttribute("class", "same-as-selected");
			  break;
			}
		  }
		  h.click();
	  });
	  b.appendChild(c);
	}
	x[i].appendChild(b);
	a.addEventListener("click", function(e) {
	  /* When the select box is clicked, close any other select boxes,
	  and open/close the current select box: */
	  e.stopPropagation();
	  closeAllSelect(this);
	  this.nextSibling.classList.toggle("select-hide");
	  this.classList.toggle("select-arrow-active");
	});
  }
  
  function closeAllSelect(elmnt) {
	/* A function that will close all select boxes in the document,
	except the current select box: */
	var x, y, i, xl, yl, arrNo = [];
	x = document.getElementsByClassName("select-items");
	y = document.getElementsByClassName("select-selected");
	xl = x.length;
	yl = y.length;
	for (i = 0; i < yl; i++) {
	  if (elmnt == y[i]) {
		arrNo.push(i)
	  } else {
		y[i].classList.remove("select-arrow-active");
	  }
	}
	for (i = 0; i < xl; i++) {
	  if (arrNo.indexOf(i)) {
		x[i].classList.add("select-hide");
	  }
	}
  }
  
  
  
  
  
	// Icon gift
	$(window).on("scroll",function(){
	  if($(window).width() <= 768){
		if($(this).scrollTop() > 0){
		  $("#smile-ui-lite-container").css("display","block");
		  $("#smile-ui-container").css("display","block");
		  $("#smile-ui-lite-launcher-frame-container").css("width","60px");
		}else{
		  $("#smile-ui-lite-container").css("display","none");
		  $("#smile-ui-container").css("display","none");
		}
	  }
	});
	// End Icon gift
  try{
	 FilterApi.afterCall = function() {
	  setTimeout(function() {
	  StampedFn.loadBadges();	
	}, 0)
	  // console.log(StampedFn);
	}
  }catch(err){
	console.log(err.message);
  }
   
	function addThousandsSeparators(n) {
	  return (''+n).split('').reverse().join('')
		  .match(/(\d{1,3})/g).join(',').split('')
		  .reverse().join('');
	}
	
  
  })
  
  // loading session
  var timeAnimeLoading = 2800;
  if (navigator.userAgent.indexOf('Mac OS X') != -1) {
	if($(window).width() <= 768){
	  timeAnimeLoading = 4500;
	}
  }
  
  window.onload = function(){
	setTimeout(function(){
	  if($(".loading").length){
		$(".loading").removeClass("is_active");
		$(".PageContainer").css("opacity",1);
		if($("#main").length){
		  $("#main").css("opacity",1);
		}
	  }
	}, timeAnimeLoading);
	// Check browser support
	if (typeof(Storage) !== "undefined") {
	  // Store
	  sessionStorage.setItem("onpage", true);
	  // Retrieve
	  // console.log(sessionStorage.getItem("onpage"));
	} else {
	  console.log("Sorry, your browser does not support Web Storage...");
	}
  };