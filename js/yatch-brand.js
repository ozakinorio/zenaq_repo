//Brand Filter SP
$(document).ready(function(){
	$(window).on("resize",function(){
	  if ($(window).width() <= 768) {
		$('.brand-filter-title').click(function () {
		  $('.brand-filter').toggleClass('is-open');
		  $('.brand-filter-list').toggleClass('is-open')
		  const text = $('.brand-filter-title').text();
		  if (text == '') {
			$('.brand-filter-title').text('');
		  } else {
			$('.brand-filter-title').text('');
		  }
		})
	  }
	});
	if ($(window).width() <= 768) {
	  $('.brand-filter-title').click(function () {
		$('.brand-filter').toggleClass('is-open');
		$('.brand-filter-list').toggleClass('is-open')
		const text = $('.brand-filter-title').text();
		if (text == '') {
		  $('.brand-filter-title').text('');
		} else {
		  $('.brand-filter-title').text('');
		}
	  })
	}
	if($(".brand-filter-list li a").length){
	  $(".brand-filter-list li a").each(function(index,val){
		var href = $(val).attr("href");
		if(!$(href).length){
		  $(val).addClass("is_out");
		  $(val).css("color","rgb(148 147 147)");
		}
	  });
	}
	$(".brand-filter-list li a").on("click", function (event) {
	  event.preventDefault();
	  try{
		var href = $(this).attr("href");
		var menuHeight = $(".Header__Wrapper").innerHeight();
		if($(href).length){
		  $("html, body").animate({
			scrollTop: $(href).offset().top - menuHeight - 30
		  }, 1000);
		}
		return false;
	  }
	  catch(err){
		console.log(err.message);
	  }
	})
  });