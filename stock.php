<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>在庫･納期リスト | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">
    
    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>在庫納期リスト</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>在庫納期リスト</h5>
                    <p>Support</p>
                </div>
                <div class="faq_inner">
                    <div class="left_faq">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li><a href="faq.php">よくある質問</a></li>
                            <li class="arrow_down">在庫納期リスト</li>
                            <li><a href="eol.php">生産終了モデル</a></li>
                            <li><a href="warranty.php">半永久保証</a></li>
                            <li><a href="repair.php">ロッド修理</a></li>
                            <li><a href="trial.php">体感イベント</a></li>
                        </ul>
                    </div>
                    <div class="right_faq">

                        <div class="faq_list">
                            <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">在庫状況・納期予定</h6>
                            <div class="sub_info_text" data-sal="slide-up" data-sal-duration="500">
                                ・下記の納期は「現在ご注文頂いた場合の納期」です。ご注文が遅れると納期も先に延びる場合があります。<br>
                                ・システムの都合上、「在庫あり」「残りわずか」でも注文のタイミングによっては在庫切れになっている場合があります。　
                            </div>
                            <div class="">
                                <div class="accordion_stock">
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_stock01" class="toggle">
                                        <label class="title" for="toggle_stock01">Tobizo</label>
                                        <div class="content">
                                            <table class="stock_list">
                                                <tr class="gray">
                                                    <th>モデル</th>
                                                    <th>TC80-50G</th>
                                                    <th>TC80-80G</th>
                                                    <th>TC86-110G</th>
                                                    <th>TC83-150G</th>
                                                    <th>TC80-200G</th>
                                                </tr>
                                                <tr>
                                                    <td>在庫</td>
                                                    <td><span>在庫切れ</span></td>
                                                    <td><span>在庫切れ</span></td>
                                                    <td><span>在庫切れ</span></td>
                                                    <td><span>在庫切れ</span></td>
                                                    <td><span>在庫切れ</span></td>
                                                </tr>
                                                <tr>
                                                    <td>納期</td>
                                                    <td>今期分完売</td>
                                                    <td>2/20 頃</td>
                                                    <td>3/10 頃</td>
                                                    <td>10/10 頃</td>
                                                    <td>12/10 頃</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_stock02" class="toggle">
                                        <label class="title" for="toggle_stock02">SINPAA</label>
                                        <div class="content">
                                            <table class="stock_list">
                                                <tr class="gray">
                                                    <th>モデル</th>
                                                    <th>83 Hiramasa</th>
                                                </tr>
                                                <tr>
                                                    <td>在庫</td>
                                                    <td><span>在庫切れ</span></td>
                                                </tr>
                                                <tr>
                                                    <td>納期</td>
                                                    <td>10/30 頃</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>

        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>