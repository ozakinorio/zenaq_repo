<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>販売規約 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>販売規約</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>販売規約</h5>
                    <p>Terms</p>
                </div>
                <div class="news_list">
                    <ul>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title">第1条：販売先</h6>
                            <div class="news_text">
                                「販売先」とは、株式会社ゼナック（以下「当社」と言います）が定める本規約に同意の上、当社商品を日本国内のみに販売する販売店又は個人をいいます。
                            </div>
                        </li>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title">第2条：禁止事項</h6>
                            <div class="news_text">
                                当社商品の販売に際して、次の各号の行為を行うことを禁止します。<br>
                                <br>
                                1.日本国外への輸出行為。<br>
                                2.輸出業者や並行輸出者への販売。<br>
                                3.ネット等での転売（オークション等）や再販売。<br>
                                4.その他、国内ユーザーへの販売業務以外を目的として商品を購入する行為。<br>
                                5.購入者名など、虚偽の情報を提供すること。
                            </div>
                        </li>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title">第3条：販売の中止、拒否</h6>
                            <div class="news_text">
                                当社の判断により商品販売の全部または一部を適宜拒否、または取り消しできるものとします。
                            </div>
                        </li>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title">第4条：本規約の改定</h6>
                            <div class="news_text">
                                当社は、本規約を任意に改定また、当社において本規約を補充する規約を定めることができます。<br>
                                改定後の規約を当社所定のサイトに掲示した時点でその効力を生じるものとします。この場合、販売先は、改定後の新規約に従うものとします。
                            </div>
                        </li>
                        <li class="end" data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title">第5条：準拠法、管轄裁判所</h6>
                            <div class="news_text">
                                本規約に関して紛争が生じた場合、当社所在地を管轄する地方裁判所を第一審の専属的合意管轄裁判所とします。
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>

        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>