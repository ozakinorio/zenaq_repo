<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>常識破壊 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>コンセプト</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="about_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>常識破壊</h5>
                    <p>About Us</p>
                </div>
                <div class="about_inner">

                    <div class="left_about">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li><a href="about_concept.php">コンセプト</a></li>
                            <li class="arrow_down">常識破壊</li>
                            <li><a href="about_history.php">ヒストリー</a></li>
                        </ul>
                    </div>

                    <div class="right_about">
                        <div class="about_col">
                            <div class="page_movie" data-sal="slide-up" data-sal-duration="500">
                                <video type="video/webm" src="img/about/movie02.mp4" playsinline loop autoplay muted></video>
                            </div>
                            <p class="catch_text" data-sal="slide-up" data-sal-duration="500">BREAKING<br>THE MOLD</p>
                            <h6 class="catch_main" data-sal="slide-up" data-sal-duration="500">常識を破壊する。</h6>
                            <div class="catch_main_text" data-sal="slide-up" data-sal-duration="500">
                                「非常識な発想」と理想をカタチにする「高い技術力」<br>
                                それが斬新で魅力的なゼナックロッドを作り上げてきました。ゼナックの挑戦はこれからも続きます。
                            </div>

                            <div class="about_tech_col">
                                <div class="about_tech_list">
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_texh01" class="toggle">
                                        <label class="title list01" for="toggle_texh01"></label>
                                        <div class="content">
                                            <div class="text">
                                                <div class="left">
                                                    <p class="tech_list_title">世界最高峰のロッドブランク</p>
                                                    <p class="tech_list_text">
                                                        長年に渡り、試行錯誤を繰り返す日々の中で磨き上げた、理想をカタチにする為のブランク成型技術。この確かな技術の裏付け無くしては、非現実な発想やフィールドが求める厳しい課題をクリアする為の理想のブランクは完成しません。ゼナックが自社製ブランクにこだわる理由はここに有ります。
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <img src="img/about/right_img01.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /option -->

                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_texh02" class="toggle">
                                        <label class="title list02" for="toggle_texh02"></label>
                                        <div class="content">
                                            <div class="text">
                                                <div class="left">
                                                    <p class="tech_list_title">RGガイドシステム</p>
                                                    <p class="tech_list_text">
                                                        スピニングロッドの性能を究極まで引き出す、PEライン専用オリジナルガイドセッティング。PEラインの使用におけるデメリットを完全に打ち消し、ガイドセッティングの新しい概念を確立した。
                                                    </p>
                                                    <p class="tech_list_btn"><a href="">Read more</a></p>
                                                </div>
                                                <div class="right">
                                                    <img src="img/about/right_img02.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /option -->

                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_texh03" class="toggle">
                                        <label class="title list03" for="toggle_texh03"></label>
                                        <div class="content">
                                            <div class="text">
                                                <div class="left">
                                                    <p class="tech_list_title">イカリブランク</p>
                                                    <p class="tech_list_text">
                                                        高弾性カーボンブランクにカーボンコアを内蔵したカーボンXカーボン構造。<br>
                                                        高弾性カーボンの特性である「高反発」なブランクでありながら破断強度を極限まで高めた、今までの常識を覆すスーパーブランク。<br>
                                                        イカリブランクの特長である破断強度・高反発力・高感度は今までのブランクとは一線を画し、ルアーフィッシング新時代の幕開けさえも予感させてくれる。
                                                    </p>
                                                    <p class="tech_list_btn"><a href="">Read more</a></p>
                                                </div>
                                                <div class="right">
                                                    <img src="img/about/right_img03.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /option -->

                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_texh04" class="toggle">
                                        <label class="title list04" for="toggle_texh04"></label>
                                        <div class="content">
                                            <div class="text">
                                                <div class="left">
                                                    <p class="tech_list_title">ヘキサゴングリップ</p>
                                                    <p class="tech_list_text">
                                                        「握った時の指の形」に合わせ込んだ「変則の6角形」のフロントグリップ。<br>
                                                        より少ない力でロッドを保持できることによりパワーファイトをサポートし、ビッグワンのランディング確率を大きく向上させる。
                                                    </p>
                                                    <p class="tech_list_btn"><a href="">Read more</a></p>
                                                </div>
                                                <div class="right">
                                                    <img src="img/about/right_img04.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /option -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>