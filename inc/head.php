<link rel="canonical" href="">
<link rel="shortcut icon" href="" type="image/vnd.microsoft.icon">
<link rel="icon" href="img/common/favicon.ico" type="image/vnd.microsoft.icon">
<link rel=icon type=image/vnd.microsoft.icon>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://use.typekit.net/lll3mes.css">
<link rel=stylesheet type=text/css href="css/style.css" rel="preload">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Marcellus&display=swap" rel="stylesheet">
<script>
(function(d) {
    var config = {
    kitId: 'knx1pcq',
    scriptTimeout: 3000,
    async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
})(document);
</script>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/modal-video@2.4.2/css/modal-video.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link href="css/sal.css" rel="stylesheet">