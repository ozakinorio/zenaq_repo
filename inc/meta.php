<meta name="description" content="ZENAQ(ゼナック)のオフィシャルサイト。直接販売も可能。ロッドは全て送料無料。　「一人のアングラーとして、創る。」をコンセプトに一本一本に魂を注ぎ込みます。">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="format-detection" content="telephone=no">

<meta name="author" content="ARUTEGA.inc">
<meta property="og:site_name" content="ZENAQ(ゼナック) | オフィシャルサイト">
<meta property="fb:app_id" content="" />
<meta property="og:type" content="website">
<meta property="og:title" content="ZENAQ(ゼナック) | オフィシャルサイト">
<meta property="og:url" content="https://zenaq.com/">
<meta property="og:image" content="https://zenaq.com/img/common/ogp.png">
<meta property="og:description" content="ZENAQ(ゼナック)のオフィシャルサイト。直接販売も可能。ロッドは全て送料無料。　「一人のアングラーとして、創る。」をコンセプトに一本一本に魂を注ぎ込みます。">
<meta name="twitter:site" content="">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:image" content="https://zenaq.com/img/common/ogp.png">
<meta name="twitter:title" content="ZENAQ(ゼナック) | オフィシャルサイト" />
<meta name="twitter:url" content="https://zenaq.com/" />
<meta name="twitter:description" content="ZENAQ(ゼナック)のオフィシャルサイト。直接販売も可能。ロッドは全て送料無料。　「一人のアングラーとして、創る。」をコンセプトに一本一本に魂を注ぎ込みます。" />
<meta name="facebook-domain-verification" content="0u5xjdy49krncc7jad3jtomdnvvfgd" />