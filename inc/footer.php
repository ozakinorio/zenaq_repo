    <!-- footer -->
    <footer class="footer">
        <div class="footer_inner">
            <div class="footer_inner_list">
                <ul class="main">
                    <li class="main_title">
                        <span>ゼナックについて</span>
                        <ul class="inner_list">
                            <li class="line"><a href="about_concept.php">コンセプト</a></li>
                            <li class="line"><a href="about_tech.php">常識破壊</a></li>
                            <li class="line"><a href="about_history.php">ヒストリー</a></li>
                        </ul>
                    </li>
                    <li class="main_title">
                        <span>ニュース</span>
                        <ul class="inner_list">
                            <li class="line"><a href="news.php">ニュース一覧</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="main">
                    <li class="main_title">
                        <span>ロッド製品</span>
                        <ul class="inner_list">
                            <li>
                                <div class="accordion">
                                    <div class="option">
                                        <input type="checkbox" id="toggle2_1" class="toggle">
                                        <label class="title" for="toggle2_1">ボートジギング</label>
                                        <div class="content">
                                            <ul class="more_inner">
                                                <li><a href="product.php">FOKEETO IKARI</a><span class="new">NEW</span></li>
                                                <li><a href="product.php">Expedition</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion">
                                    <div class="option">
                                        <input type="checkbox" id="toggle2_2" class="toggle">
                                        <label class="title" for="toggle2_2">ボートキャスティング</label>
                                        <div class="content">
                                            <ul class="more_inner">
                                                <li><a href="product.php">Tobizo</a></li>
                                                <li><a href="product.php">SINPAA</a><span class="new">NEW</span></li>
                                                <li><a href="product.php">Expedition</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion">
                                    <div class="option">
                                        <input type="checkbox" id="toggle2_3" class="toggle">
                                        <label class="title" for="toggle2_3">遠隔コンパクトロッド</label>
                                        <div class="content">
                                            <ul class="more_inner">
                                                <li><a href="">Expedition</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion">
                                    <div class="option">
                                        <input type="checkbox" id="toggle2_4" class="toggle">
                                        <label class="title" for="toggle2_4">ロックショア</label>
                                        <div class="content">
                                            <ul class="more_inner">
                                                <li><a href="">MUTHOS</a></li>
                                                <li><a href="">MUTHOS Sonio</a></li>
                                                <li><a href="">MUTHOS Accura</a></li>
                                                <li><a href="">MUTHOS Duro</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion">
                                    <div class="option">
                                        <input type="checkbox" id="toggle2_5" class="toggle">
                                        <label class="title" for="toggle2_5">シーバス</label>
                                        <div class="content">
                                            <ul class="more_inner">
                                                <li><a href="">PLAISIR ANSWER</a></li>
                                                <li><a href="">PLAISIR ANSWER SOPMOD</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion">
                                    <div class="option">
                                        <input type="checkbox" id="toggle2_6" class="toggle">
                                        <label class="title" for="toggle2_6">ロックフィッシュ</label>
                                        <div class="content">
                                            <ul class="more_inner">
                                                <li><a href="">SNIPE</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion">
                                    <div class="option">
                                        <input type="checkbox" id="toggle2_7" class="toggle">
                                        <label class="title" for="toggle2_7">ブラックバス</label>
                                        <div class="content">
                                            <ul class="more_inner">
                                                <li><a href="">Spirado BLACKART</a></li>
                                                <li><a href="">GLANZ</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion">
                                    <div class="option">
                                        <input type="checkbox" id="toggle2_8" class="toggle">
                                        <label class="title" for="toggle2_8">バンブーワーク</label>
                                        <div class="content">
                                            <ul class="more_inner">
                                                <li><a href="">Trad 87 (エギング)</a></li>
                                                <li><a href="">ASTRA 76 (メバル)</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="main">
                    <li class="main_title">
                        <span>アクセサリー</span>
                        <ul class="inner_list">
                            <li>
                                <div class="accordion">
                                    <div class="option">
                                        <input type="checkbox" id="toggle3_1" class="toggle">
                                        <label class="title" for="toggle3_1">フィッシングギア</label>
                                        <div class="content">
                                            <ul class="more_inner">
                                                <li><a href="">3Dショートグローブ</a></li>
                                                <li><a href="">フェイス＆ネックガード</a></li>
                                                <li><a href="">ドライポーター</a><span class="new">NEW</span></li>
                                                <li><a href="">フィールドバッグ</a></li>
                                                <li><a href="">ロッドベルト</a></li>
                                                <li><a href="">リールストップラバー</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion">
                                    <div class="option">
                                        <input type="checkbox" id="toggle3_2" class="toggle">
                                        <label class="title" for="toggle3_2">ウェア</label>
                                        <div class="content">
                                            <ul class="more_inner">
                                                <li><a href="">キャップ</a></li>
                                                <li><a href="">Tシャツ</a></li>
                                                <li><a href="">ドライロングTシャツ</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion">
                                    <div class="option">
                                        <input type="checkbox" id="toggle3_3" class="toggle">
                                        <label class="title" for="toggle3_3">ステッカー＆ワッペン</label>
                                        <div class="content">
                                            <ul class="more_inner">
                                                <li><a href="">ステッカー＆デカール</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="main_title">
                        <span>サポート</span>
                        <ul class="inner_list">
                            <li class="line"><a href="faq.php">よくある質問</a></li>
                            <li class="line"><a href="stock.php">在庫納期リスト</a></li>
                            <li class="line"><a href="eol.php">生産終了モデル</a></li>
                            <li class="line"><a href="warranty.php">半永久保証</a></li>
                            <li class="line"><a href="repair.php">ロッド修理</a></li>
                            <li class="line"><a href="trial.php">体感イベント</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="footer_inner_bottom">
                <div class="right">
                    <ul class="sns_list">
                        <li><a href="https://twitter.com/zenaq_tw" target="_blank"><img src="img/common/icon_tw.svg" alt=""></a></li>
                        <li><a href="https://www.facebook.com/zenaqofficial" target="_blank"><img src="img/common/icon_fb.svg" alt=""></a></li>
                        <li><a href="https://www.instagram.com/zenaq/" target="_blank"><img src="img/common/icon_insta.svg" alt=""></a></li>
                        <li><a href="https://www.youtube.com/user/zenaqjp" target="_blank"><img src="img/common/icon_youtube.svg" alt=""></a></li>
                    </ul>
                    <ul class="company_list">
                        <li><a href="company.php">会社概要</a></li>
                        <li><a href="policy.php">個人情報について</a></li>
                        <li><a href="terms.php">販売規約</a></li>
                    </ul>
                </div>
                <div class="left">
                    <p class="footer_logo"><img src="img/common/logo_wh.svg" alt=""></p>
                    <address>&copy;2021 ZENAQ</address>
                </div>
                
            </div>
        </div>
    </footer><!-- /footer -->