<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/modal-video@2.4.2/js/jquery-modal-video.min.js"></script>
<script src="js/slick.min.js" type="text/javascript"></script>
<script src="js/yatch.js" type="text/javascript"></script>
<script src="js/yatch-brand.js" type="text/javascript"></script>
<script src="js/vivus.min.js"></script>
<script src="js/index.js" type="text/javascript"></script>
<script src="js/magnific-popup.min.js" type="text/javascript"></script>
<script src="js/megamenu.js" type="text/javascript"></script>
<script src="js/parallax.min.js" type="text/javascript"></script>
<script src="js/sal.js"></script>
<script>
sal({
	once: true
});

//商品詳細動画スライダー
(function () {
  if ($(".js-modal-video").length) { //クラス名js-modal-videoがあれば以下を実行
    $(".js-modal-video").modalVideo({
      channel: "youtube",
      youtube: {
        rel: 0, //関連動画の指定
        autoplay: 0, //自動再生の指定
      },
    });
  }
})();
</script>