        <div class="header-inner">
            <div class="menu-container">
                <div class="menu">
                    <ul>
                        <li class="main_list sp_logo"><h1 class="header-logo"><a href="/"><img src="img/common/logo_wh.svg" alt=""></a></h1></li>
                        <li class="main_list hover_h"><a href="#">ABOUT</a>
                            <ul>
                                <li>
                                    <ul>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="about_concept.php">コンセプト</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="about_tech.php">常識破壊</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="about_history.php">ヒストリー</a></span></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="main_list hover_h"><a href="#">NEWS</a>
                            <ul>
                                <li>
                                    <ul>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="news.php">ニュース一覧</a></span></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="main_list hover_h"><a href="#">PRODUCT</a>
                            <ul>
                                <li>
                                    <ul>
                                        <li class="menu_col">
                                            <ul class="menu_col_list">
                                                <li class="title">ロッド</li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav01" class="toggle">
                                                            <label class="title" for="toggle_hnav01">ボートジギング</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="product.php">FOKEETO IKARI<!--<span class="new">NEW</span>--></a></li>
                                                                        <li class="line_h"><a href="product.php">Expedition</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav02" class="toggle">
                                                            <label class="title" for="toggle_hnav02">ボートキャスティング</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="product.php">Tobizo</a></li>
                                                                        <li class="line_h"><a href="product.php">SINPAA<!--<span class="new">NEW</span>--></a></li>
                                                                        <li class="line_h"><a href="product.php">Expedition</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav03" class="toggle">
                                                            <label class="title" for="toggle_hnav03">遠征コンパクトロッド</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="product.php">Expedition</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav04" class="toggle">
                                                            <label class="title" for="toggle_hnav04">ロックショア</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="product.php">MUTHOS</a></li>
                                                                        <li class="line_h"><a href="product.php">MUTHOS Sonio</a></li>
                                                                        <li class="line_h"><a href="product.php">MUTHOS Accura</a></li>
                                                                        <li class="line_h"><a href="product.php">MUTHOS Duro</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav05" class="toggle">
                                                            <label class="title" for="toggle_hnav05">シーバス</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">PLAISIR ANSWER</a></li>
                                                                        <li class="line_h"><a href="">PLAISIR ANSWER SOPMOD</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav06" class="toggle">
                                                            <label class="title" for="toggle_hnav06">ロックフィッシュ</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">SNIPE</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav07" class="toggle">
                                                            <label class="title" for="toggle_hnav07">ブラックバス</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">Spirado BLACKART</a></li>
                                                                        <li class="line_h"><a href="">GLANZ</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav08" class="toggle">
                                                            <label class="title" for="toggle_hnav08">バンブーワーク</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">Trad 87 (エギング)</a></li>
                                                                        <li class="line_h"><a href="">ASTRA 76 (メバル)</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="menu_col_list">
                                                <li class="title">アクセサリー</li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav08" class="toggle">
                                                            <label class="title" for="toggle_hnav08">フィッシングギア</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">3Dショートグローブ</a></li>
                                                                        <li class="line_h"><a href="">フェイス&ネックガード</a></li>
                                                                        <li class="line_h"><a href="">ドライポーター</a></li>
                                                                        <li class="line_h"><a href="">フィールドバッグ</a></li>
                                                                        <li class="line_h"><a href="">ロッドベルト</a></li>
                                                                        <li class="line_h"><a href="">リールストップラバー</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav08" class="toggle">
                                                            <label class="title" for="toggle_hnav08">ウェア</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">キャップ</a></li>
                                                                        <li class="line_h"><a href="">Tシャツ</a></li>
                                                                        <li class="line_h"><a href="">ドライロングTシャツ</a></li>
                                                                        <li class="line_h"><a href=""></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav08" class="toggle">
                                                            <label class="title" for="toggle_hnav08">ステッカー&ワッペン</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">ステッカー&デカール</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="main_list hover_h"><a href="#">SUPPORT</a>
                            <ul>
                                <li>
                                    <ul>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="faq.php">よくある質問</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="stock.php">在庫納期リスト</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="eol.php">生産終了モデル</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="warranty.php">半永久保証</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="repair.php">ロッド修理</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="trial.php">体感イベント</a></span></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="main_list btn"><a href="https://www.zenaq-store.jp/" target="_blank">ONLINE STORE</a></li>
                        <li class="main_list lang">JP<a href="">EN</a></li>
                    </ul>
                </div>
            </div>

        </div>