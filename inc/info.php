            <section class="info" data-sal="slide-up" data-sal-duration="500">
                <div class="info_inner">
                    <a href="warranty.php">
                        <div class="info_inner_btn">
                            <div class="left">
                                <p class="subtitle">WARRANTY</p>
                                <p class="title">半永久保証</p>
                            </div>
                            <div class="right">
                                <p class="circle"><img src="img/common/arrow.svg"></p>
                            </div>
                        </div>
                    </a>
                </div>
            </section>