<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>半永久保証 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>半永久保証</li>
        </ul>
    </div>
    </header><!-- /header -->
    
    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>半永久保証</h5>
                    <p>Support</p>
                </div>
                <div class="faq_inner">
                    <div class="left_faq">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li><a href="faq.php">よくある質問</a></li>
                            <li><a href="stock.php">在庫納期リスト</a></li>
                            <li><a href="eol.php">生産終了モデル</a></li>
                            <li class="arrow_down">半永久保証</li>
                            <li><a href="repair.php">ロッド修理</a></li>
                            <li><a href="trial.php">体感イベント</a></li>
                        </ul>
                    </div>
                    <div class="right_faq">

                        <div class="faq_list">
                            <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">半永久保証</h6>
                            <div class="sub_info_text" data-sal="slide-up" data-sal-duration="500">
                                ・ゼナック製品（ルアーロッド）全てに「半永久保証」を適用いたします。<br>
                                ・「半永久保証」とは無期限、複数回使用可能な保証書で、ユーザー登録制です。
                            </div>
                            <div class="page_fv_img" data-sal="slide-up" data-sal-duration="500">
                                <img src="img/warranty_fv.png" alt="">
                            </div>
                            <div class="checklist">
                                <ul>
                                    <li data-sal="slide-up" data-sal-duration="500"><span class="check_icon"><i class="fas fa-check"></i></span>手になじみ、喜びや汗が染み付いたロッドを末永く愛用したい。</li>
                                    <li data-sal="slide-up" data-sal-duration="500"><span class="check_icon"><i class="fas fa-check"></i></span>そうした想いにお答えできるよう、「半永久保証」は生まれました。</li>
                                    <li data-sal="slide-up" data-sal-duration="500"><span class="check_icon"><i class="fas fa-check"></i></span>我々が製造したロッドについては最後まで修理が出来るようにと、譲り受けられたロッドであっても関係なく再登録が可能です。</li>
                                    <li data-sal="slide-up" data-sal-duration="500"><span class="check_icon"><i class="fas fa-check"></i></span>末長くご愛用いただく為のゼナック独自の保証制度です。責任を持った迅速な修理対応をお約束いたします。 </li>
                                </ul>
                                <p class="btn" data-sal="slide-up" data-sal-duration="500"><a href="warranty_entry.php">登録する</a></p>

                                <div class="">
                                    <div class="atten" data-sal="slide-up" data-sal-duration="500">
                                        ご登録いただいた「ご住所」又は「メールアドレス」宛にゼナックからの案内などをお送りさせていただく場合がありますのでご了承ください。<br>
                                        ご登録いただいた情報は半永久保証とゼナックからのご案内などの目的以外には使用いたしません。詳しくは個人情報保護のページをご覧ください。<br>
                                        詳細については以下の保証規定をご覧ください。
                                    </div>

                                    <div class="accordion_stock">
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_warranty" class="toggle">
                                        <label class="title" for="toggle_warranty">「半永久保証」の保証規定</label>
                                        <div class="content">
                                            <ul class="warranty scrollbarSample" id="scrollbar">
                                                <li>
                                                    <p class="tit">保証期間と保証回数</p>
                                                    <p class="tex">
                                                        無期限で複数回有効です。<br>
                                                        修理可能期間は製造打ち切り後3年までとし、それ以降でも修理部品の在庫がある限り修理対応させていただきます。但し、製造打ち切り後3年未満の製品でも材料等の入手困難などの理由で修理ができない場合は同等商品での代替修理とさせていただく場合があります。
                                                    </p>
                                                </li>
                                                <li>
                                                    <p class="tit">保証の範囲</p>
                                                    <p class="tex">
                                                        本保証書に印字されている商品についてのみ保証されます。<br>
                                                        弊社製品の破損や故障によって起こった本製品以外の損害（弊社製品に取り付けられたリール等の破損や紛失、釣行費用、怪我の治療費や慰謝料等）は補償できません。<br>
                                                        釣り以外で起こった破損でも有効です。
                                                    </p>
                                                </li>
                                                <li>
                                                    <p class="tit">免責金額</p>
                                                    <p class="tex">
                                                        保証書に記載されている免責金額がお客様のご負担になります。<br>
                                                        2ピースロッドなどのマルチピースロッドの場合、表示されている免責金額は1箇所に付有効です。#1, #2が同時に破損した修理の場合はそれぞれのピースについて免責金額が必要になります。<br>
                                                        （表記の免責金額には消費税は含んでいません）
                                                    </p>
                                                </li>
                                                <li>
                                                    <p class="tit">保証の受け方</p>
                                                    <p class="tex">
                                                        「半永久保証登録が完了している事」と「破損したピースの1/2以上の回収」が保証適用の条件です。<br>
                                                        お買い求めいただいた販売店様、または当社に「免責金額」「破損したロッドの1/2以上」（ハンドルジョイント、2ピース、マルチピースロッドの場合はジョイント調整のために破損していない部分も必要）をお届けください。<br>
                                                        「半永久保証のご登録」も必要です。<br>
                                                        当社までの片道の運賃は元払いにてご発送ください。<br>
                                                        弊社からの発送時には元払いにてご発送いたします。（お届け先は日本国内に限ります）<br>
                                                        本ホームページに掲載されている「保証規定」が保証書などの印刷物よりも優先適用されます。
                                                    </p>
                                                </li>
                                                <li>
                                                    <p class="tit">ユーザー様登録制</p>
                                                    <p class="tex">
                                                        ご購入いただいたユーザー様が登録していただく事で保証書が有効になります。<br>
                                                        登録方法は、弊社ホームページから、又は保証書に付いている登録はがきに必要事項をご記入の上、FAXまたは郵送にてゼナックまでお送りください。<br>
                                                        日本以外のご住所での登録はできません。<br>
                                                        （海外輸出品に付いては各国のディーラーが販売するロッドに海外用の保証書が添付されます）<br>
                                                        この保証システムはゼナックが製造したロッドに責任を持って保証するものです。よって、ロッドを譲り受けられた等でユーザー様が替わった場合でも保証を継続させていただきます。その場合には新しくユーザーになられる方が再登録されることで有効になります。
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>