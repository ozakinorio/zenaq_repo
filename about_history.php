<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>ヒストリー | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>ヒストリー</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="about_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>ヒストリー</h5>
                    <p>About Us</p>
                </div>
                <div class="about_inner">

                    <div class="left_about">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li><a href="about_concept.php">コンセプト</a></li>
                            <li><a href="about_tech.php">常識破壊</a></li>
                            <li class="arrow_down">ヒストリー</li>
                        </ul>
                    </div>

                    <div class="right_about">
                        <div class="about_col">
                            <div class="page_movie" data-sal="slide-up" data-sal-duration="500">
                                <video type="video/webm" src="img/about/movie03.mp4" playsinline loop autoplay muted></video>
                            </div>
                            <p class="catch_text" data-sal="slide-up" data-sal-duration="500">BRAND<br>HISTORY</p>

                            <div class="timeline_col">

                                <div class="timeline">
                                    <ul class="timeline-list">
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">1960</div>
                                            <div class="content_col">
                                                <div class="tl_title">バンブーロッド</div>
                                                <div class="tl_subtitle">「竹」の釣竿製作開始</div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">1971</div>
                                            <div class="content_col">
                                                <div class="tl_title">グラスファイバー</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl01" class="toggle">
                                                        <label class="title" for="toggle_tl01">画期的な素材「グラスファイバー」の釣竿を発表</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                重い竹の釣竿から「軽くて強いグラスロッド」の登場で、釣りの世界が一変した。<br>
                                                                70年代の自社工場　当時最新の釣竿製造用ローリングテーブル
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">1979</div>
                                            <div class="content_col">
                                                <div class="tl_title">カーボンファイバー</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl02" class="toggle">
                                                        <label class="title" for="toggle_tl02">最先端素材「カーボンファイバー」を釣竿に導入</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                グラスロッドに比べて「驚異的に軽くて強い」カーボンロッドがこの時スタート。<br>
                                                                非常に高額な商品であったが「あゆ竿」などに使用されることで一気に浸透してゆく事になる。<br>
                                                                しかしカーボン素材のレベルや品番の少なさ、樹脂の問題などで現在のカーボンロッドとはまだまだ程遠いものであった。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">1990</div>
                                            <div class="content_col">
                                                <div class="tl_title">ZENAQ誕生</div>
                                                <div class="tl_subtitle">PEラインの出現とルアーフィッシング進化の時代</div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">1996</div>
                                            <div class="content_col">
                                                <div class="tl_title">フルオーダーロッド</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl03" class="toggle">
                                                        <label class="title" for="toggle_tl03">ブランクからのオーダーも可能</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                ブランク成型から組立の全工程までを「自社工場」で行うゼナックならではの「ブランクからのオーダー」も可能なフルオーダーバスロッド誕生。<br>画期的でまさしく「オーダーメイド」のロッドが可能になった。（現在はお受けしておりません）
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">1997</div>
                                            <div class="content_col">
                                                <div class="tl_title">試行錯誤のジギング創成期</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl04" class="toggle">
                                                        <label class="title" for="toggle_tl04">PEライン専用ロッド Jigger Trust 発表</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                PEラインの使用を前提にボートジギングロッドを開発。当時はまだジギング専用リールなどのタックルがほとんどなく「試行錯誤のジギング創成期」であったが、このロッドの誕生で未開のディープエリア攻略や新しいテクニックを生み出した。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">1997</div>
                                            <div class="content_col">
                                                <div class="tl_title">Wナットリング</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl05" class="toggle">
                                                        <label class="title" for="toggle_tl05">実釣から生まれるアイデア</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                リールのぐらつきを止めるゼナックオリジナル「Wナットリング」(PAT.)実釣から生まれたアイデアはどれもが「本質的で実用的」だ。ハードなジギングやビッグゲームには必須のアイテムとなる。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">1999</div>
                                            <div class="content_col">
                                                <div class="tl_title">FOKEETO誕生</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl06" class="toggle">
                                                        <label class="title" for="toggle_tl06">世界のルアーゲームを牽引してゆくHandmade in Japan.</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                進化してゆくテクニックに応えるべく「細分化したモデル」として誕生。ボートからのジギング、キャスティングでかつてない記録魚が釣り上げられていった。今なお世界の海で愛用されるこのロッドは、ボートゲームを牽引するゼナックのフラグシップモデルとなった。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">1999</div>
                                            <div class="content_col">
                                                <div class="tl_title">DEFI Redge 93 発表</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl07" class="toggle">
                                                        <label class="title" for="toggle_tl07">レーシングマシン「デフィーレッジ」</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                当時最先端のカーボン素材を駆使して焼き上げたブランクは非常に繊細で「感度」「軽さ」に特化したロッドに仕上がった。レーシングマシンのような「極限まで特化させた性能」をシーバスロッドに与えたのだ。しかし当時の高弾性素材は「強度面では非常にもろく」、使いこなせる釣り人を選ぶロッドであった。敢えて提案したこのロッドは、熟練したアングラーから高い評価を得た。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">1999</div>
                                            <div class="content_col">
                                                <div class="tl_title">「8本撚り」Jigger Trust PEライン</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl08" class="toggle">
                                                        <label class="title" for="toggle_tl08">世の中に無いから自分達で造ってしまう</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                当時、ジギングなどに使用されていたPEラインは「4本撚り」。<br>
                                                                当時のジギングをご存知の方はしゃくるたびにガイドに擦れる「激しい異音」の記憶があるだろう。ラインを通して海中に響く音は魚の警戒心をあおる。
                                                                ゼナックが考える「8本撚り」と言う高性能PEラインは「音の解消」だけでなく強度面でも優れ「より細い号数」を使用する事ができ、釣果に直結する性能を見せた。<br>
                                                                またラインの長さを表示するためのカラフルな色付けをやめて「真っ白なPEライン」にもこだわった。理由は「魚から一番見えにくい色」を考えた結論だった。「魚の腹は白い」これがヒントになった。<br>
                                                                当時、4本撚りの販売価格の2～3倍もしたこのJigger Trust PEラインは「この価格では売れない」という業界の予想を大きく裏切り大ヒット商品となった。
                                                                目から鱗の「8本撚り」は以後のPEラインの主流となり、ゼナックの「非常識」が常識となった。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2000</div>
                                            <div class="content_col">
                                                <div class="tl_title">非常識な発想</div>
                                                <div class="tl_subtitle">常識にとらわれない商品開発</div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2001</div>
                                            <div class="content_col">
                                                <div class="tl_title">エギング創成期</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl09" class="toggle">
                                                        <label class="title" for="toggle_tl09">現在のエギングロッドの基本形となる「アソート」を発表</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                「伸びの無いPEライン」で「激しいしゃくり」に堪えるブランクの開発。<br>
                                                                エギにキレのあるアクションを与える為の「高弾性カーボン」を使いながら「瞬発的にロッドに掛かる負荷」をいかに解決するか？何本もの試作ブランクが釣場と工場を往復した。<br>
                                                                また、「高性能ながらライントラブルの多いPEライン」をエギングで使えるように磯竿用の「ULガイド」をルアーロッドに使うなど画期的で独創的な発想でついに「アソートShore S86 Accura」が完成。<br>
                                                                現在の「デイエギング」ブームを巻き起こした。ロッドの長さ、アクションなどその基本となるブランク設計は今なおエギングロッドの中心にある。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2001</div>
                                            <div class="content_col">
                                                <div class="tl_title">スローアクション Whippy</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl10" class="toggle">
                                                        <label class="title" for="toggle_tl10">「喰いの渋い状況」で圧倒的な釣果をたたき出した「ウイッピー」</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                当時のジギングロッドの主流とは全く逆のスローアクションロッドを提案。<br>
                                                                「喰いの渋い状況」でもコンスタントに釣果を上げられるロッドとして注目される。「ロッド開発はフィールドから」と言うゼナックの商品開発のコンセプトが新しいロッドを生み出し、ボートジギングの幅を押し広げた。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2001</div>
                                            <div class="content_col">
                                                <div class="tl_title">「プレジール」鮮烈デビュー</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl11" class="toggle">
                                                        <label class="title" for="toggle_tl11">シーバス新時代幕開け</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                それまでのシーバスロッドの概念を覆し、ファーストテーパーで魚をかけていく。<br>
                                                                特にこのロッドの開発には「膨大な数の試作ロッド」と「長期間に及ぶフィールドテスト」を延々繰り返した。少しの妥協も許さないテスターとゼナックの熱い思いがようやく形になった年だった。<br>
                                                                プレジール開発テスターに大きな影響を与えていたのは1999年に発表した「DEFI Redge93」だった。その後のシーバスロッドの設計に大きな影響を与る事となるゼナック渾身のプレジールが鮮烈デビュー。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2002</div>
                                            <div class="content_col">
                                                <div class="tl_title">「攻める」メバル釣り ASTRA</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl12" class="toggle">
                                                        <label class="title" for="toggle_tl12">発明！ソフトで高感度の「チューブラトップ」</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                当時のメバルロッドは「細いソリッドティップ」と「スローテーパー」の全く張りのないロッドが使われていた。<br>
                                                                アタリを感じる事もあわせる事もかなりテンポが遅れ、繊細なメバルはなかなか釣れない。「繊細なあたりを確実にフッキングに持ち込む」と言う考えのもとブランク開発は始まった。<br>
                                                                カーボン素材メーカー様の献身的な協力も得て「チューブラトップ」でありながら「非常にソフト」でしかも「感度の良い」ブランクの開発に成功。<br>
                                                                喰った瞬間にフッキング出来るこの「攻めて獲る」アストラの出現でメバル釣りを一新した。ナイトゲームでの視認性も考慮したイエローブランクも印象的。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2003</div>
                                            <div class="content_col">
                                                <div class="tl_title">ショアジギング MUTHOS誕生</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl13" class="toggle">
                                                        <label class="title" for="toggle_tl13">「良い道具」を造るのに「近道はない」</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                1999年発表のDEFIをさらに進化させたショアジギング専用ロッド「ミュートス」。<br>
                                                                この時代まだまだ「認知度の低いショアジギング」ではあったが、熱きテスターと共に納得いく商品開発を「地道」に繰り返してきた。このロッドと共にショアジギングの進化があったと言っても過言ではない程の影響力を持ち、現在のPEラインロックショアゲームの礎を築いたミュートスがこの年にデビュー。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2004</div>
                                            <div class="content_col">
                                                <div class="tl_title">ROUF</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl14" class="toggle">
                                                        <label class="title" for="toggle_tl14">こんな思いから誕生した「工房ルーフ」</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                ゼナックが考える「非常識な商品」は「魅力的な商品」でもあったが量産化する場合には多くの問題もあった。<br>
                                                                量産品では実現できない「突出したロッド」「チャレンジングなモデル」を世に届けたい。そんな思いから誕生した「工房ルーフ」。現在では常識となった「ロッド設計」や「RGガイドシステム」など、当時の「非常識」はこの工房から発信された。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2004</div>
                                            <div class="content_col">
                                                <div class="tl_title">永遠の約束</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl15" class="toggle">
                                                        <label class="title" for="toggle_tl15">使い捨ての時代に異議を唱える「半永久保証」</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                手になじんだ愛着のロッドを出来る限り使い続けてほしい。<br>
                                                                一本一本「魂込めて作り上げたゼナックロッド」に責任もって修理にあたる覚悟。たとえ「ユーザー様が変わられた場合でも保証を継続する」という、使い捨ての時代に異議を唱えるゼナックの「半永久保証」。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2006</div>
                                            <div class="content_col">
                                                <div class="tl_title">Bamboo Work</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl16" class="toggle">
                                                        <label class="title" for="toggle_tl16">おとなの遊び心、バンブーワーク</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                ゼナックスーパーブランクをベースに遊び心溢れる「和テイスト」デザイン。趣味としての釣り、一匹を釣る楽しみを倍増してくれるロッド。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2009</div>
                                            <div class="content_col">
                                                <div class="tl_title">独創！RGガイド</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl17" class="toggle">
                                                        <label class="title" for="toggle_tl17">「突き抜ける快感」RGガイドシステム発表</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                「PEラインの性能をもっと引き出すガイド設定」が必要！<br>
                                                                スピニングロッドのバットガイド径は「大きいのが常識」。
                                                                その常識にもとらわれずトップガイド並みの「小口径」で「高足」ガイドを考案。<br>
                                                                これにより飛躍的にPEラインでのロッド性能が向上することが解った。
                                                                しかしながらこんな非常識なバットガイドは何処にも無かった。「理想のバットガイド」を試行錯誤しながら独自で製作。<br>
                                                                さらに通常の倍以上の数のガイドを並べる事でその突出した性能が発揮される。
                                                                この非常識なガイドセッティングを一見した人は口を揃えて「絶対ルアーが飛ばない」と思ったという。<br>
                                                                勿論、結果は予想を裏切り「一度使うと手放せなくなる」という評価に変わっていった。
                                                                この年のフィッシングショーでエギングロッドに搭載し「突き抜ける快感」ROUF - RGを発表。<br>
                                                                このガイドシステムによる多くのメリットは今までのガイド設計概念を大きく覆した。
                                                                また一つゼナックの「非常識」が常識に変わった。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2010</div>
                                            <div class="content_col">
                                                <div class="tl_title">探求</div>
                                                <div class="tl_subtitle">ルアーフィッシングの新たな可能性を探り続ける</div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2011</div>
                                            <div class="content_col">
                                                <div class="tl_title">高性能コンパクトロッド Expeditionガイド</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl18" class="toggle">
                                                        <label class="title" for="toggle_tl18">「突き抜ける快感」RG「全幅の信頼」を置いてこそ最高のパートナーガイドシステム発表</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                小型の飛行機やリュックでの奥地への移動、時には小型ボートで源流付近での釣りなど、海外遠征のために開発された本格的なコンパクトロッド。<br>夢を追っての海外遠征にはロッドの故障は許されない。「絶対の信頼が置けるロッド」が必要なのだ。ゼナックの経験と技術をすべて注ぎ込んだ「エクスペディション」誕生。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2014</div>
                                            <div class="content_col">
                                                <div class="tl_title">MUTHOS Accura</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl19" class="toggle">
                                                        <label class="title" for="toggle_tl19">言葉では語れない性能</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                本格的なショアジギングロッド発表から早くも15年。<br>
                                                                その間、止まる事無くフィールドで磨き続けてきたノウハウがさらなる進化を与えた。
                                                                「特化した性能」と「使い易さ」と言う「相反する性能」の融合。<br>
                                                                その次元の高さはフィールドですぐに証明された。ショアからという過酷な条件にも関わらず「55kgオーバーのクロマグロ」もランディングされた。
                                                                机上論では生み出せないであろう数々のノウハウが詰まっている。<br>
                                                                ゼナックのロッドは「一度は使ってみるべき」と言って頂ける理由がここに有ると信じ、日々進化を求める。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2016</div>
                                            <div class="content_col">
                                                <div class="tl_title">PLAISIR ANSWER SOPMOD</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl20" class="toggle">
                                                        <label class="title" for="toggle_tl20">既存のゲームに一石を投じる</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                一匹の重み。簡単ではないが故のその喜びを深く味わいたい。そんな思いが専用ロッド  = ソップモッド  = をフィールドで鍛え上げた。そしてその熱い思いと具現化されたソップモッドが新たなビッグベイトフィッシングの常識を創った
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2017</div>
                                            <div class="content_col">
                                                <div class="tl_title">「飛びのトビゾー」</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl21" class="toggle">
                                                        <label class="title" for="toggle_tl21">20年のノウハウをこのロッドに。Tobizo</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                「飛距離の差」は「釣果の差」驚異の飛距離を実現したボートキャスティングロッド Tobizoがデビュー。<br>
                                                                キャスティングロッドの本質である「飛び」を極めた。“20年に1度の画期的カーボン素材”と謳われる東レ株式会社のトレカ® T1100GをTobizoのバックボーンに採用し、ゼナックのキャスティングロッド “20年の経験”その全てを注ぎ込んだ世界の海に発信するフラッグシップモデルが完成。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2018</div>
                                            <div class="content_col">
                                                <div class="tl_title">FOKEETO IKARI</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl22" class="toggle">
                                                        <label class="title" for="toggle_tl22">常識破壊「新時代のジギングロッド」</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                1997年に世界に先駆けて開発したボートジギングロッドの発売から20年。超高弾性ロッドに「カーボンコアを内蔵」するといった全く新しい発想の「イカリブランク」釣果は勿論、ジギング本来の釣り味を存分に楽しめるロッドが完成。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2018</div>
                                            <div class="content_col">
                                                <div class="tl_title">MUTHOS Sonio</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl23" class="toggle">
                                                        <label class="title" for="toggle_tl23">マジックロッド誕生</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                超常識の MUTHOSアキュラの大反響から４年。ミドルパワークラスのマジックロッドが完成した。<br>
                                                                「シーバスロッドのようなフィーリングで扱え、青物パワーにも対抗できるロッド」をコンセプトとして開発されたこのロッド。ショアから 37キロのカンパチがランディングされるなどそのポテンシャルの高さに我々自らも驚かされた。まさにマジックロッドの誕生であった。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2020</div>
                                            <div class="content_col">
                                                <div class="tl_title">挑戦</div>
                                                <div class="tl_subtitle">常に進化を。</div>
                                            </div>
                                        </li>
                                        <li class="timeline-list-item" data-sal="slide-up" data-sal-duration="500">
                                            <div class="date">2021</div>
                                            <div class="content_col">
                                                <div class="tl_title">SINPAA</div>
                                                <div class="accordion_tl">
                                                    <div class="option">
                                                        <input type="checkbox" id="toggle_tl24" class="toggle">
                                                        <label class="title" for="toggle_tl24">新しい鼓動「シンパ」誕生</label>
                                                        <div class="content">
                                                            <div class="text">
                                                                このロッドでボートキャスティングゲームが更に進化する。ターゲットを絞り込み、考えうる最高のブランクを造り上げ細部にまでこだわり抜いたスペシャリティロッド。これまでのタックルでは困難とされていたビッグワンを仕留めるために誕生した。<br>
                                                                まさに登山家が自らの限界を求めて険しい山々の - 頂 いただき - を目指すように。
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>