<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>ロッド修理 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>ロッド修理</li>
        </ul>
    </div>
    </header><!-- /header -->
    
    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>ロッド修理</h5>
                    <p>Support</p>
                </div>
                <div class="faq_inner">
                    <div class="left_faq">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li><a href="faq.php">よくある質問</a></li>
                            <li><a href="stock.php">在庫納期リスト</a></li>
                            <li><a href="eol.php">生産終了モデル</a></li>
                            <li><a href="warranty.php">半永久保証</a></li>
                            <li class="arrow_down">ロッド修理</li>
                            <li><a href="trial.php">体感イベント</a></li>
                        </ul>
                    </div>
                    <div class="right_faq">

                        <div class="faq_list">
                            <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">ロッド修理</h6>
                            <div class="sub_info_text" data-sal="slide-up" data-sal-duration="500">
                                修理部品が切れている場合や特別な事情があった場合、<br>3週間以上の修理期間が掛かるケースもございますので予めご了承ください。
                            </div>
                            <div class="page_fv_img" data-sal="slide-up" data-sal-duration="500">
                                <img src="img/repair_fv.png" alt="">
                            </div>
                            <div class="checklist">
                                <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">ロッドの修理手順</h6>
                                <div class="sub_info_text" data-sal="slide-up" data-sal-duration="500">
                                    ご購入頂いたショップ様からでも、ユーザー様から直接でも修理可能です
                                </div>
                                <ul>
                                    <li data-sal="slide-up" data-sal-duration="500"><span class="check_no">01.</span>まずは半永久保証登録を行ってください。ご登録いただく事で免責金額での修理が可能となります。</li>
                                    <li data-sal="slide-up" data-sal-duration="500"><span class="check_no">02.</span>お買い上げいただいたショップ様に破損ロッドをお預けいただくか、弊社に直接ロッド一式を元払い（送料お客様負担）にてお送りください。</li>
                                    <li data-sal="slide-up" data-sal-duration="500"><span class="check_no">03.</span>弊社に修理品が到着後、修理完了までに3週間程度をいただいております。</li>
                                    <li data-sal="slide-up" data-sal-duration="500"><span class="check_no">04.</span>修理完了後、ショップ様、又はお客様のご自宅にロッドをお送りいたします。修理完了時の返送送料は弊社が負担いたします。</li>
                                </ul>
                                <p class="btn" data-sal="slide-up" data-sal-duration="500"><a href="warranty.php">半永久保証への登録</a></p>

                                <div class="">
                                    <!--<div class="atten">
                                        ご登録いただいた「ご住所」又は「メールアドレス」宛にゼナックからの案内などをお送りさせていただく場合がありますのでご了承ください。<br>
                                        ご登録いただいた情報は半永久保証とゼナックからのご案内などの目的以外には使用いたしません。詳しくは個人情報保護のページをご覧ください。<br>
                                        詳細については以下の保証規定をご覧ください。
                                    </div>-->

                                    <div class="accordion_stock">
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_repair" class="toggle">
                                        <label class="title" for="toggle_repair">ロッド修理について</label>
                                        <div class="content">
                                            <ul class="warranty scrollbarSample" id="scrollbar">
                                                <li>
                                                    <p class="tit">修理ロッドの送り先</p>
                                                    <p class="tex">
                                                        〒669-3166 兵庫県丹波市山南町小野尻 335-1<br>
                                                        TEL：0795-76-2001 (お電話でのお問い合わせは受け付けておりません)<br>
                                                        株式会社 ゼナック
                                                    </p>
                                                </li>
                                                <li>
                                                    <p class="tit">お送りいただくもの</p>
                                                    <p class="tex">
                                                        ・ロッド修理依頼書、または修理箇所や修理内容、お名前、お電話番号、お届け先のご住所が記載されたメモ<br>
                                                        （ロッド修理依頼書はプリントアウトしてご使用ください）<br>
                                                        ・修理ご希望のロッド一式<br>
                                                        （破損したパーツと破損していないパーツ等全て）<br>
                                                        　ハンドルジョイント、2ピース、マルチピースロッドの場合はジョイント調整のために破損していないパーツ（ハンドル等）も全てお送りください。<br>
                                                        ※パーツ紛失等の場合、免責金額が適応できない場合がございますので予めご了承ください。
                                                        <p class="btn dl_icon"><a href="https://zenaq.com/_src/14110/ZENAQ_Rod_Repair_202107.pdf?v=1597731335805" target="_blank">ロッド修理依頼書<img src="img/common/download.svg"></a></p>
                                                    </p>
                                                </li>
                                                <li>
                                                    <p class="tit">修理費用と送料に関して</p>
                                                    <p class="tex">
                                                        ・弊社まで破損ロッドをお送りいただく送料はお客様負担です。<br>
                                                        ・修理が完了したロッドをお返しする際の送料は弊社が負担いたします。<br>
                                                        ・修理をキャンセルされる場合は、ロッドをお返しする際の送料もお客様負担となります。（往復送料負担）<br>
                                                        ・故障パーツ・折損パーツ紛失の場合、免責金額が適応できませんので予めご了承ください。
                                                    </p>
                                                </li>
                                                <li>
                                                    <p class="tit">修理についてのお問合せ</p>
                                                    <p class="tex">
                                                        <p class="btn"><a href="contact.php">お問い合わせはこちら</a></p>
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>