<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>お問い合わせ | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="/">ホーム</a><i class="arrow-icon"></i></li>
            <li>お問い合わせ</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>お問い合わせ</h5>
                    <p>Support</p>
                </div>
                <div class="faq_inner">
                    <div class="left_faq">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li><a href="faq.php">よくある質問</a></li>
                            <li><a href="stock.php">在庫納期リスト</a></li>
                            <li><a href="eol.php">生産終了モデル</a></li>
                            <li><a href="warranty.php">半永久保証</a></li>
                            <li><a href="repair.php">ロッド修理</a></li>
                            <li><a href="trial.php">体感イベント</a></li>
                        </ul>
                    </div>
                    <div class="right_faq">

                        <div class="faq_list">
                            <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">問い合わせ完了しました</h6>

                            <div class="contact_form_col">
                                <div class="contact_form_col_inner">
                                    <div class="sub_info_text" data-sal="slide-up" data-sal-duration="500">
                                        お問い合わせいただきありがとうございます。<br>
                                        のちほど担当者より記入いただいたメールアドレスにご連絡させていただきます。
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>