<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>ZENAQ(ゼナック) | オフィシャルサイト</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="splash">
    <div id="splash_logo">
        <svg version="1.1" id="mask" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="300px" height="" viewBox="0 0 165 30">
        <path fill="#FFFFFF" d="M86.6,2.1h6.3v26.6h-6.1c-0.9,0-2.5-1.6-2.5-1.6L71.5,12.2v16.5H48.6c-2.9,0-4.5-1.8-5.4-3.5
            c-0.3,0.5-1.2,1.9-2.6,4.3c-0.3-0.3-1.3-0.8-2.1-0.8c-13.8,0.1-37.9,0-37.9,0s1.3-3.9,8.6-9.7c0,0,3.5-2.5,4.6-3c0,0,2.6-1,3.9-1.8
            c0,0-3.9-2.7-5.4-2.8c0,0-3.1-0.7-4.6-0.1c0,0,3.5-2.5,9-2.5c0,0,4.5,0.3,6.4,1.1c0,0,2.3-1.5,3.8-5.2H16.7c0,0-4.5,0.3-5,0.8
            l0.5-4.4h28.7c0,0-1.9,2-2,2.5c0,0-3.2,6.3-9.9,9.5c0,0,2.9,2.7,3.3,3.6c0,0,2.2,2.6,3.8,3.1c0,0,2.1,1,5.3,0.9
            c0,0-10.4,0.6-13.7-0.8c0,0-4.3-0.7-6.4-3.1c0,0-2.6,1.8-4.1,2.9c0,0-3.2,2.6-3.5,4.4c0,0,22.5,0,29.1,0c-0.3-0.5-0.5-1.4-0.5-2.1
            V2.1h20.8v5.4H48.6v4.7h13.6v5.4H48.6c0,6.2,3.3,5.9,3.3,5.9h13.3V2.2h5.6c0.9,0,2,1.2,2,1.2L86.6,20V2.1z M118.8,13.9
            c0,0,3.5,9.2,4,14.8h-6.3c0,0-0.7-4.6-2.4-7.9h-11.6c0,0-1.8,5.5-2.3,7.9H94c0,0,1.2-8.6,4-14.9c0,0,4-10,6.5-11.7h7.6
            C112.1,2.1,115.9,5.4,118.8,13.9z M112,15.3l-3.7-6.8l-3.5,6.8H112z M164.3,29.6c-3.6,0.2-12.6,0.3-15.9-2.9c0,0-3,3.3-10.4,3.3
            c0,0-15,0.5-17.3-14c0,0-0.9-15.2,17.5-16c0,0,13.7,0.3,16.3,12.2c0,0,1.2,4.7-1.5,9.6c0,0,3.9,3.2,5.3,5.4
            C158.3,27.2,160.9,29.5,164.3,29.6z M138,4.1c-10.9,0.3-10.4,11.7-10.4,11.7c1.4,11,10.5,10.1,10.5,10.1c4.6-0.2,6-2.1,6-2.1
            c-3.9-3.9-8.9-2.8-8.9-2.8c5.2-3.6,12.2-1.8,12.2-1.8C150,10.6,144.5,3.8,138,4.1z"/>
        </svg>
    </div><!--/splash_logo-->
</div><!--/splash-->

<div id="container">
<div class="wrap">
    
    <!-- header -->
    <div class="header_col fade01">
    <header id="header" class="header">
        <div class="header-inner">
            <div class="menu-container">
                <div class="menu">
                    <ul>
                        <li class="main_list sp_logo"><h1 class="header-logo"><a href="/"><img src="img/common/logo_wh.svg" alt=""></a></h1></li>
                        <li class="main_list hover_h"><a href="#">ABOUT</a>
                            <ul class="top_menu">
                                <li>
                                    <ul>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="about_concept.php">コンセプト</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="about_tech.php">常識破壊</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="about_history.php">ヒストリー</a></span></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="main_list hover_h"><a href="#">NEWS</a>
                            <ul class="top_menu">
                                <li>
                                    <ul>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="news.php">ニュース一覧</a></span></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="main_list hover_h"><a href="#">PRODUCT</a>
                            <ul class="top_menu">
                                <li>
                                    <ul>
                                        <li class="menu_col">
                                            <ul class="menu_col_list">
                                                <li class="title">ロッド</li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav01" class="toggle">
                                                            <label class="title hnav_title" for="toggle_hnav01">ボートジギング</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="product.php">FOKEETO IKARI<!--<span class="new">NEW</span>--></a></li>
                                                                        <li class="line_h"><a href="product.php">Expedition</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav02" class="toggle">
                                                            <label class="title hnav_title" for="toggle_hnav02">ボートキャスティング</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="product.php">Tobizo</a></li>
                                                                        <li class="line_h"><a href="product.php">SINPAA<!--<span class="new">NEW</span>--></a></li>
                                                                        <li class="line_h"><a href="product.php">Expedition</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav03" class="toggle">
                                                            <label class="title hnav_title" for="toggle_hnav03">遠征コンパクトロッド</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="product.php">Expedition</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav04" class="toggle">
                                                            <label class="title hnav_title" for="toggle_hnav04">ロックショア</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="product.php">MUTHOS</a></li>
                                                                        <li class="line_h"><a href="product.php">MUTHOS Sonio</a></li>
                                                                        <li class="line_h"><a href="product.php">MUTHOS Accura</a></li>
                                                                        <li class="line_h"><a href="product.php">MUTHOS Duro</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav05" class="toggle">
                                                            <label class="title hnav_title" for="toggle_hnav05">シーバス</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">PLAISIR ANSWER</a></li>
                                                                        <li class="line_h"><a href="">PLAISIR ANSWER SOPMOD</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav06" class="toggle">
                                                            <label class="title hnav_title" for="toggle_hnav06">ロックフィッシュ</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">SNIPE</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav07" class="toggle">
                                                            <label class="title hnav_title" for="toggle_hnav07">ブラックバス</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">Spirado BLACKART</a></li>
                                                                        <li class="line_h"><a href="">GLANZ</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav08" class="toggle">
                                                            <label class="title hnav_title" for="toggle_hnav08">バンブーワーク</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">Trad 87 (エギング)</a></li>
                                                                        <li class="line_h"><a href="">ASTRA 76 (メバル)</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="menu_col_list">
                                                <li class="title">アクセサリー</li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav08" class="toggle">
                                                            <label class="title hnav_title" for="toggle_hnav08">フィッシングギア</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">3Dショートグローブ</a></li>
                                                                        <li class="line_h"><a href="">フェイス&ネックガード</a></li>
                                                                        <li class="line_h"><a href="">ドライポーター</a></li>
                                                                        <li class="line_h"><a href="">フィールドバッグ</a></li>
                                                                        <li class="line_h"><a href="">ロッドベルト</a></li>
                                                                        <li class="line_h"><a href="">リールストップラバー</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav08" class="toggle">
                                                            <label class="title hnav_title" for="toggle_hnav08">ウェア</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">キャップ</a></li>
                                                                        <li class="line_h"><a href="">Tシャツ</a></li>
                                                                        <li class="line_h"><a href="">ドライロングTシャツ</a></li>
                                                                        <li class="line_h"><a href=""></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="accordion_hnav">
                                                        <div class="option">
                                                            <input type="checkbox" id="toggle_hnav08" class="toggle">
                                                            <label class="title hnav_title" for="toggle_hnav08">ステッカー&ワッペン</label>
                                                            <div class="content">
                                                                <div class="text">
                                                                    <ul>
                                                                        <li class="line_h"><a href="">ステッカー&デカール</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="main_list hover_h"><a href="#">SUPPORT</a>
                            <ul class="top_menu">
                                <li>
                                    <ul>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="faq.php">よくある質問</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="stock.php">在庫納期リスト</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="eol.php">生産終了モデル</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="warranty.php">半永久保証</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="repair.php">ロッド修理</a></span></li>
                                        <li><span class="hover_line"><a class="arrow arrow_icon" href="trial.php">体感イベント</a></span></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="main_list btn"><a href="https://www.zenaq-store.jp/" target="_blank">ONLINE STORE</a></li>
                        <li class="main_list lang">JP<a href="">EN</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </header></div><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="slideshow_col">

            <section id="section-slideshow" data-section-id="slideshow" data-section-type="slideshow" class="fade02">
                <div class="Slideshow__Carousel">
                    <picture>
                        <source srcset="img/hero/slider01_img.png" media="(min-width: 768px)">
                        <img src="img/hero/slider01_img_sp.png" alt="">
                    </picture><!-- /Slideshow__Slide -->

                    <picture>
                        <source srcset="img/hero/slider02_img.png" media="(min-width: 768px)">
                        <img src="img/hero/slider02_img_sp.png" alt="">
                    </picture><!-- /Slideshow__Slide -->

                    <picture>
                        <source srcset="img/hero/slider03_img.png" media="(min-width: 768px)">
                        <img src="img/hero/slider03_img_sp.png" alt="">
                    </picture><!-- /Slideshow__Slide -->
                </div>
                <div class="slider-dots-box"></div>
                <span id="section-slideshow-end" class="Anchor"></span>
            </section>

            <section class="about" id="area">
                <div class="aboutinner">
                    <div class="aboutinner_left">
                        <h2 class="about_title sticky"><img src="img/index/h2_title.svg"></h2>
                    </div>
                    <div class="aboutinner_right">
                        <div class="aboutinner_right_in">
                            <h3 class="about_subtitle" data-sal="slide-up" data-sal-duration="500">世界最高峰のロッドブランク</h3>
                            <p class="about_text" data-sal="slide-up" data-sal-duration="500">
                                1960年の創業から半世紀。<br>
                                どれほど時代が変わっても、我々の基本的なものづくりへの姿勢は変わることはありません。<br>
                                日本国内の自社工場にて脈々と受け継がれる熟練の技で、釣竿の命ともいえるロッドブランクを一本一本丹念に焼き続けています。<br>
                                アングラーの感動を生み出すその瞬間の為に、すべてのロッドに魂を注ぎ込みます。
                            </p>
                            <p class="bg_text">INSPIRE<br>THE SOUL</p>
                        </div>
                        <div class="about_movie" data-sal="slide-up" data-sal-duration="500">
                            <video type="video/webm" src="img/index/movie01.mp4" playsinline loop autoplay muted></video>
                        </div>
                        <div class="about_point">
                            <ul class="">
                                <li data-sal="slide-up" data-sal-duration="500">
                                    <p class="subtitle">I’M AN ANGLER</p>
                                    <h4 class="title">一人のアングラーとして、創る。</h4>
                                    <p class="text">
                                        「フィールドからの答えを製品に反映させる」というコンセプト
                                    </p>
                                </li>
                                <li data-sal="slide-up" data-sal-duration="500">
                                    <p class="subtitle">HANDMADE IN JAPAN</p>
                                    <h4 class="title">最高の品質、日本人のプライド</h4>
                                    <p class="text">
                                        日本人ならではの感性で最高のロッドを創る。
                                    </p>
                                </li>
                            </ul>
                            <div class="l-link" data-sal="slide-up" data-sal-duration="500">
                                <a href="about_concept.php" class="link">
                                    <div class="link-allow">
                                        <span class="link-allow-inner"></span>
                                    </div>
                                </a>
                            </div>
                            <!--<p class="more_btn">
                                <a href="about_concept.php"><span class="underline">Read more</span><span class="circle"><img src="img/common/arrow.svg"></span></a>
                            </p>-->
                        </div>
                    </div>
                </div>
            </section><!-- /about -->

            <section id="app" class="bg1">
                <div class="about_list">
                    <ul class="list horizontal-list">
                        <li class="item list-item" data-target="bg1">
                            <a href="about_concept.php">
                            <div class="list_items_inner">
                                <p class="number">01</p>
                                <p class="title">BRAND<br>CONCEPT</p>
                                <p class="text">コンセプト</p>
                                <p class="more_btn">
                                    <img src="img/common/arrow.svg">
                                </p>
                            </div>
                            </a>
                        </li>
                        <li class="item list-item" data-target="bg2">
                            <a href="about_tech.php">
                            <div class="list_items_inner">
                                <p class="number">02</p>
                                <p class="title"><span>BREAKING</span><br>THE MOLD</p>
                                <p class="text">常識破壊</p>
                                <p class="more_btn">
                                    <img src="img/common/arrow.svg">
                                </p>
                            </div>
                            </a>
                        </li>
                        <li class="item list-item" data-target="bg3">
                            <a href="about_history.php">
                            <div class="list_items_inner">
                                <p class="number">03</p>
                                <p class="title">BRAND-<br>HISTORY</p>
                                <p class="text">ヒストリー</p>
                                <p class="more_btn">
                                    <img src="img/common/arrow.svg">
                                </p>
                            </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </section>

            <section class="pickup">
                <div class="pickup_inner">
                    <div class="pickup_inner_titles" data-sal="slide-up" data-sal-duration="500">
                        <h5>注目ロッド</h5>
                        <p>PickUp</p>
                    </div>
                    <ul class="pickup_list">
                        <li class="pickup_list_item">
                            <div class="pickup_list_item_productimg" data-sal="slide-up" data-sal-duration="500">
                                <p><img src="img/index/pickup_sinpaa_product.png" alt=""></p>
                            </div>
                            <div class="pickup_list_item_inner">
                                
                                <div class="right" data-sal="slide-up" data-sal-duration="500">
                                    <img src="img/index/pickup_sinpaa_img.png" alt="">
                                </div>
                                <div class="left">
                                    <p class="title" data-sal="slide-up" data-sal-duration="500">シンパ</p>
                                    <p class="titleimg" data-sal="slide-up" data-sal-duration="500"><img src="img/index/pickup_sinpaa_title.png" alt=""></p>
                                    <p class="text" data-sal="slide-up" data-sal-duration="500">
                                        ターゲットを絞り込み、細部にまでこだわり抜いたスペシャリティロッド。<br>
                                        これまで幾人が挑戦し困難とされていたビッグワンを仕留めるスペシャリティロッドとして誕生。
                                    </p>
                                    <!--<div class="more_btn">
                                        <a href="product.php"><span class="underline">Read more</span><span class="circle"><img src="img/common/arrow.svg"></span></a>
                                    </div>-->
                                </div>
                            </div>
                        </li>

                        <li class="pickup_list_item">
                            <div class="pickup_list_item_productimg" data-sal="slide-up" data-sal-duration="500">
                                <p><img src="img/index/pickup_tobizo_product.png" alt=""></p>
                            </div>
                            <div class="pickup_list_item_inner">
                                
                                <div class="right" data-sal="slide-up" data-sal-duration="500">
                                    <img src="img/index/pickup_tobizo_img.png" alt="">
                                </div>
                                <div class="left">
                                    <p class="title" data-sal="slide-up" data-sal-duration="500">トビゾー</p>
                                    <p class="titleimg" data-sal="slide-up" data-sal-duration="500"><img src="img/index/pickup_tobizo_title.png" alt=""></p>
                                    <p class="text" data-sal="slide-up" data-sal-duration="500">
                                        「20年ぶりの革新的なカーボン」と謳われる「東レ ”TORAYCA® T1100G” 」、更に「ナノアロイ®樹脂」で Tobizoのバックボーンを成型。<br>
                                        圧倒的なパフォーマンスを持つ「世界最高峰のキャスティングロッド」が誕生。
                                    </p>
                                    <!--<div class="more_btn">
                                        <a href="product.php"><span class="underline">Read more</span><span class="circle"><img src="img/common/arrow.svg"></span></a>
                                    </div>-->
                                </div>

                            </div>
                        </li>

                        <li class="pickup_list_item">
                            <div class="pickup_list_item_productimg" data-sal="slide-up" data-sal-duration="500">
                                <p><img src="img/index/pickup_mutos_product.png" alt=""></p>
                            </div>
                            <div class="pickup_list_item_inner">
                                
                                <div class="right" data-sal="slide-up" data-sal-duration="500">
                                    <img src="img/index/pickup_mutos_img.png" alt="">
                                </div>
                                <div class="left">
                                    <p class="title" data-sal="slide-up" data-sal-duration="500">ミュートス</p>
                                    <p class="titleimg" data-sal="slide-up" data-sal-duration="500"><img src="img/index/pickup_mutos_title.png" alt=""></p>
                                    <p class="text" data-sal="slide-up" data-sal-duration="500">
                                        派手なデザインも不要なパーツもすべて排除し、非常識でストイックに本質のみを具現化。<br>
                                        モノマネでは練り上げる事の出来ない、実釣からのみ生まれる実践ロッド。
                                    </p>
                                    <!--<div class="more_btn">
                                        <a href="product.php"><span class="underline">Read more</span><span class="circle"><img src="img/common/arrow.svg"></span></a>
                                    </div>-->
                                </div>

                            </div>
                        </li>
                    </ul>
                    <div class="more_product" data-sal="slide-up" data-sal-duration="500">
                        <a href="product_list.php">ALL PRODUCTS</a>
                    </div>
                </div>
            </section>

            <section class="info">
                <div class="info_inner">
                    <a href="warranty.php" class="" data-sal="slide-up" data-sal-duration="500">
                        <div class="info_inner_btn">
                            <div class="left">
                                <p class="subtitle">WARRANTY</p>
                                <p class="title">半永久保証</p>
                            </div>
                            <div class="right">
                                <p class="circle"><img src="img/common/arrow.svg"></p>
                            </div>
                        </div>
                    </a>

                    <div class="info_inner_news">
                        <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                            <h5>新着情報</h5>
                            <p>News</p>
                        </div>
                        <ul class="info_inner_news_list">
                            <li class="" data-sal="slide-up" data-sal-duration="500">
                                <span>2021.05.21</span><br class="pc_br">生産終了モデルのお知らせ
                            </li>
                            <li class="" data-sal="slide-up" data-sal-duration="500">
                                <span>2021.05.13</span><br class="pc_br">セグメントオーダー5月受注分は品切れになりました
                            </li>
                            <li class="" data-sal="slide-up" data-sal-duration="500">
                                <span>2021.04.26</span><br class="pc_br">セグメントオーダー（数量限定受注）のご案内
                            </li>
                        </ul>
                        <div class="l-link" data-sal="slide-up" data-sal-duration="500">
                            <a href="about_concept.php" class="link">
                                <div class="link-allow">
                                    <span class="link-allow-inner"></span>
                                </div>
                            </a>
                        </div>
                        <!--<div class="more_news">
                            <a href="news.php"><span class="underline">Read more</span><span class="circle"><img src="img/common/arrow.svg"></span></a>
                        </div>-->
                    </div>
                </div>
            </section>

            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->
</div><!-- /container -->

<?php include('inc/script.php'); ?>

</body>
</html>