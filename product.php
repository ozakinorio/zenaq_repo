<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>商品詳細 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li><a href="">ロッド</a><i class="arrow-icon"></i></li>
            <li><a href="">ボートキャスティング</a><i class="arrow-icon"></i></li>
            <li>Tobizo</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other_product">

            <div class="otherinner">
                <div class="title_col" data-sal="slide-up" data-sal-duration="500">
                    <h3>Tobizo<span>トビゾー</span></h3>
                    <span>/ボートキャスティング</span>
                </div><!-- /title_col -->

                <div class="head_info_product">
                    <div class="left">
                        <ul class="sns_list" data-sal="slide-up" data-sal-duration="500">
                            <li><a href="" target="_blank"><img src="img/common/icon_fb_bk.svg" alt=""></a></li>
                            <li><a href="" target="_blank"><img src="img/common/icon_tw_bk.svg" alt=""></a></li>
                            <li class="text">SHARE</li>
                        </ul>
                    </div>
                    <div class="right">
                        <div class="right_head_info">
                            <div class="text_editor">
                                <h4 data-sal="slide-up" data-sal-duration="500">キャスティングロッド20年のノウハウ×20年ぶりの革新的カーボン</h4>
                                <p data-sal="slide-up" data-sal-duration="500">
                                    1997年、初代キャスティングロッド“ジガートラストP83-5” 発売以来、「20年に及ぶノウハウ」。<br>
                                    そして今、「20年ぶりの革新的なカーボン」と謳われる「東レ ”TORAYCA® T1100G” 」、更に「ナノアロイ®樹脂」で Tobizoのバックボーンを成型。<br>
                                    圧倒的なパフォーマンスを持つ「世界最高峰のキャスティングロッド」が誕生。
                                </p>
                            </div>
                        </div>

                        <div class="right_head_slider">
                            <ul class="slider" data-sal="slide-up" data-sal-duration="500">
                                <li><a href="img/product/tozizo/head_slider/01.png" class="popup"><img alt="" src="img/product/tozizo/head_slider/01.png"></a></li>
                                <li><a href="img/product/tozizo/head_slider/01.png" class="popup"><img alt="" src="img/product/tozizo/head_slider/01.png"></a></li>
                                <li><a href="img/product/tozizo/head_slider/01.png" class="popup"><img alt="" src="img/product/tozizo/head_slider/01.png"></a></li>
                                <li><a href="img/product/tozizo/head_slider/01.png" class="popup"><img alt="" src="img/product/tozizo/head_slider/01.png"></a></li>
                                <li><a href="img/product/tozizo/head_slider/01.png" class="popup"><img alt="" src="img/product/tozizo/head_slider/01.png"></a></li>
                            </ul>
                            <ul class="thumb" data-sal="slide-up" data-sal-duration="500">
                                <li><img src="img/product/tozizo/head_slider/01.png" alt=""></li>
                                <li><img src="img/product/tozizo/head_slider/01.png" alt=""></li>
                                <li><img src="img/product/tozizo/head_slider/01.png" alt=""></li>
                                <li><img src="img/product/tozizo/head_slider/01.png" alt=""></li>
                                <li><img src="img/product/tozizo/head_slider/01.png" alt=""></li>
                            </ul>
                        </div>

                        <div class="right_head_spec" data-sal="slide-up" data-sal-duration="500">
                            <table>
                                <tr>
                                    <th>Tobizo</th>
                                    <th>Length (ft)</th>
                                    <th>Lure WT Max (g)</th>
                                    <th>Line WT (PE #)</th>
                                </tr>
                                <tr>
                                    <td>TC80-50G</td>
                                    <td>8’0”</td>
                                    <td>~80</td>
                                    <td>PE 3~4</td>
                                </tr>
                                <tr>
                                    <td>TC80-50G</td>
                                    <td>8’0”</td>
                                    <td>~80</td>
                                    <td>PE 3~4</td>
                                </tr>
                                <tr>
                                    <td>TC80-50G</td>
                                    <td>8’0”</td>
                                    <td>~80</td>
                                    <td>PE 3~4</td>
                                </tr>
                                <tr>
                                    <td>TC80-50G</td>
                                    <td>8’0”</td>
                                    <td>~80</td>
                                    <td>PE 3~4</td>
                                </tr>
                            </table>
                        </div>

                        <div class="right_head_gallery" data-sal="slide-up" data-sal-duration="500">
                            <ul class="slick_gallery">
                                <li><a href="img/product/tozizo/gallery/01.jpg" class="popup"><img alt="" src="img/product/tozizo/gallery/01.jpg"></a></li>
                                <li><a href="img/product/tozizo/gallery/02.jpg" class="popup"><img alt="" src="img/product/tozizo/gallery/02.jpg"></a></li>
                                <li><a href="img/product/tozizo/gallery/03.jpg" class="popup"><img alt="" src="img/product/tozizo/gallery/03.jpg"></a></li>
                                <li><a href="img/product/tozizo/gallery/04.jpg" class="popup"><img alt="" src="img/product/tozizo/gallery/04.jpg"></a></li>
                                <li><a href="img/product/tozizo/gallery/01.jpg" class="popup"><img alt="" src="img/product/tozizo/gallery/01.jpg"></a></li>
                                <li><a href="img/product/tozizo/gallery/02.jpg" class="popup"><img alt="" src="img/product/tozizo/gallery/02.jpg"></a></li>
                                <li><a href="img/product/tozizo/gallery/03.jpg" class="popup"><img alt="" src="img/product/tozizo/gallery/03.jpg"></a></li>
                                <li><a href="img/product/tozizo/gallery/04.jpg" class="popup"><img alt="" src="img/product/tozizo/gallery/04.jpg"></a></li>
                            </ul>
                            <p class="flickr"><a href="https://www.flickr.com/photos/zenaq/albums/72157680736020281" target="_blank">ギャラリー<img class="slide-arrow" src="img/common/blank.svg"></a></p>
                        </div>
                    </div>
                </div><!-- /head_info_product -->

                <div class="product_detail">
                    <ul class="tab">
                        <li><a href="#overview"><p class="subtitle">OVERVIEW</p><p class="title">製品概要</p></a></li>
                        <li><a href="#model"><p class="subtitle">MODEL</p><p class="title">モデル</p></a></li>
                        <li><a href="#comparison"><p class="COMPARISON">OVERVIEW</p><p class="title">比 較</p></a></li>
                    </ul>
                        
                    <div id="overview" class="area">
                        <div class="detail_overview">
                            <div class="detail_left">
                                <p class="main_img"><img src="img/product/tozizo/img01.png" alt=""></p>
                            </div>
                            <div class="detail_right">
                                <div>
                                <p class="detail_number">POINT.01</p>
                                <h4 class="detail_h4">追求したのは“圧倒的な飛び”</h4>
                                <p class="detail_subtitle">トビゾーに求めたのは“飛び”。 ”飛び”は釣果に直結する。</p>
                                <h5 class="detail_h5">飛距離のアドバンテージ</h5>
                                <p class="detail_text">
                                    <strong>「魚に警戒させないポジションからのキャスティング」</strong><br>
                                    <u>当然、ナブラ打ちでも大きなアドバンテージ。</u><br>
                                    <br>
                                    「ヒットゾーンを長くトレースし同船者よりも広範囲に探れる」<br>
                                    これはキャスティングでは必須であり確実に釣果に差をつける。例えば飛距離が10m長いという事は100回のキャストで1000m(1km) もの距離の差が出る！<br>
                                    <br>
                                    「アングラーの体力温存」<br>
                                    １日の釣行では体力の消耗は激しい。トビゾーは80%の力で投げた場合でもよく飛ぶ。「より多くキャスト出来る」すなわち、確実にヒット率が上がる。
                                </p>
                                </div>
                            </div>
                        </div>

                        <div class="detail_overview">
                            <div class="detail_right">
                                <div>
                                <p class="detail_number">POINT.02</p>
                                <h4 class="detail_h4">相反する課題“掛けた魚を浮かすリフトパワー”</h4>
                                <p class="detail_text">
                                    バットパワーという名の「ただ硬いだけのバット」ではビッグワンとのファイトでは人間が確実に不利になる。ロッドはのされていなくとも人間がのされてしまう。であれば、むしろバットパワーが無いほうがマシである。
                                </p>
                                <h5 class="detail_h5">本当の意味でのバットパワー</h5>
                                <p class="detail_text">
                                    “釣り人がロッドに負荷を掛けやすく、掛けた負荷をロッドが確実に復元してくれる力”のことを言う。<br>
                                    この復元力の強いロッドがバットパワーの有るロッドであり、これにより体力ぎりぎりのファイトをロッドがサポートしてくれる。「ルアーの飛距離」だけに拘ればこの問題が置き去りになる。<br>
                                    しかしこのリフトパワーは待望のビッグワンと対峙する時には最も重要なロッド性能になる。
                                </p>
                                <h5 class="detail_h5">超筋肉質のスーパーブランク</h5>
                                <p class="detail_text">
                                    この「キャスト飛距離」≠「リフトパワー」の相反する問題を高い次元で克服できたのは、まさに時期を同じくして開発されたスーパーカーボン素材、東レTORAYCA® T1100Gとゼナックのブランク成型技術の融合であった。<br>
                                    東レもまたカーボン技術における難問とされてきた「高強度」≠「高弾性」と言う、相反する課題をこの素材で見事にクリアーしていた。更にはナノアロイ®樹脂を配合しロッドに成型することで今までにない「超筋肉質のスーパーブランク」が完成した。
                                </p>
                                </div>
                            </div>
                            <div class="detail_left">
                                <p class="main_img"><img src="img/product/tozizo/img02.png" alt=""></p>
                                <p class="sub_img"><img src="img/product/tozizo/img03.svg" alt=""></p>
                            </div>
                        </div><!-- /detail_overview -->

                        <div class="detail_overview">
                            <div class="detail_accordion">
                                <div class="option_detail_accordion">
                                    <input type="checkbox" id="toggle10_1" class="toggle">
                                    <label class="title" for="toggle10_1">“ヘキサゴン” フロントグリップの威力<p>/HEXAGON GRIP FOR POWER RODS</p></label>
                                    <div class="content">

                                        <div class="fature_area">
                                            <div class="left">
                                                <div class="hexagongrip_movie">
                                                    <a href="javascript:void(0);" data-video-id="ASNdZIMfSIo" class="js-modal-video"><img src="img/product/tozizo/hexagongrip_img.png" alt=""></a>
                                                </div>
                                            </div>
                                            <div class="right">
                                                フロントグリップを「握った時の指の形」に合わせ込んだ。<br>
                                                この3角形をベースにゼナック独自の「変則の6角形」＝「ヘキサゴングリップ®」にする事で大きなアドバンテージをアングラーに与える事が出来た。<br>
                                                トビゾーのフロントグリップは長めに設定することでパワーファイトが可能。<br>
                                                <br>
                                                TC80-50G/ TC80-80Gは200mm。TC86-110G/ TC83-150G/TC80-200Gは300mmに設定
                                            </div>
                                        </div><!--/fature_area-->

                                        <div class="fature_area">
                                            <div class="right">
                                                <p>Advantage.01</p>
                                            </div>
                                            <div class="left">
                                                <p class="fature_title">“より少ない握力でロッドを保持できる”</p>
                                                <p class="fature_text">
                                                    体力ぎりぎりのビッグファイト経験の有るアングラーならこの意味が理解できるだろう。<br>
                                                    長時間のファイトやビッグワンとのファイトで一番失うのが握力。握力が無くなれば、もはやリフトするパワーは半減してしまう。このグリップならより少ない握力でロッドをしっかり保持しながらリフトできる。スピニングリールで力強いリーリングをする為には「ロッドの横揺れ」（ローリング）をしっかり止めなければならない。少ない握力でこの横揺れを止められるからこそ握力を温存できる。<br>
                                                    渾身のリフティングとリーリングを繰り返す長時間ファイトに大きなアドバンテージが生まれる。<br>
                                                    記録級のビッグワンをランディングまで持ち込む確率を大きく上げる為の「ヘキサゴングリップ」
                                                </p>
                                            </div>
                                        </div><!--/fature_area-->

                                        <div class="fature_area">
                                            <div class="right">
                                                <p>Advantage.02</p>
                                            </div>
                                            <div class="left">
                                                <p class="fature_title">“より的確なルアー操作のために”</p>
                                                <p class="fature_text">
                                                    重いルアーや激しくアクションを付ける時もフロントグリップを安定して握る事が出来るので「楽に」そしてより「正確なルアー操作」が可能。ルアーの持つ本来のアクションを確実に演出することがヒット率向上のカギになる。体力的にも有利になるので長時間のルアー操作も可能になり釣果アップに直結する。
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="gallery_overview">
                            <div>
                                <h3><p>Garally</p>詳細写真</h3>
                                <h4 class="gallery_overview_title">ライト～ミディアムゲーム用RGガイドセッティング</h4>
                                <ul class="gallery_list">
                                    <li class="hover">
                                        <div class="hover-img">
                                            <p class="gallery_list_img"><img alt="" src="img/product/tozizo/gallery/guide/light/tob_01.png"></p>
                                            <p class="gallery_list_title">Top guide & assist guide - TC80-50G</p>
                                        </div>
                                        <div class="hover-text">
                                            <p class="text">RGガイドシステム搭載<br>Fujiチタンフレーム+SICガイドリング(シングルフットKガイド)を最適なガイド数とポジションに配置。 飛距離とPEライントラブルも大きく解消しストレスのないキャスティングを約束。 トップガイドのすぐ下に配置した「アシストガイド」がライントラブルを防止。</p>
                                        </div>
                                    </li>
                                    <li class="hover">
                                        <div class="hover-img">
                                            <p class="gallery_list_img"><img alt="" src="img/product/tozizo/gallery/guide/light/tob_02.png"></p>
                                            <p class="gallery_list_title">Top guide & assist guide - TC80-80G</p>
                                        </div>
                                        <div class="hover-text">
                                            <p class="text">RGガイドシステム搭載<br>Fujiチタンフレーム+SICガイドリング(ダブルフットKガイド)を採用。 「飛距離」や「RGガイドシステムのメリット」はもちろんの事、耐久性と強度を誇るガイドセッティング。 トップガイドのすぐ下に配置した「アシストガイド」がライントラブルを防止。</p>
                                        </div>
                                    </li>
                                    <li class="hover">
                                        <div class="hover-img">
                                            <p class="gallery_list_img"><img alt="" src="img/product/tozizo/gallery/guide/light/tob_03.png"></p>
                                            <p class="gallery_list_title">Butt guide - TC80-50G & TC80-80G</p>
                                        </div>
                                        <div class="hover-text">
                                            <p class="text">ライト～ミディアムゲームのRGガイドシステム、バットガイド。 Fujiチタンフレーム+SICガイドリング(T-LCSG20)を採用。 キャスト直後から「PEラインのバタつきを抑え」爽快で「圧倒的な飛距離」をもたらす。</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div>
                                <h4 class="gallery_overview_title">ヘビーゲーム用オーシャンガイドセッティング</h4>
                                <ul class="slick_gallery02">
                                    <li class="hover">
                                        <div class="hover-img">
                                            <p class="gallery_list_img"><img alt="" src="img/product/tozizo/gallery/guide/heavy/tob_01.png"></p>
                                            <p class="gallery_list_title">Top guide & Tip section - TC86-110G / TC83-150G / TC80-200G</p>
                                        </div>
                                        <div class="hover-text">
                                            <p class="text">ヘビーゲーム用に最も信頼性の高い「Fujiオーシャンフレーム+ SICガイドリング」に“イオンプレーティング”を施し、ソルトゲームにおける最も必要な耐久性と最高の強度を誇るガイドセッティング。 遠征先での故障やビッグフィッシュとのファイトも高い信頼性でサポート。</p>
                                        </div>
                                    </li>
                                    <li class="hover">
                                        <div class="hover-img">
                                            <p class="gallery_list_img"><img alt="" src="img/product/tozizo/gallery/guide/heavy/tob_02.png"></p>
                                            <p class="gallery_list_title">Butt guide - TC86-110G / TC83-150G / TC80-200G</p>
                                        </div>
                                        <div class="hover-text">
                                            <p class="text">すべてのTobizoにはダブルラッピング方式でガイドをしっかり固定。ガイドの取り付けからエポキシコーティング技術は長年多くのソルトウォーターロッドを作り込んできたゼナックならではのノウハウとHandmade in Japan.の名に恥じない高い技術力で完成度の高さを誇ります。</p>
                                        </div>
                                    </li>
                                    <li class="hover">
                                        <div class="hover-img">
                                            <p class="gallery_list_img"><img alt="" src="img/product/tozizo/gallery/guide/heavy/tob_03.png"></p>
                                            <p class="gallery_list_title">Tobizo ネーム</p>
                                        </div>
                                        <div class="hover-text">
                                            <p class="text">ヘビーゲーム用に最も信頼性の高い「Fujiオーシャンフレーム+ SICガイドリング」に“イオンプレーティング”を施し、ソルトゲームにおける最も必要な耐久性と最高の強度を誇るガイドセッティング。 遠征先での故障やビッグフィッシュとのファイトも高い信頼性でサポート。</p>
                                        </div>
                                    </li>
                                    <li class="hover">
                                        <div class="hover-img">
                                            <p class="gallery_list_img"><img alt="" src="img/product/tozizo/gallery/guide/heavy/tob_04.png"></p>
                                            <p class="gallery_list_title">ハーフサンディング＆ジェットブラックペイント</p>
                                        </div>
                                        <div class="hover-text">
                                            <p class="text">すべてのTobizoにはダブルラッピング方式でガイドをしっかり固定。ガイドの取り付けからエポキシコーティング技術は長年多くのソルトウォーターロッドを作り込んできたゼナックならではのノウハウとHandmade in Japan.の名に恥じない高い技術力で完成度の高さを誇ります。</p>
                                        </div>
                                    </li>


                                    <li>
                                        <p class="gallery_list_img"><a href="img/product/tozizo/gallery/guide/heavy/tob_05.png" class="popup"><img alt="" src="img/product/tozizo/gallery/guide/heavy/tob_05.png"></a></p>
                                        <p class="gallery_list_title">Wナットリング [PAT.]</p>
                                    </li>
                                    <li>
                                        <p class="gallery_list_img"><a href="img/product/tozizo/gallery/guide/heavy/tob_06.png" class="popup"><img alt="" src="img/product/tozizo/gallery/guide/heavy/tob_06.png"></a></p>
                                        <p class="gallery_list_title">リールシートとWナットリング [PAT.]</p>
                                    </li>
                                    <li>
                                        <p class="gallery_list_img"><a href="img/product/tozizo/gallery/guide/heavy/tob_07.png" class="popup"><img alt="" src="img/product/tozizo/gallery/guide/heavy/tob_07.png"></a></p>
                                        <p class="gallery_list_title">リアグリップ</p>
                                    </li>
                                    <li>
                                        <p class="gallery_list_img"><a href="img/product/tozizo/gallery/guide/heavy/tob_08.png" class="popup"><img alt="" src="img/product/tozizo/gallery/guide/heavy/tob_08.png"></a></p>
                                        <p class="gallery_list_title">ハーネスリング</p>
                                    </li>
                                    <li>
                                        <p class="gallery_list_img"><a href="img/product/tozizo/gallery/guide/heavy/tob_09.png" class="popup"><img alt="" src="img/product/tozizo/gallery/guide/heavy/tob_09.png"></a></p>
                                        <p class="gallery_list_title">ハーネスリング（ハーネス装着イメージ）</p>
                                    </li>
                                    <li>
                                        <p class="gallery_list_img"><a href="img/product/tozizo/gallery/guide/heavy/tob_10.png" class="popup"><img alt="" src="img/product/tozizo/gallery/guide/heavy/tob_10.png"></a></p>
                                        <p class="gallery_list_title">ハイカコルクエンドキャップ</p>
                                    </li>
                                    <li>
                                        <p class="gallery_list_img"><a href="img/product/tozizo/gallery/guide/heavy/tob_01.png" class="popup"><img alt="" src="img/product/tozizo/gallery/guide/heavy/tob_01.png"></a></p>
                                        <p class="gallery_list_title">ラバーエンドキャップ</p>
                                    </li>
                                    <li>
                                        <p class="gallery_list_img"><a href="img/product/tozizo/gallery/guide/heavy/tob_01.png" class="popup"><img alt="" src="img/product/tozizo/gallery/guide/heavy/tob_01.png"></a></p>
                                        <p class="gallery_list_title">ヘキサゴングリップ</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- /overview -->

                    <div id="model" class="area">
                        <div class="model_list">
                            <ul>
                                <li>
                                    <div class="model_titles">
                                        <div class="left">
                                            <p class="subtitle">for Technical game</p>
                                            <p class="title">TC80-50G<span class="">NEW</span></p>
                                        </div>
                                        <div class="right">
                                            <ul>
                                                <li>東レ TORAYCA® T1100G</li>
                                                <li>RG guide system</li>
                                                <li class="img"><img src="img/product/hexagongrip_icon.png" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="model_rodimg">
                                        <div id="image1" class="imagearea">
                                            <img src="img/product/tozizo/rod_img.png" alt="" width="200" />
                                            <div class=loupe style="background-image:url(img/product/tozizo/rod_img.png)" ></div>
                                        </div>
                                    </div>

                                    <div class="model_btns">
                                        <ul class="model_btns_list">
                                            <li><a href="">キャストカーブ</a></li>
                                            <li><a href="">ロッドカーブ</a></li>
                                            <li><a href="" target="_blank">オフィシャルストア</a></li>
                                            <li><a href="" target="_blank">釣果ギャラリー</a></li>
                                        </ul>
                                        <div class="model_movie_list">
                                            <div class="accordion_model_movielist">
                                                <div class="option">
                                                    <input type="checkbox" id="toggle2_1" class="toggle">
                                                    <label class="title" for="toggle2_1">ムービーリスト</label>
                                                    <div class="content">
                                                        <ul class="model_movie_list_items">
                                                            <li>
                                                                <div class="hexagongrip_movie">
                                                                    <a href="javascript:void(0);" data-video-id="ASNdZIMfSIo" class="js-modal-video"><img src="img/product/tozizo/hexagongrip_img.png" alt=""></a>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="hexagongrip_movie">
                                                                    <a href="javascript:void(0);" data-video-id="ASNdZIMfSIo" class="js-modal-video"><img src="img/product/tozizo/hexagongrip_img.png" alt=""></a>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="hexagongrip_movie">
                                                                    <a href="javascript:void(0);" data-video-id="ASNdZIMfSIo" class="js-modal-video"><img src="img/product/tozizo/hexagongrip_img.png" alt=""></a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="model_texts">
                                        <div class="model_head_text">
                                            <div class="left">
                                                <p class="title">＜ターゲット＞</p>
                                                <p>ヒラマサ、ブリ、マグロ、カツオ、シイラ等。3～10キロ程度サイズを対象。</p>
                                            </div>
                                            <div class="right">
                                                Length: 8’0” / Lure: 30-80g / PE Line: MAX#4<br>
                                                Daiwa 0000～0000番 / Shimano 0000～0000番
                                            </div>
                                        </div>
                                        <div class="model_bottom_text">
                                            <div class="grad-wrap">
                                                <input id="trigger1" class="grad-trigger" type="checkbox">
                                                <label class="grad-btn" for="trigger1"><p class="plus"></p>続きを読む</label>
                                                <div class="grad-item">
                                                    正確なキャスタビリティー、そしてRGガイドシステム搭載の飛距離を武器にブルーランナーなど多くのターゲットが対象。キャストの飛距離にもこだわりながらも50g前後のプラグなどを高弾性カーボンの張りでキレのあるルアー操作が可能。<br>
                                                    ツイッチなどのテクニカルなアクションでターゲットを繊細にかつアグレッシブに攻略するモデル。喰いの渋い状況でのショートバイトも繊細なティップがしっかりと追随しフッキングに持ち込める完成度の高いブランク。<br>
                                                    正確なキャスタビリティー、そしてRGガイドシステム搭載の飛距離を武器にブルーランナーなど多くのターゲットが対象。キャストの飛距離にもこだわりながらも50g前後のプラグなどを高弾性カーボンの張りでキレのあるルアー操作が可能。<br>
                                                    ツイッチなどのテクニカルなアクションでターゲットを繊細にかつアグレッシブに攻略するモデル。喰いの渋い状況でのショートバイトも繊細なティップがしっかりと追随しフッキングに持ち込める完成度の高いブランク。<br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div><!--/model-->


                    <div id="comparison" class="area">
                        <div class="gallery_overview">
                            <div class="comparison_col">
                                <div class="comparison_col_inner" style="display: block;">
                                    <h3><p>Spec</p>スペック</h3>
                                    <div class="table_col">
                                    <table>
                                        <tr>
                                            <th class="name" rowspan="2">Tobizo<br><span>(SPINNING MODEL)</span></th>
                                            <th rowspan="2">Length (ft)</th>
                                            <th rowspan="2">Joint<br>(pce)</th>
                                            <th rowspan="2">Closed Length<br>(cm)</th>
                                            <th rowspan="2">Lure WT Max<br>(g)</th>
                                            <th rowspan="2">Lure WT Best Match<br>(g)</th>
                                            <th rowspan="2">Line WT<br>(PE #)</th>
                                            <th rowspan="2">Rod’sweight (average)<br>(g)</th>
                                            <th colspan="3">Grip length</th>
                                            <th rowspan="2">Guide<br>(Fuji parts)</th>
                                            <th rowspan="2">Reel seat size<br>(Fuji parts)</th>
                                        </tr>
                                        <tr>
                                            <td class="gray">A</td>
                                            <td class="gray">B</td>
                                            <td class="gray">C</td>
                                        </tr>

                                        <tr>
                                            <td>TC80-50G</td>
                                            <td>8’0”</td>
                                            <td>Offset Handle</td>
                                            <td>181</td>
                                            <td>~80</td>
                                            <td>30~70</td>
                                            <td>PE 3~4</td>
                                            <td>265</td>
                                            <td>710</td>
                                            <td>460</td>
                                            <td>200</td>
                                            <td>RG</td>
                                            <td>DPS-20</td>
                                        </tr>
                                        <tr>
                                            <td>TC80-50G</td>
                                            <td>8’0”</td>
                                            <td>Offset Handle</td>
                                            <td>181</td>
                                            <td>~80</td>
                                            <td>30~70</td>
                                            <td>PE 3~4</td>
                                            <td>265</td>
                                            <td>710</td>
                                            <td>460</td>
                                            <td>200</td>
                                            <td>RG</td>
                                            <td>DPS-20</td>
                                        </tr>
                                        <tr>
                                            <td>TC80-50G</td>
                                            <td>8’0”</td>
                                            <td>Offset Handle</td>
                                            <td>181</td>
                                            <td>~80</td>
                                            <td>30~70</td>
                                            <td>PE 3~4</td>
                                            <td>265</td>
                                            <td>710</td>
                                            <td>460</td>
                                            <td>200</td>
                                            <td>RG</td>
                                            <td>DPS-20</td>
                                        </tr>
                                        <tr>
                                            <td>TC80-50G</td>
                                            <td>8’0”</td>
                                            <td>Offset Handle</td>
                                            <td>181</td>
                                            <td>~80</td>
                                            <td>30~70</td>
                                            <td>PE 3~4</td>
                                            <td>265</td>
                                            <td>710</td>
                                            <td>460</td>
                                            <td>200</td>
                                            <td>RG</td>
                                            <td>DPS-20</td>
                                        </tr>
                                        <tr>
                                            <td>TC80-50G</td>
                                            <td>8’0”</td>
                                            <td>Offset Handle</td>
                                            <td>181</td>
                                            <td>~80</td>
                                            <td>30~70</td>
                                            <td>PE 3~4</td>
                                            <td>265</td>
                                            <td>710</td>
                                            <td>460</td>
                                            <td>200</td>
                                            <td>RG</td>
                                            <td>DPS-20</td>
                                        </tr>
                                    </table>
                                    </div>
                                </div>
                            </div>

                            <div class="comparison_col">
                                <div class="comparison_col_inner">
                                    <div class="left_long">
                                        <h3><p>Boat Casting Rod</p>ロッドチャート</h3>
                                        <p class="rod_chart"><img src="img/product/tozizo/boat _castingrod.svg" alt=""></p>
                                    </div>
                                    <div class="right_short">
                                        <h3><p>Rod curve</p>ロッドカーブ</h3>
                                        <table>
                                            <tr>
                                                <th class="name">モデル</th>
                                                <th>キャスティングロッドカーブ<br><span>キャスト時のロッドカーブ</span></th>
                                            </tr>
                                            <tr>
                                                <td>TC80-50G</td>
                                                <td>
                                                    <a href="https://www.youtube.com/watch?v=Rm__VcWyLBg&t=18s" class="popup-youtube">MOVIE PLAY<i class="fab fa-youtube"></i</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>TC80-50G</td>
                                                <td><a href="">MOVIE PLAY<i class="fab fa-youtube"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>TC80-50G</td>
                                                <td><a href="">MOVIE PLAY<i class="fab fa-youtube"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>TC80-50G</td>
                                                <td><a href="">MOVIE PLAY<i class="fab fa-youtube"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>TC80-50G</td>
                                                <td><a href="">MOVIE PLAY<i class="fab fa-youtube"></i></a></td>
                                            </tr>
                                            <tr>
                                                <th class="name">モデル</th>
                                                <th>バーチカルロッドカーブ<br><span>垂直に負荷をかけた時のロッドカーブ</span></th>
                                            </tr>
                                            <tr>
                                                <td>TC80-50G</td>
                                                <td><a href="">PHOTO SHOW<i class="fas fa-images"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>TC80-50G</td>
                                                <td><a href="">PHOTO SHOW<i class="fas fa-images"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>TC80-50G</td>
                                                <td><a href="">PHOTO SHOW<i class="fas fa-images"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>TC80-50G</td>
                                                <td><a href="">PHOTO SHOW<i class="fas fa-images"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>TC80-50G</td>
                                                <td><a href="">PHOTO SHOW<i class="fas fa-images"></i></a></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="comparison_col">
                                <div class="comparison_col_inner" style="display: block;">
                                    <h3><p>Spec</p>ルアー適合表</h3>
                                    <div class="table_col">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <th rowspan="2">モデル</th>
                                                <th colspan="8">ジグ</th>
                                                <th colspan="2">ミノー</th>
                                                <th colspan="3">ダイビングペンシル</th>
                                                <th colspan="3">ポッパー</th>
                                            </tr>
                                            <tr>
                                                <td class="gray">15g</td>
                                                <td class="gray">20g</td>
                                                <td class="gray">30g</td>
                                                <td class="gray">40g</td>
                                                <td class="gray">50g</td>
                                                <td class="gray">60g</td>
                                                <td class="gray">80g</td>
                                                <td class="gray">100g</td>
                                                <td class="gray">-</td>
                                                <td class="gray">-</td>
                                                <td class="gray">-</td>
                                                <td class="gray">-</td>
                                                <td class="gray">-</td>
                                                <td class="gray">-</td>
                                                <td class="gray">-</td>
                                                <td class="gray">-</td>
                                            </tr>
                                            <tr>
                                                <td>TC80-50G</td>
                                                <td>○</td>
                                                <td>○</td>
                                                <td>○</td>
                                                <td>○</td>
                                                <td>○</td>
                                                <td>○</td>
                                                <td>○</td>
                                                <td></td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td></td>
                                                <td>◎</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>◎</td>
                                            </tr>
                                            <tr>
                                                <td>TC80-80G</td>
                                                <td>○</td>
                                                <td>○</td>
                                                <td>○</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td></td>
                                                <td>○</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>◎</td>
                                            </tr>
                                            <tr>
                                                <td>TC86-110G</td>
                                                <td>◎</td>
                                                <td></td>
                                                <td>◎</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>◎</td>
                                                <td></td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>○</td>
                                                <td>○</td>
                                                <td>○</td>
                                                <td></td>
                                                <td>◎</td>
                                            </tr>
                                            <tr>
                                                <td>TC83-150G</td>
                                                <td>○</td>
                                                <td>○</td>
                                                <td>○</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                            </tr>
                                            <tr>
                                                <td>TC80-200G</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td>◎</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>○</td>
                                                <td>○</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>

                            <div class="comparison_col">
                                <div class="comparison_col_inner" style="display: block;">
                                    <h3><p>Spec</p>リール適合表</h3>
                                    <div class="table_col">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <th rowspan="3">モデル</ths>
                                                <th colspan="11">推奨リールサイズ</th>
                                            </tr>
                                            <tr>
                                                <th colspan="4">新番手</th>
                                                <th>-</th>
                                                <th>-</th>
                                                <th colspan="5">旧番手</th>
                                            </tr>
                                            <tr>
                                                <td class="gray">4000</td>
                                                <td class="gray">5000</td>
                                                <td class="gray">6000</td>
                                                <td class="gray">8000</td>
                                                <td class="gray">10000</td>
                                                <td class="gray">12000</td>
                                                <td class="gray">3000</td>
                                                <td class="gray">3500</td>
                                                <td class="gray">4000</td>
                                                <td class="gray">4500</td>
                                                <td class="gray">5000</td>
                                            </tr>
                                            <tr>
                                                <td>TC80-50G</td>
                                                <td>○</td>
                                                <td>◎</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>TC80-80G</td>
                                                <td></td>
                                                <td></td>
                                                <td>○</td>
                                                <td>◎</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>○</td>
                                                <td>◎</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>TC86-110G</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>○</td>
                                                <td>◎</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>TC83-150G</td>
                                                <td></td>
                                                <td>○</td>
                                                <td>◎</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>○</td>
                                                <td>◎</td>
                                            </tr>
                                            <tr>
                                                <td>TC80-200G</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>○</td>
                                                <td>◎</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>○</td>
                                                <td>◎</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>

                            <div class="comparison_col">
                                <div class="comparison_col_inner" style="display: block;">
                                    <h3><p>Price</p>価格表</h3>
                                    <div class="table_col">
                                    <table>
                                        <tr>
                                            <th class="name">商品名</th>
                                            <th colspan="4">標準小売価格（円）（税別表示）</th>
                                            <th rowspan="2">JANコード</th>
                                        </tr>
                                        <tr>
                                            <td class="gray">Tobizo（トビゾー）</td>
                                            <td class="gray">製品</td>
                                            <td class="gray">免責金額</td>
                                            <td class="gray">パーツ（#1）</td>
                                            <td class="gray">パーツ（#2）</td>
                                        </tr>
                                        <tr>
                                            <td>TC80-50G</td>
                                            <td>88,000</td>
                                            <td>35,200</td>
                                            <td>73,200</td>
                                            <td>28,000</td>
                                            <td>4514459730013</td>
                                        </tr>
                                        <tr>
                                            <td>TC80-50G</td>
                                            <td>88,000</td>
                                            <td>35,200</td>
                                            <td>73,200</td>
                                            <td>28,000</td>
                                            <td>4514459730020</td>
                                        </tr>
                                        <tr>
                                            <td>TC80-50G</td>
                                            <td>88,000</td>
                                            <td>35,200</td>
                                            <td>73,200</td>
                                            <td>28,000</td>
                                            <td>4514459730037</td>
                                        </tr>
                                        <tr>
                                            <td>TC80-50G</td>
                                            <td>88,000</td>
                                            <td>35,200</td>
                                            <td>73,200</td>
                                            <td>28,000</td>
                                            <td>4514459730044</td>
                                        </tr>
                                        <tr>
                                            <td>TC80-50G</td>
                                            <td>88,000</td>
                                            <td>35,200</td>
                                            <td>73,200</td>
                                            <td>28,000</td>
                                            <td>4514459730051</td>
                                        </tr>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/comparison-->
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>

        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>