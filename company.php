<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>会社概要 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>


<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>会社概要</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>会社概要</h5>
                    <p>Company</p>
                </div>
                <div class="news_list">
                    <ul>
                        <li class="end">
                        <table class="company_table">
                            <tr data-sal="slide-up" data-sal-duration="500">
                                <td class="left">商号/名称</td>
                                <td class="right">株式会社ゼナック</td>
                            </tr>
                            <tr data-sal="slide-up" data-sal-duration="500">
                                <td class="left">所在地</td>
                                <td class="right">〒669-3166 兵庫県丹波市山南町小野尻335-1</td>
                            </tr>
                            <tr data-sal="slide-up" data-sal-duration="500">
                                <td class="left">代表者</td>
                                <td class="right">笹倉 圭介</td>
                            </tr>
                            <tr data-sal="slide-up" data-sal-duration="500">
                                <td class="left">資本金</td>
                                <td class="right">10,000,000円</td>
                            </tr>
                            <tr data-sal="slide-up" data-sal-duration="500">
                                <td class="left">設立年度</td>
                                <td class="right">1985/12</td>
                            </tr>
                            <tr data-sal="slide-up" data-sal-duration="500">
                                <td class="left">URL</td>
                                <td class="right"><a href="https://zenaq.com/">https://zenaq.com/</a></td>
                            </tr>
                        </table>
                        </li>
                    </ul>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>

        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>