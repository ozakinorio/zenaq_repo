<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>体感展示会のご案内 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>体感展示会のご案内</li>
        </ul>
    </div>
    </header><!-- /header -->
    
    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>体感イベント</h5>
                    <p>Support</p>
                </div>
                <div class="faq_inner">
                    <div class="left_faq">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li><a href="faq.php">よくある質問</a></li>
                            <li><a href="stock.php">在庫納期リスト</a></li>
                            <li><a href="eol.php">生産終了モデル</a></li>
                            <li><a href="warranty.php">半永久保証</a></li>
                            <li><a href="repair.php">ロッド修理</a></li>
                            <li class="arrow_down">体感イベント</li>
                        </ul>
                    </div>
                    <div class="right_faq">

                        <div class="faq_list">
                            <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">体感展示会のご案内</h6>
                            <div class="sub_info_text" data-sal="slide-up" data-sal-duration="500">
                                ・「体感展示会」とはショップ様で実際にロッドを体感していただく為の展示です。<br>
                                ・ロッドの調子やパワーをお確かめいただけるのは勿論、実際にルアーをキャスト＆アクションしていただく事も可能です。
                            </div>
                            <div class="page_fv_img" data-sal="slide-up" data-sal-duration="500">
                                <img src="img/trial_fv.png" alt="">
                            </div>
                            <div class="checklist" data-sal="slide-up" data-sal-duration="500">
                                <p class="btn"><a href="trial_ikari.php">フォキートイカリ体感展示</a></p>

                                <div class="" data-sal="slide-up" data-sal-duration="500">
                                    <div class="accordion_stock">
                                        <div class="option">
                                            <input type="checkbox" id="toggle_trial" class="toggle">
                                            <label class="title" for="toggle_trial">ご注意</label>
                                            <div class="content">
                                                <ul class="warranty scrollbarSample" id="scrollbar">
                                                    <li>・展示ショップ様の都合や状況によってはご希望される試投ができない場合もございますので、「試投」をご希望の場合は事前に展示ショップにご確認のうえご来店ください。リールやルアーなどは展示ショップ様にはご用意しておりませんのでご自身が実際にご使用になるタックルをご持参ください。</li>
                                                    <li>・ご持参頂いたルアーのロスト、ラインブレーク、リールの故障やその他のトラブルは、弊社及び展示ショップ様は一切の責任を負えませんので予めご了承ください。 </li>
                                                    <li>・レンタルロッドではありませんので貸し出しはできません。</li>
                                                    <li>・展示ロッドのラインナップは展示会により異なるので、下記の展示ロッド一覧を必ずご確認の上お越し下さい。</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>