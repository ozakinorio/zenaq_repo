<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>生産終了モデル | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>生産終了モデル</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>生産終了モデル</h5>
                    <p>Support</p>
                </div>
                <div class="faq_inner">
                    <div class="left_faq">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li><a href="faq.php">よくある質問</a></li>
                            <li><a href="stock.php">在庫納期リスト</a></li>
                            <li class="arrow_down">生産終了モデル</li>
                            <li><a href="warranty.php">半永久保証</a></li>
                            <li><a href="repair.php">ロッド修理</a></li>
                            <li><a href="trial.php">体感イベント</a></li>
                        </ul>
                    </div>
                    <div class="right_faq">

                        <div class="faq_list">
                            <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">生産終了モデル</h6>
                            <div class="">
                                <div class="accordion_stock">
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_eol01" class="toggle">
                                        <label class="title" for="toggle_eol01">ボートジギング</label>
                                        <div class="content">
                                            <table class="stock_list">
                                                <tr class="gray">
                                                    <th>シリーズ</th>
                                                    <th>モデル</th>
                                                    <th>生産終了日</th>
                                                </tr>
                                                <tr>
                                                    <td rowspan="2">FOKEETO BULL</td>
                                                    <td>FB58-6 BULL / FS58-6 BULL<br>FB58-18 BULL / FS58-18 BULL</td>
                                                    <td>2021.05</td>
                                                </tr>
                                                <tr>
                                                    <td rowspan="2">FOKEETO BULL</td>
                                                    <td>FB58-6 BULL / FS58-6 BULL<br>FB58-18 BULL / FS58-18 BULL</td>
                                                    <td>2021.05</td>
                                                </tr>
                                                <tr>
                                                    <td rowspan="2">FOKEETO BULL</td>
                                                    <td>FB58-6 BULL / FS58-6 BULL<br>FB58-18 BULL / FS58-18 BULL</td>
                                                    <td>2021.05</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_eol02" class="toggle">
                                        <label class="title" for="toggle_eol02">ボートキャスティング </label>
                                        <div class="content">
                                            <table class="stock_list">
                                                <tr class="gray">
                                                    <th>シリーズ</th>
                                                    <th>モデル</th>
                                                    <th>生産終了日</th>
                                                </tr>
                                                <tr>
                                                    <td rowspan="2">FOKEETO BULL</td>
                                                    <td>FB58-6 BULL / FS58-6 BULL<br>FB58-18 BULL / FS58-18 BULL</td>
                                                    <td>2021.05</td>
                                                </tr>
                                                <tr>
                                                    <td rowspan="2">FOKEETO BULL</td>
                                                    <td>FB58-6 BULL / FS58-6 BULL<br>FB58-18 BULL / FS58-18 BULL</td>
                                                    <td>2021.05</td>
                                                </tr>
                                                <tr>
                                                    <td rowspan="2">FOKEETO BULL</td>
                                                    <td>FB58-6 BULL / FS58-6 BULL<br>FB58-18 BULL / FS58-18 BULL</td>
                                                    <td>2021.05</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>