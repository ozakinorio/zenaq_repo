<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>体感展示会のご案内 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li><a href="">体感展示会のご案内</a><i class="arrow-icon"></i></li>
            <li>フォキートイカリ体感展示</li>
        </ul>
    </div>
    </header><!-- /header -->
    
    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>体感イベント</h5>
                    <p>Support</p>
                </div>
                <div class="faq_inner">
                    <div class="left_faq">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li><a href="faq.php">よくある質問</a></li>
                            <li><a href="stock.php">在庫納期リスト</a></li>
                            <li><a href="eol.php">生産終了モデル</a></li>
                            <li><a href="warranty.php">半永久保証</a></li>
                            <li><a href="repair.php">ロッド修理</a></li>
                            <li class="arrow_down">体感イベント</li>
                        </ul>
                    </div>
                    <div class="right_faq">

                        <div class="faq_list">
                            <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">フォキートイカリを体感する</h6>
                            <div class="page_fv_img" data-sal="slide-up" data-sal-duration="500">
                                <img src="img/trial_fv.png" alt="">
                            </div>
                            <div class="checklist">
                                <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">船上体感</h6>
                                <div class="">
                                    <div class="atten" data-sal="slide-up" data-sal-duration="500">
                                        ・フォキートイカリを船上で体感できます。<br>
                                        ・以下の遊漁船にフォキートイカリ各種サンプルロッドをご用意しております。
                                    </div>
                                    <div class="trial_shop_map" data-sal="slide-up" data-sal-duration="500">
                                        <iframe src="https://www.google.com/maps/d/embed?mid=1JDFHsZFSa6kGxPSdBWOH5Uu_IpAF4SM3" width="100%"></iframe>
                                    </div>
                                    <div class="trial_shop_list">
                                        <div class="trial_shop_list_item" data-sal="slide-up" data-sal-duration="500">
                                            <p class="trial_title"><span>兵庫･竹野</span><br><a href="http://www.grandblue.sakura.ne.jp/" target="_blank">グランブルーさま<img src="img/common/blank.svg" style="width:12px;"></a></p>
                                            <p class="trial_tel">090-1022-9970</p>
                                        </div>
                                        <div class="trial_shop_list_item" data-sal="slide-up" data-sal-duration="500">
                                            <p class="trial_title"><span>大阪･泉北郡</span><br><a href="http://www.m-jerk.jp/index.html" target="_blank">エムズジャークさま<img src="img/common/blank.svg" style="width:12px;"></a></p>
                                            <p class="trial_tel">080-6138-0202</p>
                                        </div>
                                        <div class="trial_shop_list_item" data-sal="slide-up" data-sal-duration="500">
                                            <p class="trial_title"><span>福井･小浜</span><br><a href="https://www.tomimaru.com/" target="_blank">富丸さま<img src="img/common/blank.svg" style="width:12px;"></a></p>
                                            <p class="trial_tel">090-4321-1030</p>
                                        </div>
                                        <div class="trial_shop_list_item" data-sal="slide-up" data-sal-duration="500">
                                            <p class="trial_title"><span>兵庫･加古川</span><br><a href="http://www.mitachimaru.com/" target="_blank">ミタチ丸さま<img src="img/common/blank.svg" style="width:12px;"></a></p>
                                            <p class="trial_tel">090-3923-8351</p>
                                        </div>
                                        <div class="trial_shop_list_item" data-sal="slide-up" data-sal-duration="500">
                                            <p class="trial_title"><span>島根･松江</span><br><a href="https://sw-trust.jp/" target="_blank">トラストさま<img src="img/common/blank.svg" style="width:12px;"></a></p>
                                            <p class="trial_tel">080-4234-3423</p>
                                        </div>
                                        <div class="trial_shop_list_item" data-sal="slide-up" data-sal-duration="500">
                                            <p class="trial_title"><span>兵庫･加古川</span><br><a href="http://www.eonet.ne.jp/~bigfighter/" target="_blank">ビッグファイターさま<img src="img/common/blank.svg" style="width:12px;"></a></p>
                                            <p class="trial_tel">080-5635-0880</p>
                                        </div>
                                        <div class="trial_shop_list_item" data-sal="slide-up" data-sal-duration="500">
                                            <p class="trial_title"><span>高知</span><br>香織丸さま</p>
                                            <p class="trial_tel">090-4975-9461</p>
                                        </div>
                                        <div class="trial_shop_list_item" data-sal="slide-up" data-sal-duration="500">
                                            <p class="trial_title"><span>山口･長門</span><br><a href="https://www.gotjp.com/ken_you_maru/" target="_blank">健洋丸さま<img src="img/common/blank.svg" style="width:12px;"></a></p>
                                            <p class="trial_tel">090-5264-4916</p>
                                        </div>
                                    </div>
                                    <div class="accordion_stock" data-sal="slide-up" data-sal-duration="500">
                                        <div class="option">
                                            <input type="checkbox" id="toggle_trial01" class="toggle">
                                            <label class="title" for="toggle_trial01">注意事項</label>
                                            <div class="content">
                                                <ul class="warranty scrollbarSample" id="scrollbar">
                                                    <li>・ご乗船頂いた際に実際にご使用頂くことが出来ます。</li>
                                                    <li>・予約状況やサンプルロッド貸し出し条件などに関しては、遊漁船船長に直接お問い合わせください。</li>
                                                    <li>・本数に限りがありますのでご使用になれない場合も御座います。必ず事前にご確認ください。</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="checklist secondary">
                                <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">ショップ体感展示</h6>
                                <div class="">
                                    <div class="atten" data-sal="slide-up" data-sal-duration="500">
                                        ・「体感展示」はショップでロッドを体感していただけます。<br>
                                        ・ロッドの調子やパワーを実際に手に取りお確かめ頂けます。
                                    </div>
                                    <div class="accordion_stock" data-sal="slide-up" data-sal-duration="500">
                                        <div class="option">
                                            <input type="checkbox" id="toggle_trial02" class="toggle">
                                            <label class="title" for="toggle_trial02">注意事項</label>
                                            <div class="content">
                                                <ul class="warranty scrollbarSample" id="scrollbar">
                                                    <li>・イカリの体感展示は店内でのみロッドの調子を確認していただく展示のため店外への貸し出しはできません。</li>
                                                    <li>・展示ロッドのラインナップは展示会により異なる場合がありますので、ご確認の上お越し下さい。</li>
                                                    <li>・ゼナック社員及びフィールドスタッフは体感展示には駐在しておりません。</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>