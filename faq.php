<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>よくある質問 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>よくある質問</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>よくある質問</h5>
                    <p>Support</p>
                </div>
                <div class="faq_inner">
                    <div class="left_faq">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li class="arrow_down">よくある質問</li>
                            <li><a href="stock.php">在庫納期リスト</a></li>
                            <li><a href="eol.php">生産終了モデル</a></li>
                            <li><a href="warranty.php">半永久保証</a></li>
                            <li><a href="repair.php">ロッド修理</a></li>
                            <li><a href="trial.php">体感イベント</a></li>
                        </ul>
                    </div>
                    <div class="right_faq">

                        <div class="faq_list">
                            <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">ロッド購入について</h6>
                            <div class="">
                                <div class="accordion_faq">
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_faq01" class="toggle">
                                        <label class="title" for="toggle_faq01">商品の在庫状況や納期を確認したい</label>
                                        <div class="content">
                                            <div class="text">
                                                ロッドの在庫状況や納期はこちらからご確認いただけます。
                                                <div class="news_btns">
                                                    <p class="btn"><a href="stock.php">在庫・納期リスト</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_faq02" class="toggle">
                                        <label class="title" for="toggle_faq02">商品を購入したい</label>
                                        <div class="content">
                                            <div class="text">
                                                ゼナック製品は全国の取扱店舗様からご購入いただけます。<br>
                                                在庫・納期状況をご確認の上、お近くの店舗様にてご注文ください。また、ゼナックストアでも全商品を販売しております。
                                                <div class="news_btns">
                                                    <p class="btn"><a href="https://www.zenaq-store.jp/" target="_blank">オンラインストア</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="faq_list">
                            <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">オンラインストアでのご注文について</h6>
                            <div class="">
                                <div class="accordion_faq">
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_faq03" class="toggle">
                                        <label class="title" for="toggle_faq03">注文済み商品の納期を知りたい</label>
                                        <div class="content">
                                            <div class="text">
                                                ご注文後（当日または翌営業日）に正式な納期をご返信しておりますので、再度ゼナックからのメールをご確認ください。<br>
                                                ゼナックストアの「マイページ」からでもメール内容をご確認いただけます。詳細な発送日に関しては、商品完成次第に順次発送しておりますので直前まで確定できません。商品発送日に発送完了のメールをお送りしております。
                                                <div class="news_btns">
                                                    <p class="btn"><a href="https://www.zenaq-store.jp/" target="_blank">オンラインストア</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_faq04" class="toggle">
                                        <label class="title" for="toggle_faq04">お支払いについて</label>
                                        <div class="content">
                                            <div class="text">
                                                <p>クレジットカード決済</p>
                                                <p>商品が在庫切れや予約であっても即時決済(先払い)となります。<br>VISA、Master Card、JCB、American Express、Diners Club が利用可能です。</p>
                                                <p>代金引換</p>
                                                <p>佐川急便 e-コレクト（現金決済のみ）<br>手数料につきましては弊社が負担いたします。</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_faq05" class="toggle">
                                        <label class="title" for="toggle_faq05">注文内容の変更・キャンセルをしたい</label>
                                        <div class="content">
                                            <div class="text">
                                                ご注文内容の変更やキャンセルはご自身では行っていただけません。<br>お問い合わせフォームよりお問い合わせください。
                                                <div class="news_btns">
                                                    <p class="btn"><a href="contact.php">お問い合わせ</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_faq06" class="toggle">
                                        <label class="title" for="toggle_faq06">届け先住所や登録情報を変更したい</label>
                                        <div class="content">
                                            <div class="text">
                                                ご登録内容はゼナックストアの「マイページ」より変更可能です。<br>下記リンク先よりお試しください。
                                                <div class="news_btns">
                                                    <p class="btn"><a href="https://www.zenaq-store.jp/" target="_blank">オンラインストア</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_faq07" class="toggle">
                                        <label class="title" for="toggle_faq07">荷物を長期間受領されなかった場合の送料について</label>
                                        <div class="content">
                                            <div class="text">
                                                運送会社さまのお荷物保管期間は配達日から1週間です。保管期間を過ぎたお荷物は、受取辞退となり弊社へ返送されます。返送時には着払いでの返品送料が発生しており、再度お客様に配達をすると合計で3回分の送料が発生することになります。初回のお客様への商品発送料は弊社が負担しておりますが、それ以降の弊社までの返送料・再配送料はお客様にご請求する場合がございますので予めご了承ください。<br>
                                                <br>
                                                <strong>急用や出張等で長期間お留守にされる場合について</strong><br>
                                                ご注文完了後、発送予定時期をご案内しております。長期間不在等で荷物の受取が難しい方は必ず弊社までご連絡ください。<br>
                                                <strong>ご住所不明などで、商品をお届けできない場合も対象となりますのでご注意ください</strong><br>
                                                注文時には番地や部屋番号等の記入漏れがないか、必ずご確認のうえご住所をご登録下さいますようお願い申し上げます。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="option" data-sal="slide-up" data-sal-duration="500">
                                        <input type="checkbox" id="toggle_faq08" class="toggle">
                                        <label class="title" for="toggle_faq08">その他のご質問</label>
                                        <div class="content">
                                            <div class="text">
                                                その他のご質問はお問い合わせフォームよりお問い合わせください。
                                                <div class="news_btns">
                                                    <p class="btn"><a href="contact.php">お問い合わせ</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>