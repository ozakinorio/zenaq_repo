<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>半永久保証 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="/">ホーム</a><i class="arrow-icon"></i></li>
            <li><a href="warranty.php">半永久保証</a><i class="arrow-icon"></i></li>
            <li>登録</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>半永久保証</h5>
                    <p>Support</p>
                </div>
                <div class="faq_inner">
                    <div class="left_faq">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li><a href="faq.php">よくある質問</a></li>
                            <li><a href="stock.php">在庫納期リスト</a></li>
                            <li><a href="eol.php">生産終了モデル</a></li>
                            <li class="arrow_down">半永久保証</li>
                            <li><a href="repair.php">ロッド修理</a></li>
                            <li><a href="trial.php">体感イベント</a></li>
                        </ul>
                    </div>
                    <div class="right_faq">

                        <div class="faq_list">
                            <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">半永久保証登録</h6>
                            <!--<div class="sub_info_text">
                                ・お問い合わせには必ず返信しておりますが、お返事までに数日頂く場合がございますので予めご了承ください。<br>
                                ・しばらく経っても弊社からの返信がない場合には受信設定をご確認の上、再度お問い合わせください。<br>
                                ・受信拒否設定をされている場合は、ドメイン( @zenaq.com、@zenaq-store.jp )の受信許可設定をお願いします。また、ご質問内容によってはお答えしかねる場合がございますので悪しからずご了承ください。
                            </div>-->
                            
                            <div class="contact_form_col">
                                <div class="contact_form_col_inner">
                                    <form action="" method="post">
                                        <table>
                                            <tr data-sal="slide-up" data-sal-duration="500">
                                                <td class="left">
                                                    メールアドレス
                                                </td>
                                                <td class="right">
                                                    <input name="" type="email" required="required" class="" placeholder=>
                                                </td>
                                            </tr>
                                            <tr data-sal="slide-up" data-sal-duration="500">
                                                <td class="left">
                                                    保証書番号
                                                </td>
                                                <td class="right">
                                                    <input name="" type="text" required="required" class="" placeholder=>
                                                    <p class="atten">
                                                        ・保証書の紛失やロッドの譲受けなどにより保証書番号が分からない場合は「再登録」とご記入下さい。折り返し新しい登録番号をご連絡いたします。
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr data-sal="slide-up" data-sal-duration="500">
                                                <td class="left">
                                                    ロッドモデル名
                                                </td>
                                                <td class="right">
                                                    <input name="" type="text" required="required" class="" placeholder=例：FOKEETO FB63-3 IKARI>
                                                    <p class="atten">
                                                        ・ガイド設定が複数ある場合は必ず <Kガイドモデル> < RGガイドモデル> などもご記入ください。
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr data-sal="slide-up" data-sal-duration="500">
                                                <td class="left">
                                                    お名前
                                                </td>
                                                <td class="right">
                                                    <input name="" type="text" required="required" class="" placeholder=>
                                                </td>
                                            </tr>
                                            <tr data-sal="slide-up" data-sal-duration="500">
                                                <td class="left">
                                                    郵便番号
                                                </td>
                                                <td class="right">
                                                    <input name="" type="text" class="" placeholder=例：669-3166>
                                                </td>
                                            </tr>
                                            <tr data-sal="slide-up" data-sal-duration="500">
                                                <td class="left">
                                                    ご住所
                                                </td>
                                                <td class="right">
                                                    <input name="" type="text" class="" placeholder=>
                                                </td>
                                            </tr>
                                            <tr data-sal="slide-up" data-sal-duration="500">
                                                <td class="left">
                                                    電話番号
                                                </td>
                                                <td class="right">
                                                    <input name="" type="tel" class="" placeholder=>
                                                </td>
                                            </tr>
                                            <tr data-sal="slide-up" data-sal-duration="500">
                                                <td class="left">
                                                    生年月日
                                                </td>
                                                <td class="right">
                                                    <input name="" type="text" class="" placeholder=例：1980/11/06>
                                                </td>
                                            </tr>
                                            <tr data-sal="slide-up" data-sal-duration="500">
                                                <td class="left">
                                                    お買い上げ日
                                                </td>
                                                <td class="right">
                                                    <input name="" type="text" class="" placeholder=例：2019/02/14>
                                                    <p class="atten">
                                                        ・不明な場合でもおおよそのご購入日を必ずご記入ください。
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr data-sal="slide-up" data-sal-duration="500">
                                                <td class="left">
                                                    販売店名/購入方法
                                                </td>
                                                <td class="right">
                                                    <input name="" type="text" class="" placeholder=例：XX釣具店(兵庫県)／直接購入／友人から／ｵｰｸｼｮﾝ>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn_form" data-sal="slide-up" data-sal-duration="500">
                                            <a href="warranty_entry_done.php">SEND</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>