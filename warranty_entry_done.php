<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>半永久保証 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="/">ホーム</a><i class="arrow-sicon"></i></li>
            <li><a href="warranty.php">半永久保証</a><i class="arrow-icon"></i></li>
            <li><a href="warranty_entry.php">登録</a><i class="arrow-icon"></i></li>
            <li>登録完了</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>半永久保証</h5>
                    <p>Support</p>
                </div>
                <div class="faq_inner">
                    <div class="left_faq">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li><a href="faq.php">よくある質問</a></li>
                            <li><a href="stock.php">在庫納期リスト</a></li>
                            <li><a href="eol.php">生産終了モデル</a></li>
                            <li class="arrow_down">半永久保証</li>
                            <li><a href="repair.php">ロッド修理</a></li>
                            <li><a href="trial.php">体感イベント</a></li>
                        </ul>
                    </div>
                    <div class="right_faq">

                        <div class="faq_list">
                            <h6 class="faq_title" data-sal="slide-up" data-sal-duration="500">登録完了いたしました</h6>

                            <div class="contact_form_col">
                                <div class="contact_form_col_inner">
                                    <div class="sub_info_text" data-sal="slide-up" data-sal-duration="500">
                                        ご登録ありがとうございます。半永久保証の登録が完了いたしました。<br>
                                        登録完了メールは現在システムの都合上、自動返信されませんのでご了承ください。<br>
                                        今後とも宜しくお願いいたします。<br>
                                        <br>
                                        ※保証書番号の再登録のお客様には新しい番号をメールにてご連絡いたします。
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>