<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>新着情報 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>新着情報</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>新着情報</h5>
                    <p>News</p>
                </div>
                <div class="news_list">
                    <ul>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title">
                                <p class="data">2021.07.28</p>
                                <span class="tag">MUTHOS Accura 100HHH /</span><br>
                                <div class="button button--atlas">
                                    <span>今回の受注分は完売いたしました。</span>
                                    <div class="marquee" aria-hidden="true">
                                        <div class="marquee__inner">
                                            <span>今回の受注分は完売いたしました。</span>
                                            <span>今回の受注分は完売いたしました。</span>
                                            <span>今回の受注分は完売いたしました。</span>
                                            <span>今回の受注分は完売いたしました。</span>
                                        </div>
                                    </div>
                                </div>
                            </h6>
                            <div class="news_text">
                                MUTHOS  Accura 100HHH　今回の受注分は品切れになりました。<br>
                                ご注文ありがとうございました。次回の受注日は決まり次第に告知いたします。
                            </div>
                            <div class="news_btns">
                                <p class=""><a href="">ボタン</a></p>
                                <p class=""><a href="">ボタン</a></p>
                                <p class=""><a href="">ボタン</a></p>
                            </div>
                        </li>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title"><p class="data">2021.07.15</p>
                            <span class="tag">MUTHOS Sonio /</span><br>
                            <div class="button button--atlas">
                                    <span>セグメントオーダー7月受注分は品切れになりました。</span>
                                    <div class="marquee" aria-hidden="true">
                                        <div class="marquee__inner">
                                            <span>セグメントオーダー7月受注分は品切れになりました。</span>
                                            <span>セグメントオーダー7月受注分は品切れになりました。</span>
                                            <span>セグメントオーダー7月受注分は品切れになりました。</span>
                                            <span>セグメントオーダー7月受注分は品切れになりました。</span>
                                        </div>
                                    </div>
                                </div>
                            </h6>
                            <div class="news_text">
                                セグメントオーダー7月受注分は品切れになりました。<br>
                                ご注文ありがとうございました。<br>
                                <br>
                                ・MUTHOS Sonio 100M<br>
                                　<span>次回受注日は10月14日(木)12:30PM</span><br>
                                　納期予定:2022年1月末〜2月末頃<br>
                            </div>
                        </li>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title"><p class="data">2021.07.07</p>
                            <span class="tag">ニューモデル /</span><br>MUTHOS  Accura 100HHH</h6>
                            <div class="news_text">
                                <h6>7/28 (水) 12:30PM 受注開始</h6>
                                <h6>初回デリバリー 8月末〜9月末予定</h6>
                                夢に挑戦するアングラーに贈る、ロックショア最強モデル。<br>
                                <br>
                                遠征や離島などでトロフィークラスの魚を狙う、修行にも近いロックショア釣行をサポートし、アングラーの夢を現実のものにする。
                            </div>
                        </li>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title"><p class="data">2021.06.07</p>
                            <span class="tag">ブラック再入荷 /</span><br>フィールド バッグ</h6>
                            <div class="news_text">
                                高い収納性と利便性を考慮し、広い開口部と浅目の構造が特徴となる防水バック。<br>
                                ターポリンの特性である剛性と防水性を活かすことをコンセプトとして設計することで、沖渡しや遠征など釣りの現場だけでなく、アウトドアからスポーツ、小旅行などあらゆるライフシーンをサポートしてくれます。
                            </div>
                        </li>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title"><p class="data">2021.06.03</p>
                            <span class="tag">新製品 /</span><br>ドライポーター</h6>
                            <div class="news_text">
                                冷高い防水性能と摩擦にも強く裂けにくい高い耐久性を発揮するターポリン生地を採用したベーシックな防水バックパック。<br>
                                遠征時にも十分な容量になっており、手入れもしやすく、本格的なアウトドアからデイリーユースまで様々なシーンで活躍します。
                            </div>
                        </li>
                    </ul>
                    <div class="pager" data-sal="slide-up" data-sal-duration="500">
                        <ul class="pagination">
                            <li class="pre"><a href="#"><span>«</span></a></li>
                            <li><a href="#" class="active"><span>1</span></a></li>
                            <li><a href="#"><span>2</span></a></li>
                            <li><a href="#"><span>3</span></a></li>
                            <li><a href="#"><span>4</span></a></li>
                            <li><a href="#"><span>5</span></a></li>
                            <li class="next"><a href="#"><span>»</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>

        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>