<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>商品一覧 | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>商品一覧</li>
        </ul>
    </div>
    </header><!-- /header -->
    
    <!-- main -->
    <main class="main">
        <div class="other">
            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>商品一覧</h5>
                    <p>Product</p>
                </div>
                <div class="product_inner">
                    <div class="left_product">
                    <h6 class="left_col_title" data-sal="slide-up" data-sal-duration="500">ジャンル</h6>
                        <ul class="nav" data-sal="slide-up" data-sal-duration="500">
                            <li>ロッド</li>
                            <li>アクセサリー</li>
                        </ul>
                        <h6 class="left_col_title" data-sal="slide-up" data-sal-duration="500">ロッド</h6>
                        <ul class="nav">
                            <li data-sal="slide-up" data-sal-duration="500">
                                <input type="checkbox" id="checkbox_a" name="checkbox">
                                <label for="checkbox_a" class="checkbox">ボートジギング</label>
                            </li>
                            <li data-sal="slide-up" data-sal-duration="500">
                                <input type="checkbox" id="checkbox_b" name="checkbox">
                                <label for="checkbox_b" class="checkbox">ボートキャスティング</label>
                            </li>
                            <li data-sal="slide-up" data-sal-duration="500">
                                <input type="checkbox" id="checkbox_c" name="checkbox">
                                <label for="checkbox_c" class="checkbox">遠征コンパクトロッド</label>
                            </li>
                            <li data-sal="slide-up" data-sal-duration="500">
                                <input type="checkbox" id="checkbox_d" name="checkbox">
                                <label for="checkbox_d" class="checkbox">ロックショア</label>
                            </li>
                            <li data-sal="slide-up" data-sal-duration="500">
                                <input type="checkbox" id="checkbox_e" name="checkbox">
                                <label for="checkbox_e" class="checkbox">シーバス</label>
                            </li>
                            <li data-sal="slide-up" data-sal-duration="500">
                                <input type="checkbox" id="checkbox_f" name="checkbox">
                                <label for="checkbox_f" class="checkbox">ロックフィッシュ</label>
                            </li>
                            <li data-sal="slide-up" data-sal-duration="500">
                                <input type="checkbox" id="checkbox_g" name="checkbox">
                                <label for="checkbox_g" class="checkbox">ブラックバス</label>
                            </li>
                            <li data-sal="slide-up" data-sal-duration="500">
                                <input type="checkbox" id="checkbox_h" name="checkbox">
                                <label for="checkbox_h" class="checkbox">バンブーワーク</label>
                            </li>
                        </ul>
                        <h6 class="left_col_title" data-sal="slide-up" data-sal-duration="500">価格</h6>
                        <div class="price_text" data-sal="slide-up" data-sal-duration="500">
                            <div class="cp_ipselect cp_sl01">
                                <select required>
                                    <option value="" hidden>制限なし</option>
                                    <option value="2">5,000円</option>
                                    <option value="3">10,000円</option>
                                    <option value="4">15,000円</option>
                                </select>
                            </div>
                            <span>~</span>
                            <div class="cp_ipselect cp_sl01">
                                <select required>
                                    <option value="" hidden>制限なし</option>
                                    <option value="2">5,000円</option>
                                    <option value="3">10,000円</option>
                                    <option value="4">15,000円</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="right_product">

                        <div class="product_list">
                            <ul>
                                <li data-sal="slide-up" data-sal-duration="500"><a href="product.php">
                                    <span class="sub01">NEW</span>
                                    <p class="img"><img src="img/product/list_rod_ikari.png" alt=""></p>
                                    <p class="sub02">ボートジギング</p>
                                    <p class="title">FOKEETO IKARI</p>
                                    <p class="subtitle">¥00,000~</p></a>
                                </li>
                                <li data-sal="slide-up" data-sal-duration="500"><a href="product.php">
                                    <p class="img"><img src="img/product/list_rod_ikari.png" alt=""></p>
                                    <p class="sub02">ボートジギング</p>
                                    <p class="title">FOKEETO IKARI</p>
                                    <p class="subtitle">¥00,000~</p></a>
                                </li>
                                <li data-sal="slide-up" data-sal-duration="500"><a href="product.php">
                                    <p class="img"><img src="img/product/list_rod_ikari.png" alt=""></p>
                                    <p class="sub02">ボートジギング</p>
                                    <p class="title">FOKEETO IKARI</p>
                                    <p class="subtitle">¥00,000~</p></a>
                                </li>
                                <li data-sal="slide-up" data-sal-duration="500"><a href="product.php">
                                    <p class="img"><img src="img/product/list_rod_ikari.png" alt=""></p>
                                    <p class="sub02">ボートジギング</p>
                                    <p class="title">FOKEETO IKARI</p>
                                    <p class="subtitle">¥00,000~</p></a>
                                </li>
                                <li data-sal="slide-up" data-sal-duration="500"><a href="product.php">
                                    <p class="img"><img src="img/product/list_rod_ikari.png" alt=""></p>
                                    <p class="sub02">ボートジギング</p>
                                    <p class="title">FOKEETO IKARI</p>
                                    <p class="subtitle">¥00,000~</p></a>
                                </li>
                                <li data-sal="slide-up" data-sal-duration="500"><a href="product.php">
                                    <p class="img"><img src="img/product/list_rod_ikari.png" alt=""></p>
                                    <p class="sub02">ボートジギング</p>
                                    <p class="title">FOKEETO IKARI</p>
                                    <p class="subtitle">¥00,000~</p></a>
                                </li>
                                <li data-sal="slide-up" data-sal-duration="500"><a href="product.php">
                                    <p class="img"><img src="img/product/list_rod_ikari.png" alt=""></p>
                                    <p class="sub02">ボートジギング</p>
                                    <p class="title">FOKEETO IKARI</p>
                                    <p class="subtitle">¥00,000~</p></a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>