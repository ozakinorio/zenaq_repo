<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>コンセプト | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>コンセプト</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="about_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>コンセプト</h5>
                    <p>About Us</p>
                </div>
                <div class="about_inner">

                    <div class="left_about">
                        <ul class="nav sticky" data-sal="slide-up" data-sal-duration="500">
                            <li class="arrow_down">コンセプト</li>
                            <li><a href="about_tech.php">常識破壊</a></li>
                            <li><a href="about_history.php">ヒストリー</a></li>
                        </ul>
                    </div>

                    <div class="right_about">
                        <div class="about_col">
                            <div class="page_movie" data-sal="slide-up" data-sal-duration="500">
                                <video type="video/webm" src="img/index/movie01.mp4" playsinline loop autoplay muted></video>
                            </div>
                            <p class="catch_text" data-sal="slide-up" data-sal-duration="500">INSPIRE<br>THE SOUL</p>
                            <h6 class="catch_main" data-sal="slide-up" data-sal-duration="500">魂を揺さぶる体験。<br>最高のロッドと共に。</h6>
                            <p class="catch_img01" data-sal="slide-up" data-sal-duration="500"><img src="img/about/02.png" alt=""></p>
                            <p class="catch_img02" data-sal="slide-up" data-sal-duration="500"><img src="img/about/01.png" alt=""></p>
                        </div>

                        <div class="about_col02">
                            <div class="about_col_inner">
                                <div class="title" data-sal="slide-up" data-sal-duration="500">一人のアングラーとして、創る。</div>
                                <div class="text" data-sal="slide-up" data-sal-duration="500">
                                    「フィールドからの答えを製品に反映させる」というコンセプト。<br>
                                    良い道具を作るのに、近道はない。<br>
                                    どれほど時間がかかろうとも妥協なきフィールドテストを繰り返し、釣竿としての本質的な答えを常に探求しています。<br>
                                    一人のアングラーとして、ゼナックは魂を揺さぶるロッド創りに挑戦してゆきます。
                                </div>
                                <div class="imgs">
                                    <ul class="aboutimg_list">
                                        <li class="img01" data-sal="slide-up" data-sal-duration="500"><img src="img/about/03.png" alt=""></li>
                                        <li class="img02" data-sal="slide-up" data-sal-duration="500"><img src="img/about/04.png" alt=""></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="bg_text">I'M AN<br>ANGLER.</div>
                        </div>

                        <div class="about_col03">
                            <div class="about_col_inner">
                                <div class="title" data-sal="slide-up" data-sal-duration="500">世界最高峰のロッドブランク</div>
                                <div class="text" data-sal="slide-up" data-sal-duration="500">
                                    1960年の創業以来、釣竿の原点「竹竿」から始まり、現在のカーボンロッドに至るまで、一本一本を丹念に国内の自社工場で生産し続けています。<br>
                                    一貫して同じ地に工場を構える事で熟練の技を磨き続け、日本人ならではの感性で最高のロッドを作る。<br>
                                    それがゼナックのプライドです。
                                </div>
                                <div class="img" data-sal="slide-up" data-sal-duration="500"><img src="img/about/05.png" alt=""></div>
                                <div class="bg_text">HANDMADE<br>IN JAPAN.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>
        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>