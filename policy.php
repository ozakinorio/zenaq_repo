<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>個人情報の取扱に関して | ZENAQ(ゼナック)</title>
<?php include('inc/meta.php'); ?>
<?php include('inc/head.php'); ?>
</head>
<body>

<div id="" class="wrap">

    <header class="header_other">
        <?php include('inc/header.php'); ?>
        <div class="breadlist">
        <ul>
            <li><a href="">ホーム</a><i class="arrow-icon"></i></li>
            <li>個人情報の取扱に関して</li>
        </ul>
    </div>
    </header><!-- /header -->

    <!-- main -->
    <main class="main">
        <div class="other">

            <div class="otherinner">
                <div class="info_inner_news_titles" data-sal="slide-up" data-sal-duration="500">
                    <h5>個人情報の取扱に関して</h5>
                    <p>Privacy</p>
                </div>
                <div class="sub_info_text" data-sal="slide-up" data-sal-duration="500">
                    ・当社がお客様から保証書登録などの目的でご記入いただいた個人情報につきましては、以下の通り適切な運用及び管理に勤めます。
                </div>
                <div class="news_list">
                    <ul>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title">当社が取得する個人情報</h6>
                            <div class="news_text">
                                当社の半永久保証制度にご登録をご希望のお客様に限り、当社様式による「お名前」「ご住所」「電話番号」等の個人情報をご記入いただいております。<br>
                                その他、修理お預かりの際にも「お名前」「ご住所」「電話番号」等の個人情報をご記入いただいております。
                            </div>
                        </li>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title">利用目的</h6>
                            <div class="news_text">
                                当社は、前条により取得した個人情報を、以下の目的により使用します。<br>
                                当社の販売する釣竿、及び用品に関連する修理等のアフターサービスの為の発送及び連絡事項確認のために使用する場合があります。当社の発売する新製品情報、主催する釣り大会、講習会など情報のご案内のために使用する場合があります。
                            </div>
                        </li>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title">個人情報の第三者への提供</h6>
                            <div class="news_text">
                                当社は、法令に定められた場合以外は、個人情報を本人の同意なく第三者へ提供することはありません。<br>
                                業務委託先への開示（当社は、現状の業務においては、個人情報を業務委託先へ開示することはありません）
                            </div>
                        </li>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title">個人情報の開示、訂正、削除等</h6>
                            <div class="news_text">
                                お客様はいつでも当社に対して、当社が保有するお客様ご自身の個人情報を開示するよう請求する事ができます。<br>
                                この場合は、当社がお客様がご本人であるかの確認を取らせていただく場合がございます。<br>
                                お客様は、当社が保有するお客様の個人情報の登録内容に誤り、又は変更がある場合には修正、削除の請求ができます。<br>
                                尚、上記に従って行った削除により弊社の必要な情報項目を満たさなくなった場合には半永久保証制度への登録などが不完全となり登録抹消させていただく場合もあります。その場合には必ずご本人様に通知いたします。
                            </div>
                        </li>
                        <li data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title">当社からの情報発信中止のお申し出</h6>
                            <div class="news_text">
                                お客様はいつでも当社に対して、情報発信中止のお申し出をすることができます。<br>
                                お客様はいつでも当社に対して、「情報発信中止依頼」の取り消しをすることができます。
                            </div>
                        </li>
                        <li class="end" data-sal="slide-up" data-sal-duration="500">
                            <h6 class="news_title">プライバシーポリシーの変更に付いて</h6>
                            <div class="news_text">
                                プライバシーポリシー（個人情報保護方針）の内容については、必要に応じて予告無く変更する場合があります。<br>
                                その場合にはこのページにて内容変更を掲示いたします。
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <?php include('inc/info.php'); ?>
            <?php include('inc/cv.php'); ?>

        </div>
    </main><!-- /main -->

    <?php include('inc/footer.php'); ?>

</div><!-- /wrap -->

<?php include('inc/script.php'); ?>

</body>
</html>